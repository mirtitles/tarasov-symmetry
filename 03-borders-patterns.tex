% !TEX root = tarasov-symmetry.tex






\chapter{Borders and Patterns}


\section{Borders}\marginnote{ A mathematician, just like an artist or a poet, produces designs.
- G. Hardy}

A periodically recurring pattern on a tape is called a border. Borders come in various types. These may be frescoes, decorating walls, galleries, stairs, and also iron casting used as fencing for parks, bridges, embankments. These also may be gypsum plaster-reliefs or earthenware.


%%\begin{fullwidth}
%\begin{marginfigure}
%%\centering
%%\begin{minipage}{.45\textwidth}
% % \centering
%  \includegraphics[width=\textwidth]{figs/ch-04/border-01.jpg}
%  \caption{Seven different types of borders showing translational symmetry.}
%  \label{border-01}
%%\end{minipage}%
%\end{marginfigure}

\hyperref[borders]{Figure \ref{borders}~\subref{border-01}} presents 14 borders in seven pairs. Each pair consists of borders similar in symmetry type. \emph{Altogether, there are seven types of symmetry of borders}. Any border has a translational symmetry along its axis (translation axis). In the simplest case, the border has only translational symmetry (\hyperref[borders]{Figure~\ref{borders}~\subref{border-01}~a}). \hyperref[borders]{Figure~\ref{borders}~\subref{border-02}~a} is a schematic representation of that sort of border, the triangle stands for a recurring asymmetric element.

The borders shown in \hyperref[borders]{Figure~\ref{borders}~\subref{border-01}~b}, apart from a translational symmetry, also have a mirror symmetry: they are mirror-symmetrical relative to the straight line dividing the border tape in half longitudinally. This sort of border is shown schematically in Fig. \hyperref[borders]{Figure~\ref{borders}~\subref{border-02}~b}, where the translation axis doubles as an axis of symmetry.

In the borders depicted in\hyperref[borders]{Figure~\ref{borders}~\subref{border-01}~c} and \hyperref[borders]{Figure~\ref{borders}~\subref{border-02}~c} the translation axis is a glide axis.
The borders in \hyperref[borders]{Figure~\ref{borders}~\subref{border-01}~d} have transverse axes of symmetry. They are given in \hyperref[borders]{Figure~\ref{borders}~\subref{border-02}~d} as segments of straight lines perpendicular to the translation axis.

The borders of \hyperref[borders]{Figure~\ref{borders}~\subref{border-01}~e} have two-fold rotation axes perpendicular to the border plane. The intersections of those axes with the border plane are marked in \hyperref[borders]{Figure~\ref{borders}~\subref{border-02}~e} by lentil-shaped figures.
Combining glide axes with two-fold rotation axes normal to the border plane produces .the borders given in \hyperref[borders]{Figure~\ref{borders}~\subref{border-01}~f}, which possess transverse axes of symmetry. The scheme of that kind of border is presented in \hyperref[borders]{Figure~\ref{borders}~\subref{border-02}~f}.

%\begin{figure*}
%\includegraphics[width=0.45\textwidth]{figs/ch-04/border-01.jpg}
%    \captionof{figure}{Seven different types of borders showing translational symmetry.
%  \label{border-01}}
%\quad
%\includegraphics[width=0.45\textwidth]{figs/ch-04/border-02.jpg} 
%\captionof{figure}{Schematic illustration of the translational symmetry in the seven different types of borders. \label{border-02}} 
%\end{figure*}


\begin{figure*}[!ht]
\centering
\subfloat[][Seven different types of borders showing translational symmetry.]{
\includegraphics[width=0.35\textwidth]{figs/ch-03/border-01.jpg} 
\label{border-01}}
\qquad
\subfloat[][Schematic illustration of the translational symmetry in the seven different types of borders.]{
\includegraphics[width=0.35\textwidth]{figs/ch-03/border-02.jpg} 
\label{border-02}}
\caption{The seven border types and their schematics showing translational symmetry.}
\label{borders}
\end{figure*}

Lastly, \hyperref[borders]{Figure~\ref{borders}~\subref{border-01}~g} and \hyperref[borders]{Figure~\ref{borders}~\subref{border-02}~g} provide borders based on combinations of mirror reflections. Apart from a longitudinal axis, such borders also have transverse axes of symmetry. As a consequence, two-fold rotation axes emerge. \marginnote{Please note that in the original book, the border figures are separate figures 28 and 29. Here we have put them with same figure number. Hence there is no fig 29.}

\addtocounter{figure}{1}

%\begin{marginfigure}
%  \includegraphics[width=.9\textwidth]{figs/ch-04/border-02.jpg}
%  \caption{Schematic illustration of the translational symmetry in the seven different types of borders.}
%  \label{border-02}
%\end{marginfigure}
%

\section{Decorative Patterns}
You have admired decorative patterns - those amazing designs that occur so widely in applied arts. In them you can find bizarre marriages of translational, mirror, and rotational symmetries. Examples abound. Just look at the design of the wall-paper in your room. Some patterns are shown in Figures \ref{escher-01}-\ref{escher-02}. Two of them have been produced by the eminent Dutch artist Escher:  `Flying Birds' (\hyperref[escher-01]{Figure \ref{escher-01}}) and `Lizards' (\hyperref[escher-02]{Figure \ref{escher-02}}).
\begin{figure}[!ht]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-03/tess-01.jpg}
\caption{\emph{Flying Birds} by Dutch artist M. C. Escher.}
\label{escher-01}
\end{figure}


Any pattern is based on one of the five plane lattices discussed in Chapter 2. The type of the plane lattice determines the character of the translational symmetry of a given pattern. So the `Flying Birds' pattern is based on an oblique lattice, the characteristic Egyptian ornament in \hyperref[tess-02]{Figure~\ref{tess-02}} on a square lattice, and the `Lizards' pattern on a hexagonal lattice.
\begin{figure}[!ht]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-03/tess-02.jpg}
\caption{A characteristic Egyptian ornamental decoration.}
\label{tess-02}
\end{figure}

In the simplest case, a pattern is characterized by a translational symmetry alone. Such is, for example, the `Flying Birds' pattern. To construct that pattern one must select an appropriate oblique lattice and `fill' a unit cell of the lattice with some design, and then reproduce it repeatedly by displacing the cell without changing its orientation. 
\begin{figure}[!ht]
\centering
\includegraphics[width=0.95\textwidth]{figs/ch-03/tess-03.jpg}
\caption{\emph{Lizards} by Dutch artist M. C. Escher.}
\label{escher-02}
\end{figure}

In \hyperref[tess-04]{Figure~\ref{tess-04}} a unit cell of the pattern is hatched. Note that the surface area of the cell is equal to the total area occupied by birds of different colours.

\begin{figure}[!ht]
\centering
\includegraphics[width=0.6\textwidth]{figs/ch-03/tess-04.jpg}
\caption{Finding the unit cell in \emph{Flying Birds} by Dutch artist M. C. Escher.}
\label{tess-04}
\end{figure}


The symmetry of the Egyptian design is analyzed in \hyperref[egypt-01]{Figure~\ref{egypt-01}}. The translational symmetry of the pattern is given by a square lattice with a unit cell singled out in \hyperref[egypt-01]{Figure~\ref{egypt-01}~a}. That cell has two-fold rotation axes, conventional and glide axes of symmetry. In \hyperref[egypt-01]{Figure~\ref{egypt-01}~b} conventional axes of symmetry are indicated by solid lines, and glide axes by dash lines. 

\begin{figure}[!ht]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-03/tess-05.jpg}
\caption{Finding the unit cell in Egyptian ornamental design.}
\label{egypt-01}
\end{figure}


Intersections of the two-fold rotation axes with the plane of the pattern are marked by lentil-shaped figures. Unlike the 'Flying Birds', this design has a higher symmetry, as follows from the presence of \emph{rotation} axes, and also of conventional and glide axes of \emph{mirror symmetry}.


The symmetry of the Egyptian pattern would be yet higher if we simplified the colouring - instead of the red and blue colours used one colour, for example red. In that case, we would also have four-fold rotation axes and more glide axes of symmetry. The symmetry of this design is indicated in \hyperref[egypt-01]{Figure~\ref{egypt-01}~c}, where the black squares are intersections of four-fold rotation axes with the plane of the pattern.

\hyperref[egypt-01]{Figure~\ref{egypt-01}~b} and \hyperref[egypt-01]{c} contains all the information about the elements of symmetry of the corresponding patterns. If in the figures we removed the design and only retained the conventional and glide axes and intersections of rotation axes with the plane of the pattern, we would obtain \emph{schematic} representations of two different types of the symmetry patterns. \emph{In all there are 17 types of symmetry plane designs}. They are given in \hyperref[17-types]{Figure~\ref{17-types}}. Here solid straight lines are conventional axes of symmetry and dash lines are glide axes. The lentils are the intersections of two-fold axes with the plane of the pattern; the triangles, three-fold axes; squares, four-fold axes; and hexagons, six-fold axes. The pattern of \hyperref[egypt-01]{Figure~\ref{egypt-01}~b} is represented in Fig. 35 by \textbf{\#~9}, and the pattern of \hyperref[egypt-01]{Figure~\ref{egypt-01}~c}, by \textbf{\#~12}; the `Flying Birds', by \textbf{\#~1}.

\begin{figure*}[!ht]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-03/tess-06.jpg}
\caption{The 17 types of symmetry plane designs.}
\label{17-types}
\end{figure*}

\section{Pattern Construction}

Any pattern can in principle be constructed along the lines of  `Flying Birds' by the parallel displacements of the unit cell, which is filled by some design. This procedure is the only one possible where a pattern has neither rotational nor mirror symmetry. Otherwise, a pattern can be produced using other procedures; the initial design then (or \emph{main motif}) is not the entire unit cell of a design, but just part of it.
                                  
Turning to the Egyptian design of \hyperref[tess-02]{Figure~\ref{tess-02}} , the main motif here may
be the design within the confines of the shaded rectangle at the bottom of  \hyperref[egypt-01]{Figure~\ref{egypt-01}~a}  (it makes up 1/8 of the surface area of the unit cell). The
main motif is given separately in \hyperref[egypt-02]{Figure~\ref{egypt-02}~a}. 

\begin{figure*}[!ht]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-03/pattern-01.jpg}
\caption{Constructing the Egyptian ornamental pattern.}
\label{egypt-02}
\end{figure*}

To construct a pattern we will make use of axes $BC$ and $DE$ in \hyperref[egypt-01]{Figure~\ref{egypt-01}~c} and \hyperref[egypt-02]{Figure~\ref{egypt-02}}, and the two-fold rotation axis passing through point $A$. We will now fix $A$ and $BC$ and
$DE$ in the figure and obtain for the main motif (\hyperref[egypt-02]{Figure~\ref{egypt-02}~a}) reflections relative to $BC$ and $DE$ and turns by \ang{180} about $A$ in any sequence and indefinitely. A turn about $A$ gives us \hyperref[egypt-02]{Figure~\ref{egypt-02}~b}, a subsequent reflection relative to $BC$ gives us \hyperref[egypt-02]{Figure~\ref{egypt-02}~c}. Next we effect a reflection relative to $DE$ (\hyperref[egypt-02]{Figure~\ref{egypt-02}~d}) and another turn by \ang{180} about $A$ (\hyperref[egypt-02]{Figure~\ref{egypt-02}~e}), yet another reflection relative to $BC$, and so on. As we go through the procedure, the pattern comes to life before our very eyes filling up the whole of the figure.


\begin{marginfigure}
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-03/pattern-02.jpg}
\caption{Constructing a simplified Egyptian pattern.}
\label{egypt-03}
\end{marginfigure}
Let us now take the Egyptian design with a simplified colouring (\hyperref[egypt-01]{Figure~\ref{egypt-01}~c}). For our main motif we can now have the double-shaded square in \hyperref[egypt-01]{Figure~\ref{egypt-01}~a}. We will construct our pattern using the four-fold axis and axis $BC$ (\hyperref[egypt-03]{Figure~\ref{egypt-03}}). The reader may do this on his own.




\section{The `Lizards' Design}

This design is quite interesting (\hyperref[escher-02]{Figure \ref{escher-02}}). It is essentially a mosaic composed of identical lizards which are densely packed within the space of the pattern (without gaps or overlaps). The mosaic features not only translational but also \emph{rotational} symmetry. The translational symmetry of the pattern is determined by the hexagonal lattice, and the rotational one by the rotation axes at $A, \, B,  \, C,  \, D,  \, E,  \, F,  \, G,  \, H,$ etc. (\hyperref[lizard-02]{Figure~\ref{lizard-02}~a}). The order of the rotational axes depends on the pattern's colouring. For a \emph{three-colour} pattern (lizards of three different colours) all the rotation axes are two-fold (\hyperref[lizard-02]{Figure~\ref{lizard-02}~b}). A \emph{single-colour} pattern, apart from two-fold axes, has three- and six-fold rotation axes (\hyperref[lizard-02]{Figure~\ref{lizard-02}~c}). The  `Lizards' have no mirror The elements of symmetry for the single-colour design are given in \textbf{\#~16} in \hyperref[17-types]{Figure~\ref{17-types}}, and for the three-colour one in \textbf{\#~2}.

\begin{figure*}[!ht]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-03/pattern-03.jpg}
\caption{Constructing the \emph{Lizard} design by M. C. Escher.}
\label{lizard-02}
\end{figure*}



In constructing the single-colour  `Lizards', we can select as the main
motif the triangle $AGH$ in \hyperref[lizard-02]{Figure~\ref{lizard-02}~c}. It is easily seen that the triangle includes parts of one lizard and its area is equal to that occupied by one
lizard. The pattern can be constructed using the six-fold rotation axes ($A$)
and the three-fold rotation axis ($H$). We will now turn the design in
\hyperref[lizard-02]{Figure~\ref{lizard-02}~c}) through \ang{60} about $A$, and then (after a complete turn) we will turn the resultant design about $H$ through \ang{120}.

In the case of the three-colour version the main motif is specified not by $AGH$ but by triangle $ABC$ that includes components of all three multicoloured lizards (\hyperref[lizard-02]{Figure~\ref{lizard-02}~b})). The pattern can be obtained using the two-fold rotation axes that pass, for example, through points $D, \, E,$ and $F$.                                 

