% !TEX root = tarasov-symmetry.tex






\chapter{The Ozma Problem}

\marginnote{Assume we have  already established fluent communication with Planet X. How can we communicate to Planet X our meaning of left and right? Although an old problem, it has not yet been given a name. I propose to call it the Ozma problem. - M. Gardner}

\section{What Is the Ozma Problem?}

In 1900 the American writer of children's books, Lyman Baum, wrote his
famous book \emph{Wonderful Wizard of Oz}. The land of Oz was ruled by
prince Ozma. Another of Baum's characters was a servant called the
Long Eared Hearer who could hear sounds thousands of miles away. In
1960 the American astronomer Frank Drake started a project of using
a powerful radio telescope to search for radio messages from the Galaxy
in hope of picking up signals from intelligent inhabitants of distant
planets. A long-time admirer of Baum and his Oz books, he called his
project Ozma and his radio telescope the Long Eared Hearer. This story
is behind the term the `Ozma problem' suggested by Gardner.

Suppose that we exchange radio messages with inhabitants of some
distant planet. Our signals are certain coded pulses, that is, sequences of
pulses of various intensities. Using the universal laws of mathematical
logic as well as the laws of physics which apply to the entire Universe, we
can arm ourselves with patience and achieve a measure of understanding
with extraterrestrials. If, for instance, we were to transmit a sequence of
numbers representing the masses of nuclei of helium, lithium, beryllium,
boron, carbon, etc., divided by the proton mass, we could expect that the
extraterrestrials would guess that this sequence describes the periodic
system of elements. After all, the ratios of nuclear masses to proton mass
are the same throughout the Universe.

It is rather tempting to convey to the inhabitants of other planets
visual images in the form of plane (two-dimensional) figures. Suppose we
send out a sequence of pulses that is a coded description of the simple
figure shown in Figure~\ref{ozma-1}~(a) - a rectangular figure open in its right side. To
begin with, we ask our extraterrestrial correspondent to prepare
a rectangle divided into twenty square units-five lines with four units per
line. By scanning the figure from top to bottom, left to right (in
accordance with the numbering of units in Figure~\ref{ozma-1}~(a)), we send out the
sequence of pulses shown in Figure~\ref{ozma-1}~(b): a more intensive pulse
corresponds to a darker unit. We ask the distant correspondent to copy
our operations on his rectangle: to scan the units line-by-line left-to-
right and colour them according to the sequence of pulses 
transmitted.

And here emerges a fundamental problem: our correspondent has no
idea as to what we understand by \emph{left}- and \emph{right-handedness}, and so he
does not understand what is meant by ``to scan a line from left to right''. If
he hits upon the right direction of scanning, he will clearly end up with
a contour with a gap in the right side. If he scans the lines in the opposite
direction, he will end up with a contour with a gap in the left side, not in
the right one (Figure~\ref{ozma-1}~(c)). It is unknown in what direction the
extraterrestrial will actually scan his rectangle, and so it is unknown
which (\emph{left} or \emph{right}) figure he obtained.
\begin{figure}[!h]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-14/ozma-1.jpg}
\caption{Asking extra-terrestrials for their sense of left or right.}
\label{ozma-1}
\end{figure}
To be sure, to explain our concept of handedness it would be more
convenient to transmit some kind of object that possesses reflection
asymmetry, for instance, a right screw. This is, however, absolutely
impossible, since we can only make use of radio communication. He
might be asked to look at some asymmetric constellation in the skies. But
constellations do not look the same when observed from Earth and when
observed from some distant planet.

There is thus no asymmetric object, no asymmetric structure which we
could examine together with our correspondent from space. And so the
question presents itself: \emph{under these conditions, is there any way of getting
across to the planet our concept of left and right?} It is this that is known as
the Ozma problem. It is one of the most challenging and exciting
problems of communication theory. This was conceived long before
humanity began speculating about contacts with extraterrestrial
civilizations.

\section{The Ozma Problem Before 1956}

If we had to explain the meaning of left and right to an Earth dweller, not
an extraterrestrial, we would only have to say that rotation from left to
right corresponds to the motion of the hands of a clock. But for an
extraterrestrial this explanation will not do. There is no knowing the
direction in which the hands (if any) of an extraterrestrial clock rotate.

Many natural compounds are known that turn the polarization plane
of a light beam passed through them always in the same direction: to the
right or to the left. This is because some compounds occur naturally on
Earth only in the form of \emph{certain} (left or right) \emph{stereoisomers}. One can
expect, however, that under extraterrestrial conditions these compounds
occur in the form of other stereoisomers than those of Earth.

The animate world abounds in helices of \emph{either handedness} (see
Chapter~7). A wide variety of biological spirals are, however, of no help
here. After all, the fact that all living things on Earth have their DNA
molecules twisted only to the right, by no means suggests that in
extraterrestrial beings they form right helices also.

The many manifestations of mirror asymmetry are not sufficient to
solve the Ozma problem. \emph{It is necessary for the vertical asymmetry to
manifest itself in the very laws of nature}. Physicists have long thought that
all the natural laws, without exception, are invariant under mirror
reflection. This amounts to admitting that the Ozma problem is
insolvable in principle.

Handling one problem in particle physics, the American physicists Lee
and Yang in 1956 put forward the hypothesis that \emph{space parity fails in
processes of particle decay}. Chien-Shiung Wu staged an experiment to test
the Lee-Yang hypothesis. The upheaval came on 15 January 1957 when it
was reported that in particle decays the laws of nature are not invariant
under reflection.

\section{The Mirror Asymmetry of Beta-Decay Processes}

\begin{marginfigure}
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-14/beta-decay-mirror-asymm.jpg}
\caption{The mirror asymmetry in $\beta$-decay of Cobalt (\ce{Co^{60}}).}
\label{beta-mirror}
\end{marginfigure}

The Wu experiment studied the beta-decay of radioactive 
\begin{equation*}%
\ce{Co^{60}}: \ce{Co^{60}}: \to \ce{Ni^{60}}: + e^{-} + \bar{\nu}_{e}
\end{equation*}
Let $\vec{?}$ be the intrinsic (spin) angular momentum of the
cobalt nucleus, $\vec{p}_{e}$ is the momentum of the electron born in the decay, $\theta$ is
the angle between $\vec{?}$ and $\vec{p}_{e}$ (Figure~\ref{beta-mirror}~(a)). Figure~\ref{beta-mirror}~(b) depicts the reflection of the process of Figure~\ref{beta-mirror}~(a) in mirror S. Recall that the angular
momentum $\vec{?}$ is an \emph{axial} vector, and the momentum $\vec{p}_{e}$ is a \emph{polar} vector;
therefore, $\vec{?}$ and pe are reflected in a \emph{different way} (see Chapter~10 and
Figure~\ref{vector-addition-2}). As a result, the angle between the vectors changes. If before the
reflection it was $\theta$, then after the reflection it will be $\ang{180} - \theta$ (Figure~\ref{beta-mirror}~(b)).

The idea of the Wu experiment is fairly simple. In the decay of a \ce{Co^{60}}
nucleus, an electron may fly off at any angle $\theta$ to the direction of the
nuclear angular momentum. If the beta-decay process is invariant under
reflection, then the probability for the electron to be shot out at angle
$\theta$ must equal that for $\ang{180} - \theta$, since the processes in which an electron is
emitted at $\theta$ and at  $\theta$ are \emph{mirror reflections of each other}. If these
probabilities turn out to be different, then we have a violation of the
invariance of the process under mirror reflection. Accordingly, it is
necessary to measure and compare the above-mentioned probabilities.

To measure the probability for the electron to be shot out at one angle or
another, it is necessary to consider the decay of a sufficiently large
number of  \ce{Co^{60}} nuclei. Under normal conditions spin momenta of nuclei
are oriented chaotically; in this case it was only required that for the
majority of nuclei in a sample these momenta be oriented in a certain
direction. It was this that constituted the main practical difficulty. To this
end, Madam Wu cooled the cobalt sample nearly to the absolute zero of
temperature ($T< \SI{0.03}{\kelvin}$) to reduce all the joggling of its molecules
caused by heat and placed it in a strong magnetic field. As a result, most
nuclei in the sample aligned with the applied field. It then remained only
to measure and compare the numbers of electrons emitted \emph{along} and
\emph{opposite the field} ($N_{1}$ and $N_{2}$, respectively). If there was mirror symmetry,
these numbers had to be equal. However, the experiment indicated
convincingly that $N_{2} > N_{1}$. It turned out that the \emph{probability of an
electron to be shot out against the nuclear spin is larger than along this
direction}. Thereby it was proved that the beta-decay of \ce{Co^{60}} nuclei has
no mirror asymmetry. Later, other experiments were carried out to study
other beta-decay processes. In all of them the invariance of natural laws
under mirror reflection (or $P$-invariance) was found to be violated.

\section{The Mirror Asymmetry in Decay Processes and the Ozma Problem}

Now, it seemed the Ozma problem was solved. So, to explain to an
extraterrestrial the meaning of left and right, we can do the following. We
may ask him to manufacture a solenoid, place into it a cooled sample of
radioactive cobalt and to count the electrons emerging from either
solenoid end. We will then ask our space correspondent to align the
solenoid so that his eye looks along its axis in the direction of the
maximal emission of electrons (Figure~\ref{ozma-2}). In this case, the direction of the
motion of electrons along the coils will be from left to right (as the hands
of a terrestrial clock).

\begin{figure}[!h]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-14/ozma-2.jpg}
\caption{Solving the left-right problem by looking at maximal emission of electrons.}
\label{ozma-2}
\end{figure}

Note that here the direction of motion of conditional positive charges
in the conductor will correspond to the direction from right to left, and

according to the right-handed screw, the ordering magnetic field (and
hence the vector of the spin moment of cobalt nuclei) will be directed to
the observer.

\section{The Fall of Charge-Conjugation Symmetry}

$CPT$ invariance (see Chapter~13) suggests that if any of the three
invariances (here $P$-invariance) is violated, then at least one more
invariance must be violated as well. It turned out that \emph{apart from mirror
symmetry, charge-conjugation symmetry (C-\emph{invariance}) must also be
violated}. In other words, nature's laws show noninvariance not only
under replacement of left to right, but also under replacement of particles
by antiparticles.

We will illustrate this by turning to the case of muon neutrino and
muon antineutrino. As noted (Chapter~12), the neutrino is like a left-
handed helix, and the antineutrino, like a right-handed one. It is easily
seen that this model of neutrino presupposes a violation both of
$P$-invariance and $C$-invariance.
\begin{marginfigure}
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-14/cp-violation.jpg}
\caption{The decay of a pion and its results.}
\label{cp-violation}
\end{marginfigure}
We will write the decay process for a pion $\pi^{+}$ in the form that takes
into account the left-handedness of neutrino:
\begin{equation}%
\pi^{+} \to \mu^{+}_{L} + \nu_{\mu L}
\label{pion-decay}
% eq 14.1
\end{equation}
Since in the decay of a pion the antimuon and the neutrino are shot out
in opposite directions (in the rest frame for the pion), then because of
angular momentum conservation, both particles must appear as helices
of the same handedness-in this case as left-handed helices (Figure~\ref{cp-violation}). If
the process possessed a mirror symmetry, then apart from the decay
according to \eqref{pion-decay}, since in mirror reflection left-handed helices turn into
right-handed ones, we would also have the decay process
\begin{equation*}%
\pi^{-} \to \mu^{+}_{R} + \nu_{\mu R}
\end{equation*}
But such a decay is impossible, since the neutrino may only be a \emph{left-handed} helix. If the charge-conjugation symmetry were not violated, then
in addition to \eqref{pion-decay} we would have
\begin{equation*}%
\pi^{-} \to \mu^{+}_{\bar{L}} + \bar{\nu}_{\mu R}
\end{equation*}
which is also impossible, since the antineutrino is a \emph{right-handed} helix.
Curiously enough, if we carry out a mirror reflection and
a particle-to-antiparticle replacement \emph{simultaneously}, then instead of \eqref{pion-decay}
we will have the decays
\begin{equation*}%
\pi^{-} \to \mu^{+}_{\bar{R}} + \bar{\nu}_{\mu R}
\end{equation*}
which are actually observed. This example may be used as an illustration
of the interesting idea put forward in 1957 by the Soviet physicist
L. Landau (1908-1968) and independently by Lee and Yang. They
suggested that the so-called \emph{combined parity} ($CP$-parity or a product of $C$-
and $P$-parities) is conserved.

\section{Combined Parity}

Let us return to the beta-decay of Cobalt-60 nuclei in an external
magnetic field. It was shown in Figure~\ref{beta-mirror} how the process is affected by
mirror reflection. In addition to mirror reflection, Figure~\ref{beta-decay2} also takes
into account the replacement of the particles by their antiparticles. This
figure includes four positions. $A$ is the initial position in which the spin
moment of the cobalt nucleus ($vec{M}$) is aligned with the external magnetic
field ($\vec{H}$ is the magnetic field intensity). $B$ is the reflection of $A$ in plane $S$;
this leaves the direction of $vec{M}$, just like that of $vec{H}$ (both are \emph{axial} vectors),
unchanged, whereas the direction of $\vec{p}_{e}$ (\emph{polar} vector) changes. As a result
the angle $\theta$ between $vec{M}$ and  $\vec{p}_{e}$ in the initial position becomes $\ang{180} ? \theta$ in
$B$. We have already discussed this.
\begin{figure}[!h]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-14/beta-decay-anti-particles.jpg}
\caption{Looking at $\beta$-decay with antiparticles and mirror symmetry.}
\label{beta-decay2}
\end{figure}
Now suppose that we replace the particles by their respective
antiparticles: the cobalt nuclei are replaced by cobalt antinuclei, the
electrons are replaced by positrons. Unlike a nucleus, an antinucleus has
a negative electric charge, and so an external magnetic field will align the
spin moments $vec{M}$ in the opposite direction to $vec{H}$ (see $?$ in Figure~\ref{beta-decay2})
Reflecting in $S$ gives $D$.

It is easily seen that if we carry out \emph{simultaneously} the two
transformations - \emph{mirror reflection} and \emph{particle-to-antiparticle
replacement} - we obtain either the transition $A=D$ or $B=C$. This leaves
the angle between $vec{M}$ and  $\vec{p}_{e}$ unchanged, or leaves invariant the decay
process in question. This invariance is known as \emph{combined} or
\emph{CP-invariance}. Accordingly, they attest to the \emph{conservation of combined
parity (CP-parity)}.

Conservation of combined parity implies that the laws of nature
remain unchanged not when we venture into the looking-glass land, and
not when we venture into the antiworld, but when we cross over into the
\emph{looking-glass antiworld}. Put another way, the laws of physics turn out to
be symmetrical under reflection in that imagined mirror, which at the
same time effects a replacement of particles by their respective
antiparticles (and antiparticles by particles) In the words of the Soviet
physicist Ya. Smorodinsky, 
\begin{quote}
some test will be passed with the same result both by a left-handed screw made from matter and a right-handed helix made from antimatter.
\end{quote}

From the conservation of combined parity, in decay experiments the
\emph{mirror asymmetry comes from the fact that in our comer of the Universe
particles outnumber antiparticles markedly}. Our world can be said to be
asymmetrical with respect to mirror reflection only because it is
asymmetrical in terms of the density of particles and antiparticles.
Imagine that in the world the density of particles and antiparticles were
the same (one has to suppose at the same time that the processes of
annihilation of particles and antiparticles are forbidden in some sort of
way). If that had been the case, Wu would have obtained a symmetrical
result. In fact, in an external magnetic field half the nuclei (neutrons and
protons) in the cooled sample of cobalt (anticobalt) would tend to align
with the field, and half the nuclei (antineutrons and antiprotons) against
the field. In this case, we would have an equal mixture of positions $A$ and
$C$ in Figure~\ref{beta-decay2}. Clearly, reflecting yields the same mixture.

\section{Combined Parity and the Ozma Problem}

Since the laws of nature do not enable us to distinguish a \emph{left-handed helix
of matter and a right-handed helix of antimatter}, this suggests that the
violation of spatial parity in decays of particles by no means solves the
Ozma problem. It is of no help to attempt to use the Wu experiment to
explain to an extraterrestrial our understanding of left and right, if we do
not know a priori which he is made of, matter or antimatter. If he lives in
an antiworld, then by repeating the Wu experiment and using our
explanations, he would consider right what we consider left. The fact is
that in an antiworld the solenoid coils carry positrons, not electrons, the
electric charge of nuclei is negative, not positive.

We can imagine the following fantastic situation. We have agreed with
our extraterrestrial to meet in space and set out to our rendezvous. And
so, having put on our space-suits, we walk out of our ships and move
toward each other. You extend your right hand and suddenly you see
that he, aware of the terrestrial custom of handshaking, extends his left
hand, not his right one. Do not touch it because you are facing a dweller
of an antiworld.

To sum up, the conservation of combined parity gives the Ozma
problem a new perspective. \emph{To solve this problem we will have to find out
beforehand whether our companion is made of matter or antimatter.}

\section{The Solution to the Ozma Problem}

To understand with which world (a conventional or antiworld) we are in
contact, apart from radio signals we can make use of the neutrino
communication channel. Our Sun is a source of neutrino; that is. of left-
handed helices. It is, therefore, in principle sufficient to send to our
distant correspondent a solar neutrino and ask him to compare its
handedness with that of the neutrino sent out by the luminary in the
world of the extraterrestrial. Unfortunately, we at present cannot even
think of a way of sending our neutrino to a distant planet.

And still there is a solution to the Ozma problem. Leaving out details,
which can be far too complicated for this book, we note that physicists
have revealed that in one decay \emph{CP-parity is not conserved}. Considering
\emph{CPT-invariance}, this means that in this process \emph{T-parity must also not be
conserved} (that is, the symmetry with respect to time reversal must be
violated). As it has been noted above, there are two types of \emph{neutral
kaons} - long-lived kaons ($K_{l}$) and short-lived kaons ($K_{s}$). The former live
about \SI{d-8}{\second}, the latter, \SI{d-10}{\second}. In accordance with the conservation of
combined parity, kaons Xi decay into three pions, and kaons Ks into two
pions (the decay schemes are given in the table ``Elementary particles'').
In 1964 it was found that every now and then (in about one case out of
1000) a kaon $K_{l}$ decays into two pions, not three. This suggested that \emph{for
kaons the law of conservation of CP-parity is not completely exact}. It is this
additional violation of symmetry in the laws of physics that enables, in
principle, the Ozma problem to be solved, or rather to find of which
material the extraterrestrial correspondent consists, matter or antimatter.

We will have to ask him to observe the process of decay of neutral
kaons, for example, to measure the density of the kaons K in the beam at
various distances from the place of origin of the kaons and to report the
results of his measurements. If the report comes from antiworld, then it
will differ from the results of our measurements.

%
%\begin{figure*}[!h]
%\centering
%\includegraphics[width=0.9\textwidth]{figs/ch-04/regular-polyhedra.png} 
%\caption{The five possible types of regular polyhedra. \label{regular-poly}} 
%\end{figure*}
%
%\begin{marginfigure}
%\centering
%\includegraphics[width=0.9\textwidth]{figs/ch-04/dodecahedron-symm.png}
%\caption{The symmetry of dodecahedron.}
%\label{dodeca-symm}
%\end{marginfigure}
%
%
%\begin{figure}[!h]
%\centering
%\includegraphics[width=0.75\textwidth]{figs/ch-04/kepler.png}
%\caption{Scheme of the Solar System based on the regular polyhedra devised by Kepler.}
%\label{kepler-scheme}
%\end{figure}
%
%
%
%                        