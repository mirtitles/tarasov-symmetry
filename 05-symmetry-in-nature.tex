% !TEX root = tarasov-symmetry.tex






\chapter{Symmetry In Nature}


\section{From the Concept of Symmetry to the Real Picture of a Symmetrical World}

We\marginnote{We like to look at symmetrical things in nature, such as perfectly symmetrical spheres like planets and the Sun, or symmetrical crystals like snowflakes, or flowers which are nearly symmetrical. - R. Feynman} have seen that the concept of symmetry has often been used by scientists through the ages as a guiding star in their speculations. Recall the Pythagoreans who concluded that the Earth is spherical and moves over a sphere. Pythagoras's ideas were used by the great Polish astronomer Copernicus when he was working on his theory of the Solar system. According to Copernicus, celestial bodies are spherical because the sphere is 'a perfect, comprehensive shape, having no corners, the most capacious'. He wrote: 
\begin{quote}
All bodies tend to assume that form; this can be noticed in water droplets and other liquid bodies.
\end{quote}
Here Copernicus meant free falling drops, which are known to take on a nearly spherical shape. In actual fact, he anticipated the deep analogy between a water drop falling under gravity and the Earth falling (or rather orbiting) in the gravitational field of the Sun.

On the other hand, scholars of earlier times were inclined to exaggerate a bit the role of symmetry in the picture of the world. They sometimes forced their admiration for symmetry on nature by artificially squeezing nature into symmetrical models and schemes. Recall Kepler's scheme based on the five regular polyhedra.

The modern picture of the world, with its rigorous scientific justification, differs markedly from earlier models. It excludes the existence of some `centre of the world' Gust as some magic power of the Platonic solids) and treats the Universe in terms of the \emph{unity of symmetry and asymmetry}. Observing the chaotic mass of stars in the skies, we understand that beyond the seeming chaos are quite symmetrical spiral structures of galaxies, and in them symmetrical structures of planetary systems. This symmetry is illustrated in \hyperref[galaxy-symm]{Figure~\ref{galaxy-symm}} showing the Galaxy and a magnified and simplified scheme of the Solar system.

\begin{figure}[!h]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-05/galaxy.jpg}
\caption{Symmetry of the Galaxy and the Solar System.}
\label{galaxy-symm}
\end{figure}


The nine planets move around the Sun in their elliptical orbits which are nearly circular. The planes of the orbit (save for Pluto) within high accuracy coincide with the Earth's orbital plane, the so-called ecliptic. For example, Mars's orbit forms an angle of \ang{2} with the \emph{ecliptic}. Also coinciding with the ecliptic are the planes of all the 13 satellites of the planets, including the Moon.

Even more than in the picture of the universe, symmetry manifests itself in an infinite variety of structures and phenomena of the inorganic 'world and animate nature.


\section{Symmetry in Inanimate Nature. Crystals}

When we look at a heap of stones on a hill, the irregular line of mountains on the horizon, meandering lines of river banks or lakeshores, the shapes of clouds, we may think that symmetry in the inorganic world is rather rare. At the same time, it is widely believed that symmetry and strict order are hostile to living things. It is no wonder that the lifeless castle of the Snow Queen in the fairy tale by Hans Christian Andersen is often pictured as a highly symmetrical structure shining with polished mirror faces of regular shape. Who is right then? Those who view inanimate nature as a realm of disorder, or, on the contrary, those who see in it the predominance of order and symmetry?

Strictly speaking, both schools are wrong. To be sure, such natural factors as wind, water, and sunlight affect the terrestrial surface in a high- 1y random and disorderly manner. However, sand dunes, pebbles on the seashore, the crater of an extinct volcano are as a rule regular in shape. Of course, a heap of stones is rather disorderly, but each stone is a huge colony of crystals, which are utterly symmetrical structures of atoms and molecules. \emph{It is crystals that make the world of inanimate nature so charmingly symmetrical.}

\begin{figure}[!h]
\centering
\includegraphics[width=0.85\textwidth]{figs/ch-05/snowflakes.jpg}
\caption{Snowflakes have six-fold rotational symmetry.}
\label{snowflake-symm}
\end{figure}


Inhabitants of cold climates admire \emph{snowflakes}. A snowflake is a tiny crystal of frozen water. Snowflakes are of various shapes, but each has a six-fold rotational symmetry and also a mirror symmetry (\hyperref[snowflake-symm]{Figure~\ref{snowflake-symm}}).



All solids consist of \emph{crystals}. Individual crystals are generally tiny (less than a grain of sand), but in some cases they grow to considerable sizes, and then they appear before us in all their geometrical beauty. Some naturally grown crystals are given in \hyperref[crystal-symm]{Figure~\ref{crystal-symm}}. It can be seen from the figure that crystals are polyhedra of fairly regular shapes having plane faces and straight edges. The figure shows \emph{topaz} (aluminium fluosilicate), \emph{beryl} (beryllium aluminium silicate), \emph{smoky quartz} (silicon dioxide).

\begin{figure}[!h]
\centering
\includegraphics[width=0.85\textwidth]{figs/ch-05/crystals.jpg}
\caption{Some naturally grown crystals, notice the plane faces and straight edges.}
\label{crystal-symm}
\end{figure}


The beryl crystal shown in the figure is \emph{heliodor}, one of the crystalline varieties of that compound. Other varieties are \emph{aquamarine} (blue-green), \emph{emerald} (green), and \emph{vorobyevite} (pale red). The colour is conditioned by impurities. So, the yellow of heliodor is due to \ce{Fe3+} impurities. Quartz comes in a variety of forms. The clearest and transparent variety of quartz is \emph{rock crystal}, the second clearest is \emph{smoky quartz}, shown in the figure. There are also \emph{violet amethyst, red sardonyx, black onyx,} and \emph{grey chalcedony}. Quartz is also grindstone, flint, and common sand.

\begin{marginfigure}
 \centering
  \includegraphics[width=0.7\textwidth]{figs/ch-05/crystal-symm-01.jpg}
  \caption{The symmetry of some common crystals.}
  \label{crystal-symm-01}
\end{marginfigure}


The symmetry of crystals is clearly seen in \hyperref[crystal-symm-01]{Figure~\ref{crystal-symm-01}}: \begin{enumerate*}[label=(\alph*)]
\item \emph{common salt}, 
\item \emph{quartz}, and 
\item \emph{aragonite}.
\end{enumerate*}
The latter is one of the naturally occurring varieties of \emph{calcite} ($CaCO_{3}$). 
\begin{figure}[!h]
\centering
\includegraphics[width=0.85\textwidth]{figs/ch-05/crystal-symm-02.jpg}
\caption{The symmetries of crystalline forms of diamond.}
\label{crystal-symm-02}
\end{figure}

\hyperref[crystal-symm-02]{Figure~\ref{crystal-symm-02}} represents three crystalline forms of \emph{diamond}: 
\begin{enumerate*}[label=(\alph*)]
\item octahedron, 
\item rhombic dodecahedron, and 
\item hexagonal octahedron.
\end{enumerate*}
The \emph{outer symmetry} of a crystal comes from its inner symmetry, that is, its ordered arrangement of atoms (molecules) in space. In other words, the symmetry of a crystal is related to the existence of the space lattice of atoms - the so-called \emph{crystalline lattice}.

\section{Symmetry in the World of Plants}

In his book \emph{The Ambidextrous World}, M. Gardner writes: 
\begin{quote}
On the earth life started out with spherical symmetry, then branched off in two major directions: the plant world with symmetry similar to that of a cone, and the animal world with bilateral symmetry.\footnote[][-1cm]{The term `bilateral symmetry' is widely used in biology. It means mirror symmetry.}
\end{quote}

\begin{marginfigure}%[!h]
\centering
\includegraphics[width=0.85\textwidth]{figs/ch-05/tree-cone.jpg}
\caption{The conical symmetry of trees.}
\label{tree-cone}
\end{marginfigure}

The \emph{symmetry of a cone}, which is characteristic of plants, is seen essentially in any tree (\hyperref[tree-cone]{Figure~\ref{tree-cone}}). The root of a tree absorbs water and nutrients from the soil, that is, from \emph{below}, whereas all the other vital functions occur in the canopy, that is, \emph{above} the ground. Therefore, the directions `upwards' and `downwards' for the tree are significantly different. At the same time the direction in a plane perpendicular to a vertical are essentially indistinguishable: from every quarter the tree receives air, light, and water in equal measure. As a result, we have a vertical rotation axis (the cone's axis) and vertical planes of symmetry. Note that the vertical orientation of the cone's axis, which characterizes the symmetry of a tree, is determined by the direction of gravity\footnote[][1cm]{Biological experiments on board the Soviet orbital station Salyut-6 have shown that under weightlessness, the spatial orientation of sprouts, leaves, and root of wheat and peas is violated.}. That is why the general orientation of the stem of a tree is normally independent of the slope of the ground or the Sun's altitude in a given latitude.

True, trees can be encountered now and then, such that their stems are not just non-vertical, but bent in a snake-like manner, and their canopy may be lop-sided. It would seem that any symmetry here is out of the question. And still the concept of a cone at all times correctly reflects the nature of the symmetry of the tree, its essence. After all, for any tree we can indicate the \emph{base} and the \emph{top}, and at the same time for the tree the notions of left and right, back and front are invalid.


\begin{figure*}[!h]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-05/leaves.jpg}
\caption{Symmetry displayed in leaves and flowers.}
\label{leaves-symm}
\end{figure*}

Remarkable symmetry is inherent in leaves, branches, flowers, and fruit. \hyperref[leaves-symm]{Figure~\ref{leaves-symm}} gives examples of mirror symmetry, which is characteristic of leaves, although is also encountered in flowers, which are generally described by rotational symmetry. 

\hyperref[john-wort]{Figure~\ref{john-wort}~a}  depicts a flower of \emph{St. John's wort (Hypericum)}, which has a five-fold rotation axis and no mirror symmetry. In flowers, rotational symmetry is often accompanied by mirror symmetry (\hyperref[john-wort]{Figure~\ref{john-wort}~b}). An \emph{acacia} leaf, shown in \hyperref[leaves-symm-02]{Figure~\ref{leaves-symm-02}~a}, has both mirror and translational symmetries.  And a \emph{hawthorn} branch (\hyperref[leaves-symm-02]{Figure~\ref{leaves-symm-02}~b}) can be seen to have a glide axis of symmetry.

\begin{marginfigure}%[!h]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-05/flowers-01.jpg}
\caption{Five-fold rotational symmetry of a flower of \emph{St. John's wort (Hypericum)}, and mirror symmetry.}
\label{john-wort}
\end{marginfigure}


\hyperref[plant-symm-01]{Figure~\ref{plant-symm-01}} shows a wild flower known as silverweed (\emph{Potentilla anserina}). The flower has a five-fold rotation axis and five planes of symmetry. The high degree of orderliness in the arrangement of individual leaves on stems imparts to the figure some likeness to borders discussed above.


\begin{figure}[!h]
\centering
\includegraphics[width=0.7\textwidth]{figs/ch-05/leaves-02.jpg}
\caption{The symmetry of leaves.}
\label{leaves-symm-02}
\end{figure}


\begin{figure*}[!h]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-05/plant-01.jpg}
\caption{The symmetry in the silverweed wildflower \emph{Potentilla anserina}.}
\label{plant-symm-01}
\end{figure*}

In the rich world of flowers rotation axes of symmetry of various orders occur. But the most widespread is the five-fold rotational symmetry. This symmetry is to be found in many wild flowers (\emph{bluebell, forget-me-not, geranium, stellaria, pink, St. John's wort, silverweed}, etc.), in fruit tree flowers (\emph{cherry, apple, pear, mandarin,} etc.), in flowers of fruit and berry plants (\emph{strawberry, blackberry, raspberry, guelder rose, bird-cherry, rowan-tree, hawthorn, dog-rose,} etc.), and in some. garden flowers (\emph{nasturium, phlox},etc.). It is sometimes argued that plants' `love', which is known to be impossible in principle in periodic structures, can be explained as a safeguard of the plant's individuality. Academician N. Belov maintains ``that the five-fold axis is a sort of tool in the struggle for existence, an insurance against petrification, the first stage of which would be `catch' by a lattice.''


\section{Symmetry in the World of Animals}

The five-fold rotational symmetry also occurs in the world of animals. Examples are the \emph{starfish} and the \emph{urchin} (\hyperref[starfish]{Figure~\ref{starfish}}). Unlike the world of plants, however, rotational symmetry is rare in the world of animals. As a matter of fact, we may find it only in some denizens of the sea.

\begin{figure*}[!h]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-05/starfish.jpg}
\caption{The five-fold rotational symmetry in the Starfish and Sea Urchin.}
\label{starfish}
\end{figure*}


Insects, fishes, birds and other animals generally exhibit a difference between forward and backward directions, which is incompatible with rotational symmetry. The Push-Pull invented in a famous Russian fairy tale (\hyperref[animal-symm-01]{Figure~\ref{animal-symm-01}}) is a striking animal in that its front and rear are absolutely symmetrical. The \emph{direction of motion} is an essentially distinguishable direction, about which no animal is symmetrical. In that direction an animal moves for its food and escapes from danger.

\begin{figure*}[!h]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-05/animal-symmetry-01.jpg}
\caption{The Push-Pull, a mythical animal whose front and rear are absolutely symmetrical.}
\label{animal-symm-01}
\end{figure*}


The symmetry of living creatures is also dictated by another
direction - the \emph{direction of gravity}. Both directions are significant, since
they define the \emph{plane of symmetry} of a creature (\hyperref[animal-symm-02]{Figure~\ref{animal-symm-02}}) . \emph{Bilateral (mirror)
symmetry is characteristic of nearly all members of the animal kingdom.}

\begin{figure*}[!h]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-05/animal-symmetry-02.jpg}
\caption{The bilateral plane of symmetry for different animals.}
\label{animal-symm-02}
\end{figure*}


This symmetry is especially apparent in a \emph{butterfly} (\hyperref[butterfly]{Figure~\ref{butterfly}}). The symmetry of left and right is present here with nearly mathematical accuracy.

\begin{figure}[!h]
\centering
\includegraphics[width=0.85\textwidth]{figs/ch-05/butterfly.jpg}
\caption{The bilateral symmetry of a butterfly.}
\label{butterfly}
\end{figure}


It can be said that any animal consists of two \emph{enantiomorphs} - its left and right halves. Also enantiomorphs are paired organs, one of which is in the right and the other in the left half of the body, such as ears, eyes, horns, and so on.


\section{Inhabitants of Other Worlds}

Many works of science fiction discuss the possible appearances of visitors from other planets. Some authors believe that extraterrestrials may differ markedly in their appearance from `earthlings'; others, on the contrary, believe that intelligent creatures throughout the entire Universe must be very much alike. The question concerns us only in the context of symmetry. Whatever the extraterrestrial looks like, his appearance must exhibit bilateral symmetry, because on any planet a living creature must have a distinguishable direction of motion and on any planet there is gravity. The extraterrestrial may be like a dragon from some fairy tale, but not like a Push-Pull, by no means. He cannot be left-eyed or right- eared. He must have an equal number of limbs on either side. Symmetry requirements reduce drastically the number of possible versions of the extraterrestrial's appearances. And although we cannot say with certainty what that appearance \emph{must be}, we can say what it \emph{cannot be}. Recall the idea expressed in Chapter 4: \emph{symmetry limits the diversity of structures possible in nature}.


%
%\begin{figure*}
%\centering
%\includegraphics[width=0.9\textwidth]{figs/ch-04/regular-polyhedra.jpg} 
%\caption{The five possible types of regular polyhedra. \label{regular-poly}} 
%\end{figure*}
%
%\begin{marginfigure}
% \centering
%  \includegraphics[width=0.9\textwidth]{figs/ch-04/dodecahedron-symm.jpg}
%  \caption{The symmetry of dodecahedron.}
%  \label{dodeca-symm}
%\end{marginfigure}
%
%
%\begin{figure}
%\centering
%\includegraphics[width=0.75\textwidth]{figs/ch-04/kepler.jpg}
%\caption{Scheme of the Solar System based on the regular polyhedra devised by Kepler.}
%\label{kepler-scheme}
%\end{figure}
%
%
%
%                        