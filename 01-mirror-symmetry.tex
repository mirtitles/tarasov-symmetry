% !TEX root = tarasov-symmetry.tex






\chapter{Mirror Symmetry }


\section{An Object and Its Mirror Twin} \marginnote{What can be more like my hand or my ear than their reflections in a mirror!
 And still the hand I see in the mirror cannot substitute for my real hand.\\
I. Kant}

\hyperref[triangle-mirror]{Figure \ref{triangle-mirror}} shows a simple example of an object and its `\emph{looking-glass twin}' - triangle $ABC$ and triangle $A_{1}B_{1}C_{1}$ (here $MN$ is the intersection of the mirror plane with the plane of the drawing). For each point on the triangle there is a corresponding point on the mirror twin. These points lie on the same perpendicular to straight line MN, on either side and at the same distance from it. For simplicity, the object in the figure is taken to be two-dimensional. In the general case, the object (and hence its mirror twin) are three-dimensional.

\begin{marginfigure}
\includegraphics[width=\textwidth]{figs/ch-01/triangle-symm.jpg}
\caption{Looking at symmetry in a mirror reflection of a triangle, its looking glass twin.}
\label{triangle-mirror}
\end{marginfigure}

We all know that a mirror twin can be seen readily. Suffice it to place an illuminated object in front of a plane mirror and look into it. It is generally believed that the twin in the mirror is an exact replica of the object itself. But this is actually not so. A mirror does not just duplicate the object but transposes the front and back (in relation to the mirror) parts of the object. In comparison with the object itself, its mirror twin seems to be turned inside out in the direction perpendicular to the plane of the mirror. This is clearly seen in \hyperref[cone-mirror-1]{Figure~\ref{cone-mirror-1} a} and essentially imperceptible in \hyperref[cone-mirror-1]{Figure~\ref{cone-mirror-1} b}. When examining the cones in the figure the reader might disagree with our statement that the mirror reflection is not an exact copy of the object. After all, the object and its reflection in \hyperref[cone-mirror-1]{Figure~\ref{cone-mirror-1} a} only differ in their orientation: they are so arranged that they face each other. This is all the more so for \hyperref[cone-mirror-1]{Figure~\ref{cone-mirror-1}~b}. In this connection let us turn to a more interesting example.

\begin{figure}[!ht]
\includegraphics[width=0.9\textwidth]{figs/ch-01/cone-symm-01.jpg}
\caption{Looking at symmetry of a cone in the mirror reflection.}
\label{cone-mirror-1}
\end{figure}
\begin{figure}[!ht]
\includegraphics[width=0.9\textwidth]{figs/ch-01/cone-symm-02.jpg}
\caption{Looking at rotation of a cone in the mirror reflection.}
\label{cone-mirror-2}
\end{figure}
\begin{figure}[!ht]
\includegraphics[width=0.9\textwidth]{figs/ch-01/cone-symm-03.jpg}
\caption{Looking at spirality of a cone in the mirror reflection.}
\label{cone-mirror-3}
\end{figure}


Suppose that a cone is rotated about its axis (\hyperref[cone-mirror-2]{Figure~\ref{cone-mirror-2}}), the direction of rotation being indicated by the arrow. If the rotation axis is normal to the mirror's plane, the cone retains its direction of rotation when reflected in the mirror (\hyperref[cone-mirror-2]{Figure~\ref{cone-mirror-2}~a}). If the rotation axis is parallel to the mirror, the direction of rotation is reversed in the reflection (\hyperref[cone-mirror-2]{Figure~\ref{cone-mirror-2}~b}). No shifts or turns can now make the object coincide with its reflection. In other words, the rotating cone and its reflection are essentially \emph{different} objects. To obtain a reflection without actually reflecting the cone in the mirror, we will have to reverse the direction of the rotation of the cone.



We can do this without any rotation, though. If we make a \emph{thread} on the cone (\hyperref[cone-mirror-3]{Figure~\ref{cone-mirror-3}}), then the object's thread and the reflection's thread will have different directions: for the object's thread to be driven into wood we will have to rotate its head clockwise, and the reflection's one counterclockwise. The first thread is called \emph{right}, and the second \emph{left}t. We generally use right screws.

We have thus seen that, whatever the amount of similarity, an object and its reflection may be different, \emph{incompatible}. Sometimes the difference is not very conspicuous; for instance, you generally attach no importance to the fact that a birth-mark on your right cheek in your `looking-glass' counterpart appears on the left cheek. In other cases the difference becomes so striking that we can only be surprised that it has been overlooked earlier. It is sufficient to compare a text with its reflection (\hyperref[mirror-text]{Figure~\ref{mirror-text}}). Hold a book up to a mirror and try and read the reflection of the text in it. Or, worse, try and write just one line looking not at the paper but at its mirror reflection.

\begin{figure}[!ht]
\includegraphics[width=0.9\textwidth]{figs/ch-01/mirror-text-01.jpg}
\caption{Looking at reflection of a text.}
\label{mirror-text}
\end{figure}

\section{Mirror Symmetry}
Suppose now that one half of an object is a mirror reflection of the other about a plane. Such an object is said to be \emph{mirror-symmetrical}, and the plane is said to be the \emph{plane of symmetry}. For a two-dimensional (flat) object instead of a plane of symmetry we have an  \emph{axis of symmetry} - the line of intersection of the plane of symmetry with the plane of the object. For a unidimensional (linear) object we have the  \emph{centre of symmetry} - the point of the intersection of the object?s straight line with the plane of symmetry.

\begin{marginfigure}
\includegraphics[width=0.9\textwidth]{figs/ch-01/mirror-symm.jpg}
\caption{Examples of mirror-symmetrical objects.}
\label{mirror-symm-examples}
\end{marginfigure}

\hyperref[mirror-symm-examples]{Figure~\ref{mirror-symm-examples}} gives examples of mirror-symmetrical objects:
\begin{enumerate*}[label=(\alph*),leftmargin=1cm]
\item	unidimensional object ($O$ is the centre of symmetry),
\item two-dimensional object ($MN$ is the axis of symmetry).
\item	three-dimensional object ($S$ is the plane of symmetry).
\end{enumerate*}

\begin{marginfigure}
\includegraphics[width=0.9\textwidth]{figs/ch-01/hexagonal.jpg}
\caption{Symmetries of a hexagon.}
\label{hexagon-symm}
\end{marginfigure}

A unidimensional object has no more than one centre of symmetry. A two-dimensional object may have several axes of symmetry, and a three-dimensional object, several planes of symmetry. So a regular hexagon has six axes of symmetry (the red lines in \hyperref[hexagon-symm]{Figure~\ref{hexagon-symm}}). In Fig. 4 were shown four planes of symmetry of a regular pyramid. The circle has an infinite number of axes of symmetry, just as the sphere, the circular cylinder, the circular cone, and the spheroid.



Let us print on a sheet of paper in capital letters the words \textbf{COOK} and \textbf{PAN}. We will now get a mirror and place it vertically so that the line of the intersection of the mirror's plane with the sheet's plane divides these words in two along the horizontal line. Perhaps some will be surprised at finding that the mirror has not changed the word \textbf{COOK}, whereas it has changed the word \textbf{PAN} beyond recognition ( \hyperref[mirror-text2]{Figure~\ref{mirror-text2}}). This `trick' can be explained very simply. To be sure, the mirror reflects the lower part of both words in a similar fashion. Unlike \textbf{PAN}, however, the word \textbf{COOK} possesses a horizontal axis of symmetry, that is why it is not distorted by the reflection.

\begin{figure}[!ht]
\includegraphics[width=0.9\textwidth]{figs/ch-01/mirror-text-02.jpg}
\caption{Looking at reflection of a text.}
\label{mirror-text2}
\end{figure}

\section{Enantiomorphs}
Suppose that an object is characterized by a \emph{single} plane (axis) of symmetry. Let us cut it along the plane (axis) of symmetry into two halves. These two halves are, clearly, mirror reflections of each other. It is essential that each half in itself is mirror-asymmetrical. The halves are said to be \emph{enantiomorphs}.

Thus, \emph{enantiomorphs are pairs of mirror-asymmetrical objects (figures) that are mirror reflections of each other.} In other words, enantiomorphs are an object and its mirror reflection provided that the object itself is mirror-asymmetrical. Enantiomorphs may be individual objects and halves of an object cut appropriately. To distinguish between enantiomorphs of a given pair they are referred to as left and right. It is immaterial which is called left (right), it is only a matter of convention, tradition, or habit.

Examples of three-dimensional enantiomorphs are given in  \hyperref[enantiomorphs-3d]{Figure~\ref{enantiomorphs-3d}}: 

\begin{enumerate*}[label=(\alph*),leftmargin=1cm]
\item left and right screws,
\item left and right dice,
\item left and right knots,
\item left and right gloves,
\item left and right reference frames,
\item  left and right halves of a chair cut along the symmetry plane.
\end{enumerate*}
Normally used in practice are right screws, left dice, and right reference frames. Left and right gloves and knots occur with equal frequency.

\begin{figure*}[!ht]
\includegraphics[width=0.9\textwidth]{figs/ch-01/enantiomorphs-3d.jpg}
\caption{Examples of three-dimensional enantiomorphs.}
\label{enantiomorphs-3d}
\end{figure*}

Given in  \hyperref[enantiomorphs-2d]{Figure~\ref{enantiomorphs-2d}} are examples of two-dimensional enantiomorphs: 

\begin{enumerate*}[label=(\alph*),leftmargin=1cm]
\item left and right spirals,
\item left and right traffic signs, 
\item  left and right reference frames,
\item left and right halves of an oak leave cut along the axis of symmetry.
\end{enumerate*}


\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\textwidth]{figs/ch-01/enantiomorphs-2d.jpg}
\caption{Examples of two-dimensional enantiomorphs.}
\label{enantiomorphs-2d}
\end{figure}

\emph{Two-dimensional} enantiomorphs cannot be superposed on each other by any translations or turns in the space of these enantiomorphs, that is, in a plane. To make them coincide a turn in the \emph{three-dimensional} space is required, that is, a turn as shown in \hyperref[folding]{Figure~\ref{folding}}. As for three-dimensional enantiomorphs, for them to coincide exactly, turning in a fantastic non-existent four-dimensional space would be required. It is clear that to effect the turn or even to visualize it is impossible. Therefore, for three-dimensional enantiomorphs the following statement is valid: \emph{no translocations or turns can convert a left enantiomorph into its right one, and vice versa}. So a left enantiomorph will always be left and a right one will always be right. Turn your left shoe as you might, it will never fit your right foot. Cast a left die as often as you might, it will never turn into a right die.

\begin{marginfigure}%[!ht]
\centering
\includegraphics[width=\textwidth]{figs/ch-01/folding.jpg}
\caption{Coinciding two-dimensional enantiomorphs via turning in three-dimensional space.}
\label{folding}
\end{marginfigure}

Curiously, to prove the existence of an `other-worldly' four-dimensional world they contrived tricky demonstrations involving conversions of left enantiomorphs into right ones, and vice versa. Such demonstrations were performed at so-called spiritualistic sessions, which were rather fashionable at the turn of the century in some decadent aristocratic circles. It goes without saying that those demonstrations were just adroit tricks based on sleight of hand. For example, the spiritualist would ask a participant in the session to give him his left glove, and after some distracting manipulations, the spiritualist would present to the spectators sitting in a half-dark room an identical right glove. This was claimed to be a proof of the short-term residence of the glove in the other world, where it had allegedly turned into the right glove.

