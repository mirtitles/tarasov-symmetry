% !TEX root = tarasov-symmetry.tex






\chapter{Symmetry and Conservation Laws}

\marginnote{Today it is difficult to find a paper devoted
to fundamental issues of physics that would not make mention of invariance principles and its author would not in his arguments draw on the concepts of the existence of relations between conservation laws and invariance principles. - 	E. Wiegner}

The law of conservation of energy had been used in mechanics before Galileo. So in the late 15th century the great Leonardo da Vinci postulated the impossibility of perpetuum mobile. In his book On True and False Science he wrote: "Oh, seekers of perpetual motion, how many empty projects you have created in those searches." The laws of conservation of momentum and angular momentum were formulated later, in the 17-18th centuries. It was not, however, till the beginning of the 20th century that the laws assumed prominence. The attitude to them changed radically only after it had been discovered that these laws were related to the principles of invariance. Once this relation had been revealed, it became clear that conservation laws are predominant among other laws of nature.


\section{The Relationship of Space and Time Symmetry to Conservation Laws}

The relationship can be formulated as follows:
\begin{enumerate}
\item \emph{The law of conservation of momentum is a consequence of the homogeneity of space}, or rather a consequence of the invariance of the laws of physics under translations in space. Momentum can thus be defined as a physical quantity whose conservation is a consequence of the above-mentioned symmetry of physical laws.
\item \emph{The law of conservation of angular momentum is a consequence of the isotropy of space}, or rather a consequence of the invariance of the laws of physics under rotations in space. Angular momentum is a physical quantity that is conserved as a consequence of the above-mentioned symmetry of physical laws.
\item \emph{The law of conservation of energy is a consequence of the homogeneity of time}, or rather a consequence of the invariance of the laws of physics under translations in time. Energy is a physical quantity that is conserved as a consequence of the above-mentioned symmetry of physical laws.
\end{enumerate}

The three-dimensionality of space predetermines the vector nature of momentum and angular momentum, and so the laws of conservation of momentum and angular momentum are vector laws. The one-dimensionality of time predetermines the scalar nature of energy and the corresponding conservation law.

The relationship of conservation laws to space-time symmetry means that the passage of time or a translation and a rotation in space cannot cause a change in the physical state of the system. This will require that the system interact with other systems the corresponding conservation law.

\section{The Universal and Fundamental Nature of Conservation Laws}

Such a way of looking at conservation laws may appear to be unusual to the uninitiated reader. Significantly, the \emph{conservation laws can be obtained without applying the laws of motion directly from symmetry principles}. The derivation of the law of conservation of momentum from Newton's second and third laws, provided in Chapter 10, should in this connection be regarded as a special consequence.

It follows thus that the \emph{region of applicability} of conservation laws is wider than that of the laws of motion. The laws of conservation of energy, momentum and angular momentum are used both in classical mechanics and in quantum mechanics, whereas Newton's laws of dynamics are out of place in quantum mechanics. Wiegner wrote: 
\begin{quote}
For those who derive conservation laws from invariance principles, it is clear that the region of applicability of these laws lies beyond the framework of any special theories (gravitation, electromagnetism, etc.) that are essentially separated from one another in modern physics.
\end{quote}
Clearly, the region of applicability of conservation laws must be as wide as that of appropriate invariances. This enables one to think of the laws of conservation of energy, momentum and angular momentum as \emph{universal laws}.

The relationship of conservation laws to invariance principles also suggests that any violation of these laws, should it occur, would point to a violation of appropriate invariance principles. So far no experimental evidence is available to indicate that the laws of nature may turn out to be non-invariant under translation in time, and under translation and rotation in space. This circumstance, together with the aforementioned property of universality, makes the laws of conservation of energy, momentum, and angular momentum really \emph{fundamental laws}.

It follows from the fundamental nature of conservation laws that we should select as the \emph{conserved quantities fundamental physical quantities}: energy, momentum, and angular momentum. Note that in classical mechanics these quantities appear as functions of the velocity and coordinates of a body. So the energy, momentum, and angular momentum of a billiard ball can be represented in the form
\begin{equation}%
E = \frac{mv^{2}}{2}, \, \vec{p} = m \vec{v}, \, \vec{M} = (\vec{r} \times m\vec{v}) 
\label{eq-11.1}
%eq 11.1
\end{equation}
whence it follows in particular that
\begin{equation}%
E= \dfrac{p^{2}}{2m}, \,  \vec{M} = (\vec{r} \times \vec{p}) 
\label{eq-11.2}
%eq 11.2
\end{equation}


It might appear that we can conclude from expressions of type \eqref{eq-11.1} that the role of fundamental quantities is played by velocity and coordinates. But if we go over from the classical mechanics of billiard balls to the quantum mechanics of microobjects, the very concept of the velocity of an object will then become unsuitable and expressions \eqref{eq-11.1} become pointless. At the same time the conserved quantities $(E, \, \vec{p}, \, \vec{M})$ retain their meaning both in classical and quantum mechanics. It is important that in quantum mechanics they are, generally speaking, not expressible in terms of each other; the second of \eqref{eq-11.2} does not hold in the microworld, since a microobject has no states in which the values of momentum and coordinates are specified simultaneously. As regards the first of \eqref{eq-11.2}, it is valid only for the free motion of a microobject. For a bound microobject (for instance, an atomic electron) the energy is quantized, with the result that for each energy level we cannot indicate a definite value of momentum. Quantum mechanics, thus, actually makes it possible to bring out the fundamental nature of conserved quantities, their independence in full conformity with the fundamentality and independence of the corresponding types of symmetry of the laws of physics.

The universality of conservation laws suggests that the conserved physical quantities are used in \emph{various branches of physics}. They can be described by various expressions into which enter physical quantities characteristic of a given field. Consider momentum, for example. We will write out the four expressions for the momentum
\begin{align*}%
\vec{p} & = m\vec{v}; \\
\vec{p} & = \dfrac{m \vec{v}}{\sqrt{1 - \dfrac{v^{2}}{c^{2}}}} \\
\vec{p} & = \dfrac{1}{c^{2}} (\vec{E} \times \vec{H})\\
\vec{p} & = \dfrac{\hbar \omega }{c} \vec{n}
\end{align*}
The first expression describes the momentum of a body with mass $m$ and speed $v$ in classical mechanics; the second one, in the theory of relativity. The third expression is the momentum of a unit volume of electromagnetic field in terms of the vectors of the strengths of electric and magnetic fields. The fourth expression gives the momentum of a photon in terms of its cyclic frequency $\omega$ and unit vector $\vec{n}$ in the direction of the motion of the photon; here $\hbar$ is Planck's constant. These four formulas are a good illustration of the universal nature of the concept of ``momentum''. As for the quantities $\vec{v}, \, \vec{E}, \,\vec{H}$, and $\omega$), they are all applicable in respective disciplines of physics only.

It is worth noting here that proceeding from Newton's laws of dynamics we can obtain the law of conservation of momentum solely for the special case where the momentum is given by the expression $\vec{p}= m \vec{v}$; in other cases Newton's laws are clearly not applicable. If we start off with invariance principles, we can derive the law of conservation of momentum regardless of the expression describing the momentum in one case or another.


\section{The Practical Value of Conservation Laws}

In the introductory talk about symmetry, it was noted that symmetry is some general entity inherent in a wide variety of objects (phenomena), whereas asymmetry brings out some individual characteristics of a specific object or phenomenon. Permeating all spheres of physics and all specific situations, \emph{conservation laws express those general aspects of all situations, which is eventually related to the appropriate principles of symmetry}. These laws ``ignore'' the particularity of a given situation, they ``ignore'' the particular mechanisms of interplay, their region of applicability \emph{lies beyond the framework of specific theories}. It is the general, all-embracing character of conservation laws, which do not require an analysis of phenomena, that is responsible for the striking simplicity of these laws and unconditioned reliability of results derived from them.

It is worth mentioning that not infrequently the interaction mechanisms (details of a phenomenon) are unknown or known in only a fairly approximate way. In many cases the inclusion of a host of details in a problem overly complicates the mathematical side of the. problem. Against the background of these difficulties, the simple and elegant conservation laws appear quite attractive. When handling a phenomenon, a physicist, above all, applies conservation laws and only then proceeds to examine details. Many phenomena to date have been studied on the level of conservation laws only.

So in the billiard problem (see Chapter 10) we neglected the mechanism of collision of balls. Otherwise, we would have had to delve into the details of elasticity theory. In this particular case, it turned out to be sufficient to utilize the laws of conservation of energy and momentum alone.

\section{The Example of the Compton Effect}
We will illustrate the practical value of conservation laws referring to an effect discovered in 1923 by the British physicist A. Compton (the \emph{Compton effect}). The gist of the effect is as follows: when a monochromatic beam of X-rays is scattered at electrons that enter the composition of the target substance, the wavelength of the radiation is increased by $\Delta \lambda$, which is found using the relation (the \emph{Compton formula})
\begin{equation*}%
\Delta \lambda = \dfrac{4 \pi \hbar}{mc} \sin^{2} \dfrac{\varphi}{2}
\end{equation*}
where $\varphi$ is the scattering angle, $\hbar$ Planck's constant, $m$ electron rest mass, $c$ the velocity of light in a vacuum.
\begin{marginfigure}
 \centering
  \includegraphics[width=0.9\textwidth]{figs/ch-11/photon-electron1.jpg}
  \caption{The Compton effect.}
  \label{compton-1}
\end{marginfigure}

\begin{marginfigure}
 \centering
  \includegraphics[width=0.9\textwidth]{figs/ch-11/photon-electron2.jpg}
  \caption{The Compton effect.}
  \label{compton-2}
\end{marginfigure}
To derive the Compton formula we need not know the exact mechanism of the interaction of X-ray quanta (photons) with the electrons; \emph{it is sufficient to apply the laws of conservation of energy and momentum for a collision o f a photon with an electron}. In a sense, we again have to turn to the billiard problem, although neither the electron nor the photon resemble a billiard ball, of course. Before a collision, the ball of an electron can be treated as being at rest as compared with the incident ball of a photon. The energy of the photon is $E$, its momentum is $\vec{p}$. Assuming the collision to be off-centre, after it, the photon will be scattered at an angle $\varphi$, its energy being $E_{1}$ and momentum $p_{1}$; the electron will be scattered at an angle $\theta$, its energy being $E_{e}$; and momentum $p_{e}$ (Figure~\ref{compton-1}). \emph{The law of conservation o f energy has the form}
\begin{equation*}%
E=E_{1} +E_{e}
\end{equation*}
and the law of conservation of momentum (Figure~\ref{compton-2})
\begin{equation*}%
\vec{p}=\vec{p}_{1} + \vec{p}_{e}
\end{equation*}
or in the components,
\begin{align*}%
p & = p_{1} \cos \varphi + p_{e} \cos \theta \\
O & = p_{1} \sin \varphi - p_{e} \sin \theta
\end{align*}
The form of these equations is \emph{quite general}, they are applicable also for billiard balls. To take account of the specific feature of the photon-electron problem it is sufficient to express the photon energy as
\begin{equation*}%
E = \hbar \omega
\end{equation*}
and its momentum, using the formula discussed earlier in the book, as
\begin{equation*}%
\vec{p} = \dfrac{\hbar \omega}{c} \vec{n}
\end{equation*}
As regards the electron, its energy can be given in terms of momentum using the conventional relation
\begin{equation*}%
E_{e} = \dfrac{p_{e}^{2}}{2m}
\end{equation*}
that is equally valid both for the billiard balls and for the free electron (provided that its velocity is well under the velocity of light).

It follows that the conservation laws can be written as
\begin{align*}%
\hbar \omega & = \hbar \omega_{1} + \dfrac{p_{e}^{2}}{2m} \\
\dfrac{\hbar \omega}{c}  & = \dfrac{\hbar \omega_{1}}{c} \cos \varphi + p_{e} \cos \theta \\
0 & = \dfrac{\hbar \omega_{1}}{c}  \sin \varphi + p_{e} \sin \theta
\end{align*}
It only remains to do some algebra. Introducing the notation $\Delta \omega = \omega - \omega_{1}$ ($\Delta \omega$ is generally small, that is, $\Delta \omega \ll \omega$), we can write the above set of equations in the form
\begin{align*}%
\hbar \Delta \omega & =  \dfrac{p_{e}^{2}}{2m} \\
\hbar \omega - \hbar ( \omega - \Delta \omega) \cos \varphi & =  p_{e} c \cos \theta \\
 \hbar ( \omega - \Delta \omega) \sin \varphi & = p_{e} c \sin \theta
\end{align*}
Squaring the second and third equations and adding them together, we get rid of $\theta$:
\begin{equation*}%
\hbar^{2} \omega^{2} + \hbar^{2} (\omega - \Delta \omega)^{2} - 2 \hbar^{2} \omega (\omega - \Delta \omega) \cos \varphi = p_{e}^{2} c^{2}
\end{equation*}
or, using the first equation (the law of conservation of energy),
\begin{equation*}%
\omega^{2} + (\omega - \Delta \omega)^{2} - 2 \omega (\omega - \Delta \omega) \cos \varphi = \dfrac{2mc^{2}\Delta \omega}{\hbar}
\end{equation*}
We will now divide both sides of this by $\omega^{2}$ and open the brackets. Ignoring the small term $(\Delta \omega/\omega)^{2}$ gives
\begin{equation*}%
1 - \dfrac{\Delta \omega}{\omega} - \left( 1 - \dfrac{\Delta \omega}{\omega} \right) \cos \varphi = \dfrac{2mc^{2}}{\hbar \omega} \cdot \dfrac{\Delta \omega}{\omega}
\end{equation*}
or
\begin{equation*}%
2 \sin^{2} \dfrac{\varphi}{2} \left( 1 - \dfrac{\Delta \omega}{\omega}\right) = \dfrac{2mc^{2}}{\hbar \omega} \cdot \dfrac{\Delta \omega}{\omega}
\end{equation*}
The wavelength $\lambda$ is related to the cyclic frequency $\omega$ by the expression $\lambda = 2 \pi c/ \omega$. It is easily seen that
\begin{equation*}%
\Delta \lambda  = \lambda_{1} - \lambda = \dfrac{ 2 \pi c }{\omega_{1}} - \dfrac{ 2 \pi c }{\omega} = 2 \pi c \dfrac{\omega - \omega_{1} }{\omega \omega_{1}} =  2 \pi c \dfrac{\Delta \omega / \omega^{2}}{1 - \Delta \omega / \omega} 
\end{equation*}
Hence
\begin{equation*}%
2 \sin^{2} \dfrac{\varphi}{2} = \dfrac{mc}{2 \pi \hbar} \Delta \lambda
\end{equation*}
The Compton formula follows from this immediately.

We have thus seen that by approaching the Compton effect at the level of conservation laws, we can derive the Compton formula, that is, to find the variation of the wavelength increment with the scattering angle for
the photon.

Before we leave this section, it should be stressed that it would be wrong to overestimate the role of conservation laws. To be sure, they are not a panacea for all aches and pains of the physicist. So to determine the probability for a photon to scatter at a given angle, it is necessary, apart from conservation laws, to take into account the specific mechanism of the interaction of photons and electrons. In that case, the parallel with billiard balls does not work any more.

\section{Conservation Laws as Prohibiting Rules}

It is well known that conservation laws are often formulated as \emph{rules (laws) of prohibition}. So the law of conservation of energy is, in essence, the law that prohibits perpetual motion. In a sense the law of conservation of momentum is the law that prohibits lifting oneself by the hair. The law of conservation of angular momentum prohibits, for example, a planet from leaving its orbit and changing the angle at which
its spin axis is inclined to the orbital plane.

There are two reasons to approach conservation laws as laws of prohibition. Above all, we notice that \emph{symmetry, which introduces a measure of orderliness, at all times tends to reduce the number o f possible versions}. It has already been stressed in Chapter 4 that symmetry dramatically limits the diversity of structures that may be encountered in nature. We can now complement that statement by stressing that symmetry limits the diversity of \emph{not only structures} but also of the \emph{forms of behaviour of physical systems}. Through conservation laws (or rather prohibition laws) symmetry covers all the conceivable forms of the behaviour of a system and at times it nearly uniquely predetermines a certain behaviour of a system.

Remember the billiard balls. No matter how strongly you push a ball to a head-on collision it is bound to come to rest, since by energy and momentum conservation it may not move after the impact. Also of interest is the case of off-centre collision: the conservation laws decree that after the collision the balls only move off at right angle to each other.

In his book \emph{The World of Elementary Particles} the American physicist K. Ford writes: ``The older view of a fundamental law of nature was that it must be a law of \emph{permission}. It defined what can (and must) happen in natural phenomena. According to the new view, the more fundamental law is a law of \emph{prohibition}. It defines what \emph{cannot} happen. A conservation law is, in effect, a law of prohibition. It prohibits any phenomenon that would change the conserved quantity.''

\begin{marginfigure}
 \centering
  \includegraphics[width=0.85\textwidth]{figs/ch-11/traffic-signs.jpg}
  \caption{The Compton effect.}
  \label{traffic-signs}
\end{marginfigure}

It would seem that \emph{prohibitory} rules represent just a simplified version of \emph{guiding} rules. In actual fact, this is not so. Take road signs, for example. Suppose the sign shown in Figure~\ref{traffic-signs}~(a) is put up before a crossing. This sign is guiding: it explicitly directs the traffic - only forward. If then the sign is as shown in Figure~\ref{traffic-signs}~(b) (prohibitory sign), the traffic does not have an unequivocal direction - it may either continue forward or turn to the left; it is only prohibited to turn to the right. The meaning of the above quotation from Ford is that the conservation laws are to be likened to prohibitory laws, and not guiding laws.

There is one more important reason for which conservation laws are to be regarded as laws of prohibition. The fact is that in the world of elementary particles (where, additionally, there are a wide variety of other conservation laws), conservation laws are derived as rules that prohibit the phenomena that are never observed in experiment. Suppose experimental data definitely indicate that some transmutations of particles never occur, or in other words are prohibited. This circumstance may be used as a basis for formulating \emph{some conservation law}. But it is not always that the invariance principle underlying the law is revealed; in such cases a conservation law only appears as a law of prohibition. We will be looking at such examples later in the book.
%
%\begin{figure*}
%\centering
%\includegraphics[width=0.9\textwidth]{figs/ch-04/regular-polyhedra.png} 
%\caption{The five possible types of regular polyhedra. \label{regular-poly}} 
%\end{figure*}
%
%\begin{marginfigure}
% \centering
%  \includegraphics[width=0.9\textwidth]{figs/ch-04/dodecahedron-symm.png}
%  \caption{The symmetry of dodecahedron.}
%  \label{dodeca-symm}
%\end{marginfigure}
%
%
%\begin{figure}
%\centering
%\includegraphics[width=0.75\textwidth]{figs/ch-04/kepler.png}
%\caption{Scheme of the Solar System based on the regular polyhedra devised by Kepler.}
%\label{kepler-scheme}
%\end{figure}
%
%
%
%                        