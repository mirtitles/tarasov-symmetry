% !TEX root = tarasov-symmetry.tex






\chapter{The Symmetry of Various Interactions}

\marginnote{The state of the art in particle physics does not differ markedly from what you observe when you sit in a concert hall just before the performance begins. Many (but not all) musicians have appeared on the scene. They are tuning their instruments. At times you can hear some interesting musical passages: the improvisations are to be heard from all directions, sometimes wrong notes are also heard. The scene is pregnant with the
expectation of the moment when the first sounds of the symphony will be heard.
\newline  - A. Pais. }

\section{The Principal Types of Interactions}

According to modern views, there are \emph{four} main types of forces in nature,
or rather four types of interactions - \emph{strong (nuclear), electromagnetic,
weak}, and \emph{gravitational}.

\emph{Nuclear forces} strongly bind the neutrons and the protons in atomic
nuclei. They are responsible for a wide variety of nuclear reactions,
specifically for those reactions that release energy in the core of a nuclear
reactor at an atomic power station. Hadrons are responsible for strong
interactions (baryons and mesons), while leptons do not participate in
them.

\emph{Electromagnetic interactions}, it seems, are now the ones which occur
most often: we encounter them when studying electric and magnetic
phenomena and properties of matter and electromagnetic (in particular
optical) radiation. These interactions determine the structure and
properties of atoms and molecules. They encompass Coulomb forces, the
forces acting on a current-bearing conductor, the forces of friction,
resistance, elasticity, chemical forces, and what not. All the elementary
particles, except for both neutrino and antineutrino participate in
electromagnetic interactions.

\emph{Weak interactions} are predominant in the realm of subatomic particles.
They are responsible for the interactions of particles involving neutrino
and antineutrino (specifically beta-decay processes). Furthermore, they
are involved in neutrinoless decays that are characterized by a relatively
long lifetime of the decaying particles - about \SI{d-10}{\second} or more.\sidenote{Speaking about the long lifetime of a particle, we compare it with the time during which light covers the distance of the order of the atomic nucleus itself, that is, $l \approx \SI{d-15}{\metre}$. The reference time taken to be a ``unit time'' in the world of
elementary particles is about $l/c \approx \SI{d-23}{\second}$.} These
processes include the decays of kaons and hyperons.

\emph{Gravitational interactions} are inherent in all particles, without
exception, but they are of no significance for elementary particles. These
interactions only manifest themselves on a sufficiently large scale when
the masses involved are rather large. They are responsible, say, for the
attraction of the planets to the Sun or for the falling of an object onto the
ground. In what follows we will ignore gravitational interactions

Interactions differ markedly in terms of the forces or energies involved. The strong interaction is about 100 times higher than the electromagnetic one and \num{d14} higher than the weak one. \emph{The stronger the interaction, the faster it carries out its task}. So the particles called resonances, whose decay occurs through nuclear interactions, have a lifetime of about  \SI{d-23}{\second}; the neutral pions, which decay through an electromagnetic interaction ($\pi^{0} \to \gamma + \gamma$), have a lifetime of  \SI{d-16}{\second}; decays through a weak interaction have a lifetime of \SIrange{d-8}{d-10}{\second}. The strong
interaction produces fast processes, the weak interaction, slow processes.
The duration of a process is defined as a quantity that is the reciprocal of
the probability of the process per unit time. The smaller the probability,
the slower the process. It is to be recalled in this connection that the
neutrino and antineutrino processes characteristic of the weak
interaction are highly unlikely.

Unlike the electromagnetic interaction, strong and weak interactions
manifest themselves over extremely short distances, or rather, have
a \emph{small range}. The strong interaction between two baryons or mesons
only shows up when the particles approach each other and come within
a distance of only \SI{d-15}{\metre}. The range of the weak interaction is yet
shorter, it is known to be within \SI{d-19}{\metre}.

The most interesting difference between the types of interactions is
associated with \emph{symmetry}. All the interactions of particles are controlled
by the absolute conservation laws discussed in Chapter 13. But there exist
conservation laws (and pertinent symmetry principles), which are valid
for some interactions and not for other interactions. So the laws of
conservation of spatial and charge parity (P-invariance and C-in variance)
hold for both the electromagnetic and strong interactions, but they do
not hold for the weak interaction. There is the rule: \emph{the stronger an
interaction the more symmetrical it is}. Put another way the weaker an
interaction the less it is controlled by conservation laws. In the words of
Ford, \emph{``weaker interactions turn into infringers of the law. and the weaker an
interaction the more lawlessness''.}

\section{Isotopic Invariance of Strong Interactions. The Isotopic Spin (Isospin)}

Suppose that all the protons in the atomic nucleus are replaced by
neutrons, and all the neutrons by protons. The resultant nucleus is called
the mirror nucleus of the initial nucleus. Mirror nuclei are, for example,
the pairs: $\ce{Be^{7}}$ and $\ce{Li^{7}}, \, ce{B^{9}}$ and $\ce{Be^{9}}, \, \ce{C^{14}}$ and $\ce{O^{14}}$, and so on (Figure~\ref{isotopic-doublet}). It was noticed long ago that pairs of mirror nuclei have similar properties:
essentially the same nuclear binding energy, the similar structure of
energy level, the same spin. The similarity of mirror nuclei reflects
a measure of \emph{symmetry of nuclear forces}, namely the fact that the nuclear
forces between two protons are the same as the forces between two
neutrons.
\begin{figure}[!h]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-16/isotopic-doublet.jpg}
\caption{Some examples of isotpoic doublets.}
\label{isotopic-doublet}
\end{figure}

This symmetry is a special case of the so-called \emph{isotopic invariance}. The
latter means that from the point of view of the strong interaction, the
system $p-p$ (proton-proton) is identical with not only the system $n-n$
(neutron-neutron) but also with the system $p-n$ (proton-neutron). Stated
another way, \emph{the nuclear forces are independent of the electric charge of
particles}.

Associated with the isotopic invariance of the strong interaction is the
concept of \emph{isotopic spin (isospin)}. It is worth recalling in this connection
that the proton and the neutron can be viewed as two charge conditions
of one particle the nucleon (see Chapter~12). It is said that the proton
and the neutron form an \emph{isotopic doublet}. Isotopic doublets are also
formed by two xi-hyperons $(\Xi^{-},\Xi^{0})$ and two kaons $(K^{0}, K^{+})$. It appeared that pions are better combined into a \emph{triplet}, by adding to $\pi^{+}$ and $\pi^{0}$ an antipion $\pi^{-}$. An isotopic triplet is also formed by three sigma- hyperons $( \Sigma^{~}, \Sigma^{0}, \Sigma^{+})$. As to the lambda-hyperon $\Lambda^{0}$, omega-hyperon $\Omega^{-}$ and eta-meson $\eta^{0}$, to each of them an isotopic singlet is put in correspondence. Isotopic multiplets of known elementary particles come in three types-triplets (three charge states), doublets (two charge states), and singlets (one charge state). True, there exists a multiplet with four
charge states. This multiplet is formed by $\Delta$-particles which belong to
short-lived baryons, called resonances $(\Delta^{-}, \Delta^{0}, \Delta^{+}, \Delta^{++})$. The particle
$\Delta^{++}$ has a positive electric charge, whose magnitude equals the doubled
electron charge. 

Each isotopic multiplet is characterized by a quantity
called the \emph{isotopic spin (isospin)}. The magnitude of isospin $I$ of a particle is
related to the number of charge states $n$ in the multiplet by the
relationship $n = 2I + 1$. Recall that the spin s of a particle is related to the
number of spin states of the particle in exactly the same way. The analogy
between the isospin and the spin, although formal (spin and isospin are
physically absolutely different), is rather profound One must recall that if
the spin vector is in the conventional space, the isospin vector is
considered in some fictitious space (called \emph{isotopic space} or \emph{isospin space}).

The electron's spin is $s = 1 /2$, its projection in a given direction in conventional space takes on the values $s_{z} = + 1/2$ and $s_{z} = - 1/2$. The isospin of the nucleon (nucleon doublet) $I = 1/2$, its projection in some
``direction'' in the isospin space assumes the values $I_{\zeta} = +1/2$ (for the
proton) and $I_{\zeta} = -1/2$ (for the neutron). Figure~\ref{iso-multi} contains the various
isotopic multiplets, as well as the values of the isospin $I$ and the
projections of the isospin  $I_{\zeta}$. Note that for an antiparticle the projection
of the isospin has the sign opposite to that of the corresponding particle.
When dealing with the strong interaction of particles, the \emph{isospin vectors
of particles must be combined by the same rules as the spin vectors.}
Specifically, the isospin projection of several particles is the algebraic sum
of the isospin projections for individual particles.
\begin{figure*}[!h]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-16/isotopic-multiplets.jpg}
\caption{Table showing isotopic multiplets and values of isospin.}
\label{iso-multi}
\end{figure*}
The isotopic invariance of the strong interaction lies at the foundation
of the physics of isospin formalism. This invariance means that the \emph{laws of
nature are invariant under rotations in isospin space}. This finds its
expression in the \emph{law of conservation of isospin} (just as the invariance of
the laws of nature under rotations in conventional space is expressed in
the law of conservation of angular momentum). In all the strong
interactions of subatomic particles, the \emph{total isospin of a system of
particles is conserved}. Note that also conserved is the total projection of
the isospin, which in fact implies that the total electric charge of the
particles is conserved as well.

We will now illustrate the conservation of isospin using two processes:
\begin{align*}%
p + p & \to \pi^{+} + D \\
n + p & \to  \pi^{0} + D
\end{align*}
where $D$ is the deuteron (the nucleus of heavy hydrogen, which consists of a neutron and a proton). The
deuteron's isospin is zero; therefore, the products of reactions in the
general case have the total isospin that is equal to the isospin of pions.
that is, unity. In the first reaction the sum of the isospin projections will
be $1/2+ 1/2= 1$, hence the isospin itself is $1$. In the second reaction the
total projection of the isospin is zero $(-1/2 + 1/2 = 0)$; in this case the
total isospin may be either unity or zero. Both values are equiprobable,
therefore only in half of the cases a collision of a neutron with a proton
can result in a reaction producing a deuteron. This suggests that the
reaction $n + p \to\pi^{0} + D$ must be half as likely as the reaction $p + p \to
\pi^{+} +D$. Experiment supports this prediction made on the basis of
isospin conservation.

\section{Strangeness Conservation in Strong and Electromagnetic Interactions}

In the years 1947-1955 kaons and a number of hyperons (in collisions of
pions with nucleons) were discovered. The particles discovered turned
out to be fairly strange. First, they came in \emph{pairs} - a kaon paired with
a hyperon. For example,
\begin{align*}%
\pi^{-} + p & \to K^{0} + \Lambda^{0} \\
\pi^{-} + p & \to K^{+} + \Sigma^{-} \\
\pi^{+} + p & \to K^{+} + \Sigma^{+}
\end{align*}
Second, the lifetime of new particles produced without leptons 
\begin{align*}%
K^{+} & \to \pi^{+} + \pi^{-} \quad \Lambda^{0}  \to p + \pi^{-}\\
\Lambda^{0} & \to n + \pi^{0} \quad  \Sigma^{+}  \to p + \pi^{0} \\ 
\Sigma^{+} & \to n + \pi^{+} \quad  \Sigma^{-} \to n + \pi^{-} 
\end{align*}
turned out to be \emph{startlingly long}: \SI{d-8}{\second} for kaons and
\SI{d-10}{\second} for hyperons. The fact that the decay schemes included no
leptons suggested that these decays are associated with the strong
interaction, in which case the lifetime of the particles must be about
\SIrange{d-22}{d-23}{\second}.

An elegant solution to both problems was found by the American
physicist M. Gell-Mann and the Japanese physicist K. Nishijima. They
assumed that the long lifetime of kaons and hyperons is associated with
the \emph{conservation of some hitherto-unknown physical quantity} (just like the
stability of the proton is associated with the conservation of baryon
number and the stability of the electron, with the electric charge). So,
another characteristic of the elementary particles appeared, and not
without humour it was called the \emph{strangeness}. A new conservation law
was established that is valid for strong and the electromagnetic
interactions: \emph{the total strangeness of the mesons and the baryons involved in
the process is conserved}.

Figure~\ref{strangeness} tabulates the values of strangeness $S$ for various mesons
(antimesons) and baryons (antibaryons). The strangeness of an
antiparticle equals the strangeness of a respective particle with the
opposite sign.
\begin{figure*}[!h]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-16/strangeness.jpg}
\caption{Table showing strangeness for mesons and baryons.}
\label{strangeness}
\end{figure*}
It follows from \emph{strangeness conservation law} that in collision of
a particle with zero strangeness, a lambda (or sigma) hyperon may only
be produced together with a kaon (the total strangeness of the kaon and
the hyperon is zero). But the production of a xi hyperon must be
accompanied by the production of two kaons (for example, $p + p \to  p +
\Xi^{0} + K^{0} + K^{+}$). Omega hyperons have been observed to be produced
in a beam of negatively charged kaons: $K^{-} + p \to\Omega + K^{0} + K^{+}$

The long lifetime of kaons is accounted for by the fact that the kaon is
the lightest particle with a nonzero strangeness. It cannot decay either
due to the \emph{strong} interaction, or due to the \emph{electromagnetic} interaction
since there is \emph{no particle to which it could transfer its strangeness}. The
kaon has one possibility: to decay by weak interaction, since in such
interactions strangeness is not conserved. So, the decays of the type 
$K^{0} \to \pi^{0} + \pi^{0} + \pi^{0}$ or $K^{+} \to \pi^{+} + \pi^{0}$ are controlled, despite the absence of
leptons, exactly by the weak interaction, which in turn predetermines the
long lifetime of kaons.

The long lifetime of the lambda hyperon stems from the fact that this
hyperon is the lightest baryon with nonzero strangeness. The decay of the
lambda hyperons into kaons (or rather antikaons) is absolutely
prohibited by the law of conservation of baryon number, and the decay
into nucleons is prohibited by strangeness conservation. The observed
decays $Lambda^{0} \to p + \pi^{-}$ and $\Lambda^{0} \to n + \pi^{0}$ occur owing to the \emph{weak
interaction}, which does not conserve strangeness.

The charged sigma hyperons $\Sigma^{-}$ and $\Sigma^{+}$, too, can only decay through
the weak interaction. The sigma hyperon cannot decay into a lambda
hyperon and a pion, since the mass difference of a sigma and a lambda
hyperon is smaller than the pion mass. In the case of the neutral sigma
hyperon, a decay is possible that \emph{conserves strangeness} (through the
\emph{electromagnetic} interaction): $\Sigma^{0} \to \Lambda^{0} + \gamma$. Therefore, the lifetime of
a $\Sigma^{0}$-hyperon is shorter than \SI{d-8}{\second}.

An omega hyperon and xi hyperons decay into hyperons with a smaller
mass:
\begin{align*}%
\Xi^{0} & \to \Lambda^{0} + \pi^{0} \\
\Xi^{-} & \to \Lambda^{0} + \pi^{-}\\
\Omega^{-} & \to \Xi^{0} + \pi^{-} \\
\Omega^{-} & \to \Lambda^{-} + \pi^{0} \\
\Omega^{-} & \to \Lambda^{0} + K^{-}
\end{align*}
Since for the omega hyperon $S = -3$, and for xi hyperons $S = -2$, then
in these processes, too, \emph{strangeness is not conserved}, which predetermines
their slow (\emph{weak}) nature.

So far we do not know which principles of symmetry underlie the law
of conservation of strangeness. There is no doubt, however, that
strangeness conservation is one of the most important properties of the
strong and the electromagnetic interactions, which accounts for the
observed processes of interactions in the world of mesons and baryons.
Specifically, it is of fundamental importance that strangeness does not
conserve in weak interactions. If it were conserved not only in strong and
electromagnetic interactions, but also in weak interactions (as, for
instance, the electric charge, the electron, muon, and baryon numbers),
then in addition to the electron and the proton \emph{there would exist eight
more (!) stable} subatomic particles with a nonzero rest mass:  $K^{+}, \, K^{0}, \,  \Lambda^{0}, \, \Sigma^{+}, \, \Sigma^{-},\,  \Xi^{0}, \, \Xi^{-}, \, \Omega^{-}$. 
What structure would the atom have then would be anyone's guess.

\section{Interactions and Conservations}

As it has already been noted, the \emph{highest symmetry} is inherent in processes
occurring due to the strong interaction. For them we have ten
conservation laws (Figure~\ref{interactions2}): \emph{energy, momentum, angular momentum,
electric charge, baryon number, space, charge}, and \emph{time parity, strangeness,
isospin}. In principle, we can add two more conservation laws for the
\emph{electron} and \emph{muon} numbers. True, in strong interactions these laws are
fulfilled simply because there are no leptons, since the electron and muon
numbers of all the components are zero.
\begin{figure*}[!h]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-16/interactions2.jpg}
\caption{Table showing interactions and quantities that are conserved in them.}
\label{interactions2}
\end{figure*}

Turning to \emph{electromagnetic} interactions, symmetry \emph{becomes
lower - isospin} conservation is no longer valid. \emph{Yet more marked reduction
is observed when we go over to the weak interaction}. In the world of weak
interactions we will have to forsake four conservations at once: \emph{space} and
\emph{charge parity, strangeness}, and \emph{isospin}. In some cases \emph{time parity} is
violated as well. This is somewhat compensated for by the conservation
of \emph{CPT-parity}, and in the majority of cases \emph{combined parity} as well.

\section{A Curious Formula}

Gell-Mann and Nishijima turned their attention to a rather curious fact.
It turns out that the electric charge $Q$ of a particle (in terms of the ratio of
the particle's charge to the electron's charge), the isospin projection $I_{\zeta}$,
the baryon number $B$ and the strangeness $S$ are related by the following
simple relationship (the \emph{Gell-Mann-Nishijima formula}):
\begin{equation*}%
Q = I_{\zeta} + \dfrac{B + S}{2}
\end{equation*}
The reader can easily prove this relationship for any meson or baryon.
For example, for a $\Xi^{-}$-hyperon: 
\begin{equation*}%
Q =  -1, \, I_{\zeta}= -1/2, \, B = 1, \, S = -2. 
\end{equation*}
In this case, we have $ -1 = - \dfrac{1}{2} + \dfrac{1 - 2}{2}$

The Gell-Mann-Nishijima formula related the four (seemingly
different) physical characteristics for any meson or any baryon. The
existence of such a relation suggests that there is a definite \emph{internal
completeness} of the established description of the properties of strongly
interacting particles.

\section{The Unitary Symmetry of Strong Interactions}

Consider a system of coordinates in which the abscissa axis is the
projection of isospin $I_{\zeta}$, and the ordinate system is $Y=B + S$, a quantity
called the \emph{hypercharge}. On this plane we will position all the baryons with
$s=1/2: p, \, n, \, \Lambda^{0}, \, \Sigma^{-}, \, \Sigma^{0}, \, \Sigma^{+}, \, \Xi^{-}, \, \Xi^{0}$

The eight baryons with spin $1/2$ form a hexagon in the plane $I_{\zeta}, \,Y$. At
each vertex of the hexagon there lies one baryon, in the centre two
baryons (Figure~\ref{eight-baryons}). The arrangement of baryons in the plane allows the
$Q$ axis to be introduced.
\begin{figure}[!h]
\centering
\includegraphics[width=0.65\textwidth]{figs/ch-16/eight-baryons.jpg}
\caption{Arrangement of baryons to reveal concealed symmetry.}
\label{eight-baryons}
\end{figure}
Looking at Figure~\ref{eight-baryons}, where all the eight baryons with spin $1/2$ appear
to be combined within a geometrically symmetric closed figure, one
cannot ignore the fact that this is an example of some \emph{concealed symmetry
in nature}. This assumption is substantiated if we place other strongly
interacting particles on the plane $I_{\zeta}, \,Y$ by combining them into groups
with the same spin $s$. It appears that the eight particles with $s=0$, which
include all the mesons and antimesons ($K^{0}, \, K^{+}, \, \bar{K}^{0}, \, K^{-}, \, \pi^{+}, \, \pi^{0}, \pi^{-},
\eta^{0}$), form (on the plane $I_{\zeta}, \,Y$) exactly the same hexagon as the eight
baryons (Figure~\ref{eight-mesons}). 
\begin{figure}[!h]
\centering
\includegraphics[width=0.65\textwidth]{figs/ch-16/eight-mesons.jpg}
\caption{Arrangement of mesons to reveal concealed symmetry.}
\label{eight-mesons}
\end{figure}
A result of no less interest is obtained for short-lived particles called \emph{resonances}. Among these particles, which refer to baryons, we know nine particles with $s = 3/2: \Delta^{-}, \, \Delta^{0}, \, \Delta^{+}, \, \Delta^{++}, \, Y^{*-}_{1}, \, Y^{*0}_{1}, \, Y^{*+}_{1}, \, \Xi^{*0}, \, \Xi^{*-}$
In the plane $I_{\zeta}, \,Y$ they form a rectangle shown in Figure~\ref{ten-resonances}, in
which, however, one place is vacant - the vertex $A$. It is clearly seen in the
figure that the missing particle (the missing baryon with spin $3/2$) must be
included in the isotopic singlet and have a negative charge and
strangeness $S = -3$. You can imagine the satisfaction physicists derived
when in 1964 the missing particle was actually found. So the hyperon $\Omega^{-}$
was added to the list of elementary particles.
\begin{figure}[!h]
\centering
\includegraphics[width=0.65\textwidth]{figs/ch-16/ten-resonances.jpg}
\caption{Arrangement of resonances to reveal concealed symmetry.}
\label{ten-resonances}
\end{figure}
The \emph{eight baryons}, the \emph{eight mesons}, the \emph{ten baryons} shown in
Figures~\ref{eight-baryons}-\ref{ten-resonances} are called \emph{supermultiplets}. Each supermultiplet contains
several isotopic multiplets with different values of strangeness.

The symmetry that manifests itself through a union of mesons and
baryons into several supermultiplets is the so-called \emph{unitary symmetry}.
The explanation of the mathematical nature of unitary symmetry lies
beyond the scope of this book, and we only note that this symmetry
establishes the internal relationship between the particles belonging to
various isotopic multiplets and having different strangeness. The fact that
a fairly numerous set of mesons and baryons (including resonances) can
be compressed into a small number of eight-fold and ten-fold 
supermultiplets suggests that in the world of strongly interacting particles
there exists a general order.

%
%\begin{figure*}[!h]
%\centering
%\includegraphics[width=0.9\textwidth]{figs/ch-04/regular-polyhedra.png} 
%\caption{The five possible types of regular polyhedra. \label{regular-poly}} 
%\end{figure*}
%
%\begin{marginfigure}
%\centering
%\includegraphics[width=0.9\textwidth]{figs/ch-04/dodecahedron-symm.png}
%\caption{The symmetry of dodecahedron.}
%\label{dodeca-symm}
%\end{marginfigure}
%
%
%\begin{figure}[!h]
%\centering
%\includegraphics[width=0.75\textwidth]{figs/ch-04/kepler.png}
%\caption{Scheme of the Solar System based on the regular polyhedra devised by Kepler.}
%\label{kepler-scheme}
%\end{figure}
%
%
%
%                        