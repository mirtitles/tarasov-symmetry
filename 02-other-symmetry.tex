% !TEX root = tarasov-symmetry.tex






\chapter{Other Kinds of Symmetry }




\section{Rotational Symmetry}
\marginnote[-2cm]{ If we venture beyond our
habitual notion of symmetry as a property that
is in all ways related to
our external appearances, we can find quite a few figures that are symmetrical in one or another sense. - A.S. Kompaneets}

Let there be an object that coincides with itself exactly when turned about some axis through an angle of $\ang{360}/n$ (or its multiple), where $n = 2, \, 3, \, 4, \dots$ In that case, we speak about \emph{rotational symmetry}, and the above-mentioned axis is referred to as the \emph{$n$-fold rotation symmetry-axis}, or \emph{$n$-fold symmetry}, or \emph{axis of $n$-fold symmetry}. In the earlier examples of letters H and N we dealt with a two-fold axis, and in the example with an Egyptian pyramid, with a 4-fold axis. \hyperref[regular-symm]{Figure~\ref{regular-symm}} gives examples of simple objects with rotation axes of different orders-from two-fold to five-fold.

\begin{marginfigure}[0.5cm]
\includegraphics[width=0.9\textwidth]{figs/ch-02/3d-solids.jpg}
\caption{Symmetries axes of various three-dimensional solids.}
\label{regular-symm}
\end{marginfigure}


A three-dimensional object may have several rotation axes. For example, the first object in  \hyperref[regular-symm]{Figure~\ref{regular-symm}} has three two-fold axes, the second object, in addition to a three-fold axis, has three two-fold axes, and the third object, in addition to a four-fold axis, has four two-fold axes (additional axes are shown in the figure by dash lines).

Consider the \emph{cube}. We can see immediately that it has three four- fold rotation axes (\hyperref[cube-symmetry]{Figure~\ref{cube-symmetry}~a}). Further examination reveals six two-fold axes passing through the centres of opposite parallel edges (\hyperref[cube-symmetry]{Figure~\ref{cube-symmetry}~b}), and four three-fold axes, which coincide with the inner diagonals of the cube (\hyperref[cube-symmetry]{Figure~\ref{cube-symmetry}~c}). The cube thus has 13 rotation axes in all, among them two-, three-, and four-fold ones.

\begin{figure}[!ht]
\includegraphics[width=0.9\textwidth]{figs/ch-02/cube.jpg}
\caption{The 13 axes of symmetry in a cube.}
\label{cube-symmetry}
\end{figure}


\begin{marginfigure}[-2cm]
\includegraphics[width=0.9\textwidth]{figs/ch-02/cylinder.jpg}
\caption{The infinite two-fold axes of symmetry, and one three-fold axes of symmetry for a cylinder.}
\label{cylinder-symm}
\end{marginfigure}

Also interesting is the rotational symmetry of the \emph{circular cylinder}. It has an infinite number of two-fold axes and one infinite-fold axis (\hyperref[cylinder-symm]{Figure~\ref{cylinder-symm}}).

\emph{To describe the symmetry of a concrete object requires specifying all the axes and their orders, and also all the planes of symmetry}. Let us take, for example, a geometrical body composed of two identical regular pyramids (Fig. 19). It has one four-fold rotation axis ($AB$), four two-fold axes ($CE, \, DF, \, MP$ , and $NQ$), five planes of symmetry ($CDEF, \, AFBD, \, ACBE, \, AMBP$, and $ANBQ$).

\begin{figure}[!ht]
\centering
\includegraphics[width=0.45\textwidth]{figs/ch-02/regular-pyramid.jpg}
\caption{The axes of symmetry of a body composed of two identical regular pyramids..}
\label{regular-pyramid}
\end{figure}


\section{Mirror-Rotational Symmetry}
\begin{marginfigure}%[-2cm]
\includegraphics[width=0.9\textwidth]{figs/ch-02/square.jpg}
\caption{Combining two squares to create a composite.}
\label{two-squares}
\end{marginfigure}

Let us cut a square out of thick paper and inscribe into it obliquely another square (\hyperref[two-squares]{Figure~\ref{two-squares}}). Now we will bend the corners along the periphery of the inner square as shown in \hyperref[two-squares2]{Figure~\ref{two-squares2}}. The resultant object will have a two-fold symmetry axis ($AB$) and no planes of symmetry. We will view this object first from above and then from below (from the other side of the sheet). We will find that the top and bottom views look the same. This suggests that the two-fold rotational symmetry does not exhaust all the symmetry of a given object.


\begin{marginfigure}%[-2cm]
\includegraphics[width=0.9\textwidth]{figs/ch-02/square2.jpg}
\caption{Axes of symmetry for the composite 3-d object created from inscribed square.}
\label{two-squares2}
\end{marginfigure}

The additional symmetry of this object is the so-called \emph{mirror-rotational symmetry}, in which the object coincides with itself when turned through \ang{90} about axis $AB$ and then reflected from plane $CDEF$. Axis $AB$ is called the four-fold \emph{mirror-rotational axis}. We thus
have a symmetry relative to two consecutive operations - a turn by \ang{90} and a reflection in a plane normal to the rotation axis.


\section{Translational Symmetry}

Consider the plane figure in  \hyperref[trans-01]{Figure~\ref{trans-01}~a}. A translation along line $AB$ by a distance $a$ (or its multiple) makes the figure seem unchanged. In that case we speak about \emph{translational symmetry}. $AB$ is called the \emph{axis of translation}, and $a$ is called the \emph{fundamental translation}, or \emph{period}. Strictly speaking, a body having a translational symmetry must be infinite in the direction of the translation. But the concept of translational symmetry in a number of cases is also applied to bodies of finite sizes when partial coincidence is observed. It is seen in   \hyperref[trans-01]{Figure~\ref{trans-01}~b} that when a finite figure is displaced by distance a along line $AB$, part 1 coincides with part 2.
\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\textwidth]{figs/ch-02/translation-01.jpg}
\caption{ A translation along line gives rise to translational symmetry.}
\label{trans-01}
\end{figure}

Associated with translational symmetry is the important concept of a two-dimensional  \emph{periodic structure} - the \emph{plane lattice}. A plane lattice can be formed by crossing two sets of parallel equi-spaced straight lines (\hyperref[trans-02]{Figure~\ref{trans-02}}), their intersections being called \emph{sites}. To specify a lattice, it is sufficient to specify its \emph{unit cell} and then to displace it along $AB$ by distances multiple of $a$, or along $AC$ by distances multiple of $b$. Note that in a given lattice a unit cell may be chosen in a \emph{wide variety of ways}. So we may select the red cell in \hyperref[trans-02]{Figure~\ref{trans-02}}. Or we may also use any of the shaded ones.
\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\textwidth]{figs/ch-02/translation-02.jpg}
\caption{ Different ways of choosing the unit cell in a lattice.}
\label{trans-02}
\end{figure}


The translational symmetry of a plane lattice is totally defined by
a combination of two vectors ($\vec{a}$ and $\vec{b}$ in \hyperref[trans-02]{Figure~\ref{trans-02}}). Five types of plane lattices (five types of translational symmetry in a plane) are distinguished. These are given in  \hyperref[trans-03]{Figure~\ref{trans-03}}:
\begin{enumerate*}[label=(\alph*),leftmargin=1cm]
\item $a = b, \,\, \gamma = \ang{90} $ (\emph{square} lattice),
\item $a \neq b, \,\, \gamma = \ang{90} $ (\emph{rectangular} lattice),
\item $a = b, \,\, \gamma = \ang{60} $ (\emph{hexagonal} lattice),
\item $a = b, \,\, \gamma \neq \ang{90}, \,\, \gamma \neq \ang{60} $ (\emph{rhombic} lattice), and
 \item $a \neq b, \,\, \gamma \neq \ang{90}$ (\emph{oblique} lattice).
\end{enumerate*}

Translational symmetry in three-dimensional space is associated
with the notion of a three-dimensional periodic structure - the \emph{space
lattice}. This lattice can be thought of as a result of the crossing of
three sets of parallel planes. The translational symmetry of
a three-dimensional lattice is defined by the three vectors that specify
the unit cell of the lattice. \hyperref[cube-2]{Figure~\ref{cube-2}} shows a unit cell described by $\vec{a}$, (oblique lattice) $\vec{b}$, and $\vec{c}$. In the simplest case, all the edges of a cell are equal in length and the angles between them are \ang{90}. In that case, we have a \emph{cubic lattice}. All in all, there are 14 types of space lattices, differing in their type of space symmetry. In other words, there are 14 \emph{Bravais lattices} (named after a French crystallographer of the 19th century).

\begin{marginfigure}%[-2cm]
\includegraphics[width=0.9\textwidth]{figs/ch-02/cube2.jpg}
\caption{Vectors defining a three-dimensional unit lattice.}
\label{cube-2}
\end{marginfigure}


\section{Bad Neighbours}

Translational and rotational symmetries may live side by side with each other. So, the square lattice (\hyperref[trans-03]{Figure~\ref{trans-03}~a}) has a four-fold rotational symmetry, and the hexagonal lattice (\hyperref[trans-03]{Figure~\ref{trans-03}~c}) has a six-fold rotational symmetry. It is easily seen that a lattice has an infinite number of rotation axes. For example, in the case of the square lattice, the rotation axes (four-fold) pass through the centre of each square cell and through each lattice site.
 \begin{figure*}[!ht]
\centering
\includegraphics[width=.8\textwidth]{figs/ch-02/translation-03.jpg}
\caption{Five types of plane lattices (five types of translational symmetry in a plane).}
\label{trans-03}
\end{figure*}
          
 But the translational and rotational symmetries are bad neighbours. \emph{In the presence of a translational symmetry only two-, three-, four-, and sixfold rotation axes are possible}. Let's prove this.
 
Let points $A$ and $B$ in \hyperref[quad]{Figure~\ref{quad}} be sites of some plane lattice ($|AB| = a$). Suppose that through these sites $n$-fold rotation axes pass perpendicular to the plane of the lattice. We will now turn the lattice about axis $A$ through an angle $\alpha= \ang{360}$, and will designate by $C$ the new position of site $B$. If the lattice were turned through the angle $\alpha$ around axis $B$ in the opposite direction, site $A$ would come to $D$.

\begin{marginfigure}[2cm]
\includegraphics[width=0.9\textwidth]{figs/ch-02/quad.jpg}
\caption{Vectors defining a three-dimensional unit lattice.}
\label{quad}
\end{marginfigure}

The presence of translational symmetry requires that points $C$ and $D$ coincide with lattice sites. Hence,
\begin{equation*}
|CD|= m \, |AB|= ma
\end{equation*}
where $m$ is an integer. The trapezoid $ABDC$ (see the figure) being equilateral, we have $|CD| = a \pm 2a \cos \alpha$. Thus,
\begin{equation*}
 \alpha (1 \pm 2 \cos \alpha) = ma,
\end{equation*}
that is, $\cos \alpha = \pm (m- 1)/2$. Since $|\cos \alpha| \leq 1$,
\begin{equation*}
-2  \leqslant (m - 1) \leqslant 2,
\end{equation*}

It follows that only the following five cases are possible:
\begin{fullwidth}
\begin{enumerate}[label=(\arabic*),leftmargin=1cm]
\item $m= -1, \,\, \cos \alpha = -1, \,\, \alpha= \ang{180}, \,\, n = 2$ (two-fold rotational symmetry);
\item $m= 0, \,\, \cos \alpha = -1/2, \,\, \alpha= \ang{120}, \,\, n = 3$ (three-fold rotational symmetry);
\item $m= 1, \,\, \cos \alpha = 0, \,\, \alpha= \ang{90}, \,\, n = 4$ (four-fold rotational symmetry); 
\item $m= 2, \,\, \cos \alpha = 1/2, \,\, \alpha= \ang{60}, \,\, n = 6$ (six-fold rotational symmetry); 
\item $m= 3, \,\, \cos \alpha = 1, \,\, \alpha= 0$.
\end{enumerate}
\end{fullwidth}
Accordingly, with translational symmetry, five-fold rotation axes are
impossible in principle, as well as axes of an order higher than six.

\section{Glide Plane (Axis) of Symmetry}
 \begin{figure}[!ht]
\centering
\includegraphics[width=.8\textwidth]{figs/ch-02/glide-symmetry.jpg}
\caption{An example of glide symmetry with period $2a$.}
\label{glide-symm}
\end{figure}

It was shown earlier that successive turns and reflections may give rise to a new type of symmetry - the mirror-reflection symmetry. Combining turns or reflections with translations can also produce new- types of symmetry. By way of example, consider a symmetry involving the so-called \emph{glide plane of symmetry} (or rather \emph{glide axis of symmetry}, since the figure in question is a plane). \hyperref[glide-symm]{Figure~\ref{glide-symm}} depicts a design exhibiting a translational symmetry along axis $AB$ with period $2a$. It is easily seen that another symmetry can be revealed here - the symmetry relative to the displacement along $AB$ with period $a$ followed by the reflection about axis $AB$. Axis $AB$ is termed a glide axis of symmetry with period $a$.



