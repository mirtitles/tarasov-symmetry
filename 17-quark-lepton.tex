% !TEX root = tarasov-symmetry.tex






\chapter{Quark-Lepton Symmetry}

\marginnote{The symmetry between quarks and leptons today appears quite significant. It suggests that with all their striking dissimilarity, there is something common in the nature of these particles. It is the creation of the unified theory of quarks and leptons that, it seems, is going to be the main effort of physicists in future. \newline - L. Okun}
Up until recently physicists were bewildered by the disagreement
between the abundance of hadrons and a modest number of lepton types.
Perhaps that is why a hypothesis put forward in 1964 appeared to be so
attractive. According to this hypothesis, all hadrons consist of several
elementary "building blocks" called quarks. With the passage of years the
quark hypothesis gradually gained ground. It is formulated as the rule:
the number of quark types must be equal to the number of lepton types. This
rule reflects the quark-lepton symmetry, which is as yet quite enigmatic.

\section{Quarks}
Unitary symmetry allows the existence of supermultiplets not only of
eight or ten particles, but also other ones; specifically, supermultiplets are
possible that contain only three particles. In the plane $I_{\zeta, \, Y}$ these
``particles'' form a triangle shown in Figure~\ref{three-supermultiplet}~(a). The appropriate
``antiparticles'' form a triangle in Figure~\ref{three-supermultiplet}~(b). In the figure, $u, d, s$ stand for
the ``particles'' and $\bar{u}, \bar{d}, \bar{s}$ for the ``antiparticles''.
\begin{figure}[!h]
\centering
\includegraphics[width=0.85\textwidth]{figs/ch-17/three-supermultiplet.jpg}
\caption{Supermultiplet with three particles.}
\label{three-supermultiplet}
\end{figure}
Among the known elementary particles (antiparticles) there are no
``particles'' that are included in the triplets shown in Figure~\ref{three-supermultiplet}. And yet
these ``particles'', called quarks, for more than 15 years now have
attracted the attention of physicists. In 1964 Gell-Mann and Zweig
pointed out that three quarks in combination with three antiquarks can,
in principle, be those ``building blocks'' of which all the known hadrons
(mesons and baryons) and their antiparticles are constructed.

The characteristics of quarks $u, d, s$ and their respective antiquarks are
summarized in Figure~\ref{quarks-anti-quarks}. Quarks do not have an integral, but fractional
electric charge ($+ 2/3$ or $-1/3$). They are fermions (spin $1/2$); this is only
natural because only out of fermions can we construct both fermions and
bosons (an odd number of fermions gives a fermion, an even number of
fermions gives a boson). Quarks $u$ and $d$ have no strangeness, quark $s$ has
the strangeness $S = -1$ (the $s$-quark is, as it were, a carrier of
strangeness).
\begin{figure}[!h]
\centering
\includegraphics[width=0.75\textwidth]{figs/ch-17/quarks-anti-quarks.jpg}
\caption{Table showing varieties of quarks and their properties.}
\label{quarks-anti-quarks}
\end{figure}

Hadrons are constructed out of quarks according to the following
simple rule: \emph{the baryon consists of three quarks} (antibaryon out of three
antiquarks), and the \emph{meson, out of a quark and an antiquark}. So. for
example, pion $\pi^{+}$ has the quark structure $u\bar{d}$, and its antiparticle (pion $\pi^{-}$),
the structure $\bar{u}d$. The structure of kaons has the strange antiquark
$\bar{s}(K^{+} = u\bar{s}, \, K^{0} = d\bar{s})$. The quark structure of long-lived baryons is
represented in Figure~\ref{quark-structure}. It is seen that the structure of most baryons
includes pairs of identical quarks, and in the hyperon $\Omega^{-}$ all the three
quarks are identical. Furthermore, different baryons may have the same
quark structure (hyperons $\Lambda^{0}$ and $\Sigma^{0}$). This means that a quark may be in
different states. So we should take into account the possibility of two spin
states of a quark. This does not need to be considered in the case of the
hyperon $\Omega^{-}$. The spin of this hyperon being $3/2$, all three $s$-quarks are in
the same spin state. Quarks are fermions, therefore, according to Pauli's
exclusion principle, the three above $s$-quarks must differ in some
additional parameter. In quark theory this parameter is called ``colour''.
\begin{figure}[!h]
\centering
\includegraphics[width=0.75\textwidth]{figs/ch-17/quark-structure.jpg}
\caption{Quark composition of baryons.}
\label{quark-structure}
\end{figure}

According to modern views, each quark (antiquark) comes in \emph{three
varieties}, conditionally called \emph{colours}. So, for example, there is a red
$s$-quark, yellow $s$-quark, and blue $s$-quark. To be sure, the notion of
quark colour should not be taken literally.
\begin{marginfigure}
\centering
\includegraphics[width=0.7\textwidth]{figs/ch-17/baryon-colours.jpg}
\caption{The colours of quarks.}
\label{baryon-colours}
\end{marginfigure}
Significantly, every baryon includes quarks of \emph{different} colours
(Figure~\ref{baryon-colours}). Using the colour terminology, we can say that in each baryon
three main colours are blended and so baryons can be viewed as
\emph{colourless} (white) objects. Mesons are colourless as well, since the colour
of an antiquark is always complementary in relation to the quark colour
in this meson. The theory of coloured quarks (quantum
chromodynamics) explains why we do not encounter in nature particles
consisting, say, of two or four quarks and, specifically, individual (free)
quarks. This is related to the fact that the \emph{hadrons (antihadrons) observed
in nature must be colourless by all means}. Clearly, we cannot produce
a colourless combination from one, two, or four quarks.

The theory shows quite convincingly why the hadrons occurring in
nature must be colourless. But how stringent is the requirement of
colourlessness? The final answer, clearly, comes from experiment The
experimental search for free quarks has been under way for 15 years
already, but to no avail.

Having read that no free quarks have as yet been found, the reader may
doubt the physical reality of the quark hypothesis and may regard it as
just an elegant mathematical trick. In 1965 Ya. Zeldovich wrote: 
\begin{quote}
The dilemma facing physics now can be formulated as follows: either only the classification and symmetry properties of known particles are clarified, or this symmetry is a consequence of the existence of quarks, that is,
absolutely new fundamental type of matter, atomism of new type.
\end{quote}
Another decade passed and physicists saw that the quark hypothesis was
associated with the existence of a new type of atomism. In other words,
by the end of the 1970s physicists no longer doubted that quarks in
hadrons really exist.

What made them so sure? In the first place, three quarks (plus three
antiquarks) made it possible to build all the hadrons (antihadrons)
discovered up until 1974. It is remarkable that this construction
produced no extraneous objects-all the particles constructed from
quarks (antiquarks) according to the rules mentioned earlier have
eventually been found experimentally. The quark model enabled the
physicists to work out correctly the various characteristics of hadrons,
the probabilities of transformations, and so on. Experiments on the
scattering high-energy electrons at nucleons allowed quarks to be
literally groped for within nucleons. Conclusive evidence for the validity
of the quark hypothesis came from the discovery of new types of particles
that were christened \emph{charmed particles}.

\section{The Charmed World}

In November 1974 at the accelerator of Stanford, USA, a new particle of
mass about $6000 m$ and lifetime about \SI{d-20}{\second} was discovered. This
particle is now known as the $J/\Psi$-meson. The spin of the $J/\Psi$ is unity. Like
mesons $\pi^{0}$ and $\eta^{0}$, $J/\Psi$ is truly neutral.

The new meson does not conform to any of the earlier theoretical
schemes, it would have a lifetime approximately 1000 times shorter. To
describe the quark structure of $J/\Psi$-meson a new quark had to be
introduced-the so-called $c$-quark and a new conservable quantity called
``charm''. Just like strangeness and parity, charm is conserved in strong
and electromagnetic interactions, but not in weak ones. It is \emph{charm
conservation} that is responsible for the relatively long lifetime of the
$J/\Psi$-meson.

When the $c$-quark was introduced, the number of quark types became
equal to four. Note that the $c$-quark is the carrier of charm, just like the
$s$-quark is the carrier of strangeness. The electric charge of the $c$-quark is
$+2/3$.
\begin{marginfigure}
\centering
\includegraphics[width=0.7\textwidth]{figs/ch-17/charmonium.jpg}
\caption{The positronium and charmonium.}
\label{charmonium}
\end{marginfigure}
The quark structure of the $J/\Psi$-meson is $c\bar{c}$ (this structure explains
specifically the true neutrality of the meson). The structure $c\bar{c}$ is called the
\emph{charmonium} and it is treated as an atom-like system resembling the
long-known \emph{positronium} (Figure~\ref{charmonium}). Recall that the positronium
represents an ``atom'' consisting of an electron and a positron orbiting
around a common centre of mass. (The existence of the bound system
including an electron and its antiparticle was established experimentally
in 1951; the lifetime of the positronium is as long as \SI{d-7}{\second}.) Like any
atom, the charmonium is characterized by a system of energy levels. The
$J/\Psi$-meson corresponds to one of the levels of charmonium. Soon after
the $J/\Psi$-meson had been discovered, a number of mesons were found as
well ($\Psi^{1}, \, \chi_{0},\, \chi_{1}, \, \chi_{2}$, and so on), which could be correlated with various
levels of charmonium. Studies of charmonium properties is of great
interest - they provide information about the interaction of quarks.


The charms of the $c$-quark and $\bar{c}$-antiquark have opposite signs. 
Therefore, the resultant (total) charm of the $c\bar{c}$-structure is zero. It is said that
the $c\bar{c}$-structure has a \emph{hidden charm}. Mesons with open charm were
discovered in the summer of 1976: the $D^{0}$-meson ($c\bar{u}$-structure) and the
$D^{+}$ -meson ($c\bar{d}$-structure). Their behaviour appeared to be in complete
agreement with the hypothesis of the charmed $c$-quark. In 1977 the
$F^{+}$-meson ($c\bar{s}$-structure) was discovered, which apart from charm also
has strangeness.

The discovery of charmed particles proved experimentally the
existence of the $c$-quark. And since the very $c$-quark and its properties are
closely linked to those of $u, d, s$ quarks, then the \emph{quark model as a whole
was proved experimentally}.

\begin{figure}[!h]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-17/meson-multiplet-polyhedron.jpg}
\caption{A polyhedron corresponding to the meson supermultiplet consisting of
fifteen mesons.}
\label{meson-multiplet-polyhedron}
\end{figure}


Returning to the issue of unitary symmetry of the strong interaction,
note that allowing for charm the supermultiplets of hadrons take the
form of \emph{volume} bodies (polyhedrons) in the space in which the axes are $I_{\zeta},Y,C$ (recall that here $C$ is charm, $I_{\zeta}$ is the isospin projection, $Y$ is the sum
of the baryon number and strangeness called the \emph{hypercharge}). The 
supermultiplets presented in Figures~\ref{eight-baryons}-\ref{ten-resonances} (see Chapter~16) are the sections of such polyhedrons by the plane $C=0$. Given in Figure~124 is an example of
the polyhedron corresponding to the meson supermultiplet consisting of
fifteen mesons.

\section{Quark-Lepton Symmetry}

It is to be stressed that the \emph{representatives of hadrons in weak interactions
are quarks}. Consider two examples: the decay of the neutron( $n \to + e^{-} + \bar{\nu}_{e}$) and the collision of a neutrino with a neutron ($\nu_{e} +n \to p + e^{-}$).
The decay of the neutron comes down to the decay of one of the d-quarks,
which is a constituent part of the neutron, into a u-quark and leptons:
\begin{equation*}%
d \to u + e^{-} + \bar{\nu}_{e}
\end{equation*}
The collision of a neutrino with a neutron resulting in the production
of a proton and an electron boils down to the collision of a neutrino and
a $d$-quark that enters the composition of the neutron, with the result that
the $d$-quark turns into a $u$-quark and in the process an electron is
produced:
\begin{equation*}%
\nu_{e} + d \to u + e^{-}
\end{equation*}
The above processes involve a pair of $e \nu_{e}$ (or $e \bar{\nu}_{e}$) leptons and a pair of
$ud$-quarks. Other forms of weak processes are also possible. So, for
example, the lepton pair $\mu \nu$ may interact with the quark pair $us$. Any
weak process is an interaction of a \emph{lepton pair with a quark pair}.

This removes the discrepancy between the small number of lepton
types and the vast number of hadrons. The number of leptons must be
compared not with the number of hadrons but with the number of
quarks. It appears that between leptons and quarks some sort of
symmetry exists: \emph{the number of lepton types must be exactly equal to the
number of quark types}. This conclusion follows from the theory based on
a vast body of experimental evidence and, in particular, on the data on
the decay of strange particles and the nonconservation of space parity in
weak interactions.

Before the charmed particles had been discovered, there was no
symmetry between leptons and quarks: only three quarks $(u, \, d, \, s)$
corresponded to the four leptons $e^{-}, \, v_{e}, \, \mu^{-}, \, \nu{\mu}$. Therefore, the $c$-quark
was a welcome arrival.

The scheme of four leptons and four quarks suffered, however, from
one drawback. The number of leptons (quarks) involved was insufficient
to account for the nonconservation of combined parity in decays of
neutral kaons (the nonconservation of combined parity was noted
specifically in Chapter~14). At least six leptons (and as many quarks) were
required

\section{A New Discovery}

The first evidence for the existence of a fifth lepton came in 1975. In 1977
the hypothesis became certainty. The fifth lepton was called the \emph{tauon}
($\tau^{-}$). Its mass was found to be $3500 m$. Apart from the new lepton another
neutrino-tauon neutrino ($\nu_{\tau}$) must exist.

In the summer of 1977 at the Fermi National Accelerator Laboratory,
USA, superheavy mesons with a mass of about $20000 m$ (epsilon-messon
$\theta$) were discovered. It was found that these mesons represented a 
structure of a quark and an antiquark of a new type. This quark ($b$-quark) is
the carrier of the quantity that is conserved in strong interactions. This
quantity was dubbed \emph{beauty}-this explains the abbreviation for the fifth
quark. The electric charge of the $b$-quark is $-1/3$, its mass is about
$10000 m$.

At present, a search is under way for beautiful hadrons, and also for
a sixth quark. If the third quark ($s$-quark) is called \emph{strange}, the fourth
quark ($c$-quark) is \emph{charmed}, the fifth quark ($b$-quark) is \emph{beautiful}, but it has
been decided to call the sixth quark \emph{true} quark, or $t$-quark.

The scheme of the six leptons and six quarks seems to be quite
attractive to physicists today. The future will show whether or not this
quark-lepton scheme is a final one or the number of leptons (quarks) will
continue to grow.
%
%\begin{figure*}[!h]
%\centering
%\includegraphics[width=0.9\textwidth]{figs/ch-04/regular-polyhedra.png} 
%\caption{The five possible types of regular polyhedra. \label{regular-poly}} 
%\end{figure*}
%
%\begin{marginfigure}
%\centering
%\includegraphics[width=0.9\textwidth]{figs/ch-04/dodecahedron-symm.png}
%\caption{The symmetry of dodecahedron.}
%\label{dodeca-symm}
%\end{marginfigure}
%
%
%\begin{figure}[!h]
%\centering
%\includegraphics[width=0.75\textwidth]{figs/ch-04/kepler.png}
%\caption{Scheme of the Solar System based on the regular polyhedra devised by Kepler.}
%\label{kepler-scheme}
%\end{figure}
%
%
%
%                        