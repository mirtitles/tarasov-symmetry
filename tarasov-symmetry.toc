\contentsline {chapter}{Preface}{13}{section*.3}
\contentsline {chapter}{A Conversation}{15}{chapter*.4}
\contentsline {part}{I\hspace {1em}Symmetry Around Us}{23}{part.1}
\contentsline {chapter}{\numberline {1}Mirror Symmetry }{27}{chapter.1}
\contentsline {section}{\numberline {1}An Object and Its Mirror Twin}{27}{section.1.1}
\contentsline {section}{\numberline {2}Mirror Symmetry}{30}{section.1.2}
\contentsline {section}{\numberline {3}Enantiomorphs}{31}{section.1.3}
\contentsline {chapter}{\numberline {2}Other Kinds of Symmetry }{35}{chapter.2}
\contentsline {section}{\numberline {4}Rotational Symmetry}{35}{section.2.4}
\contentsline {section}{\numberline {5}Mirror-Rotational Symmetry}{37}{section.2.5}
\contentsline {section}{\numberline {6}Translational Symmetry}{37}{section.2.6}
\contentsline {section}{\numberline {7}Bad Neighbours}{39}{section.2.7}
\contentsline {section}{\numberline {8}Glide Plane (Axis) of Symmetry}{41}{section.2.8}
\contentsline {chapter}{\numberline {3}Borders and Patterns}{43}{chapter.3}
\contentsline {section}{\numberline {9}Borders}{43}{section.3.9}
\contentsline {section}{\numberline {10}Decorative Patterns}{45}{section.3.10}
\contentsline {section}{\numberline {11}Pattern Construction}{49}{section.3.11}
\contentsline {section}{\numberline {12}The `Lizards' Design}{51}{section.3.12}
\contentsline {chapter}{\numberline {4}Regular Polyhedra}{53}{chapter.4}
\contentsline {section}{\numberline {13}The Five Platonic Solids}{53}{section.4.13}
\contentsline {section}{\numberline {14}The Symmetry of the Regular Polyhedra}{54}{section.4.14}
\contentsline {section}{\numberline {15}The Uses of the Platonic Solids to Explain Some Fundamental Problems}{55}{section.4.15}
\contentsline {section}{\numberline {16}On the Role of Symmetry in the Cognition of Nature}{57}{section.4.16}
\contentsline {chapter}{\numberline {5}Symmetry In Nature}{59}{chapter.5}
\contentsline {section}{\numberline {17}From the Concept of Symmetry to the Real Picture of a Symmetrical World}{59}{section.5.17}
\contentsline {section}{\numberline {18}Symmetry in Inanimate Nature. Crystals}{61}{section.5.18}
\contentsline {section}{\numberline {19}Symmetry in the World of Plants}{63}{section.5.19}
\contentsline {section}{\numberline {20}Symmetry in the World of Animals}{66}{section.5.20}
\contentsline {section}{\numberline {21}Inhabitants of Other Worlds}{69}{section.5.21}
\contentsline {chapter}{\numberline {6}Order in the World of Atoms}{71}{chapter.6}
\contentsline {section}{\numberline {22}Molecules}{71}{section.6.22}
\contentsline {section}{\numberline {23}The Puzzle of the Benzene Ring}{72}{section.6.23}
\contentsline {section}{\numberline {24}The Crystal Lattice}{73}{section.6.24}
\contentsline {section}{\numberline {25}The Face-Centred Cubic Lattice}{74}{section.6.25}
\contentsline {section}{\numberline {26}Polymorphism}{76}{section.6.26}
\contentsline {section}{\numberline {27}The Crystal Lattice and the External Appearance of a Crystal}{77}{section.6.27}
\contentsline {section}{\numberline {28}The Experimental Study of Crystal Structures}{78}{section.6.28}
\contentsline {section}{\numberline {29}The Mysteries of Water}{79}{section.6.29}
\contentsline {section}{\numberline {30}Magnetic Structures}{80}{section.6.30}
\contentsline {section}{\numberline {31}Order and Disorder}{82}{section.6.31}
\contentsline {chapter}{\numberline {7}Spirality In Nature}{85}{chapter.7}
\contentsline {section}{\numberline {32}The Symmetry and Asymmetry of the Helix}{85}{section.7.32}
\contentsline {section}{\numberline {33}Helices in Nature}{87}{section.7.33}
\contentsline {section}{\numberline {34}The DNA Molecule}{89}{section.7.34}
\contentsline {section}{\numberline {35}The Rotation of the Plane of Light Polarization}{91}{section.7.35}
\contentsline {section}{\numberline {36}Left and Right Molecules. Stereoisomerism}{92}{section.7.36}
\contentsline {section}{\numberline {37}The Left-Right Asymmetry of Molecules and Life}{93}{section.7.37}
\contentsline {part}{II\hspace {1em}Symmetry Around Us}{97}{part.2}
\contentsline {chapter}{\numberline {8}Symmetry and The Relativity of Motion}{101}{chapter.8}
\contentsline {section}{\numberline {38}The Relativity Principle}{101}{section.8.38}
\contentsline {section}{\numberline {39}The Relativity of Simultaneous Events}{102}{section.8.39}
\contentsline {section}{\numberline {40}The Lorentz Transformations}{103}{section.8.40}
\contentsline {section}{\numberline {41}The Relativity of Time Periods}{105}{section.8.41}
\contentsline {section}{\numberline {42}The Speed in Various Frames}{106}{section.8.42}
\contentsline {chapter}{\numberline {9}The Symmetry of Physical Laws}{109}{chapter.9}
\contentsline {section}{\numberline {43}Symmetry Under Spatial Translations}{109}{section.9.43}
\contentsline {section}{\numberline {44}Rotational Symmetry}{111}{section.9.44}
\contentsline {section}{\numberline {45}Symmetry in Time}{112}{section.9.45}
\contentsline {section}{\numberline {46}The Symmetry Under Mirror Reflection}{113}{section.9.46}
\contentsline {section}{\numberline {47}An Example of Asymmetry of Physical Laws}{114}{section.9.47}
\contentsline {chapter}{\numberline {10}Conservation Laws}{117}{chapter.10}
\contentsline {section}{\numberline {48}An Unusual Adventure of Baron M\"unchhausen}{117}{section.10.48}
\contentsline {section}{\numberline {49}The Problem of Billiard Balls}{118}{section.10.49}
\contentsline {section}{\numberline {50}On the Law of Conservation of Momentum}{121}{section.10.50}
\contentsline {section}{\numberline {51}The Vector Product of Two Vectors}{122}{section.10.51}
\contentsline {section}{\numberline {52}Kepler's Second Law}{123}{section.10.52}
\contentsline {section}{\numberline {53}Conservation of the Intrinsic Angular Momentum of a Rotating Body}{126}{section.10.53}
\contentsline {chapter}{\numberline {11}Symmetry and Conservation Laws}{129}{chapter.11}
\contentsline {section}{\numberline {54}The Relationship of Space and Time Symmetry to Conservation Laws}{129}{section.11.54}
\contentsline {section}{\numberline {55}The Universal and Fundamental Nature of Conservation Laws}{130}{section.11.55}
\contentsline {section}{\numberline {56}The Practical Value of Conservation Laws}{133}{section.11.56}
\contentsline {section}{\numberline {57}The Example of the Compton Effect}{134}{section.11.57}
\contentsline {section}{\numberline {58}Conservation Laws as Prohibiting Rules}{136}{section.11.58}
\contentsline {chapter}{\numberline {12}The World of Elementary Particles}{139}{chapter.12}
\contentsline {section}{\numberline {59}Some Features of Particles}{139}{section.12.59}
\contentsline {section}{\numberline {60}The Zoo of Elementary Particles}{141}{section.12.60}
\contentsline {section}{\numberline {61}Particles and Antiparticles}{142}{section.12.61}
\contentsline {section}{\numberline {62}Particles, Antiparticles and Symmetry}{146}{section.12.62}
\contentsline {section}{\numberline {63}Neutrino and Antineutrino}{148}{section.12.63}
\contentsline {section}{\numberline {64}The Instability of Particles}{149}{section.12.64}
\contentsline {section}{\numberline {65}Inter-conversions of Particles}{152}{section.12.65}
\contentsline {chapter}{\numberline {13}Conservation Laws and Particles}{157}{chapter.13}
\contentsline {section}{\numberline {66}Conservation of Energy and Momentum in Particle Reactions}{157}{section.13.66}
\contentsline {section}{\numberline {67}The Conservation of Electric Charge and Stability of the Electron}{159}{section.13.67}
\contentsline {section}{\numberline {68}The Three Conservation Laws and Neutrino}{160}{section.13.68}
\contentsline {section}{\numberline {69}Experimental Determination of Electron Antineutrino}{162}{section.13.69}
\contentsline {section}{\numberline {70}Electron and Muon Numbers. Electron and Muon Neutrinos}{163}{section.13.70}
\contentsline {section}{\numberline {71}The Baryon Number and Stability of the Proton}{165}{section.13.71}
\contentsline {section}{\numberline {72}Discrete Symmetries. $CPT$-Invariance}{167}{section.13.72}
\contentsline {chapter}{\numberline {14}The Ozma Problem}{171}{chapter.14}
\contentsline {section}{\numberline {73}What Is the Ozma Problem?}{171}{section.14.73}
\contentsline {section}{\numberline {74}The Ozma Problem Before 1956}{173}{section.14.74}
\contentsline {section}{\numberline {75}The Mirror Asymmetry of Beta-Decay Processes}{174}{section.14.75}
\contentsline {section}{\numberline {76}The Mirror Asymmetry in Decay Processes and the Ozma Problem}{175}{section.14.76}
\contentsline {section}{\numberline {77}The Fall of Charge-Conjugation Symmetry}{176}{section.14.77}
\contentsline {section}{\numberline {78}Combined Parity}{177}{section.14.78}
\contentsline {section}{\numberline {79}Combined Parity and the Ozma Problem}{179}{section.14.79}
\contentsline {section}{\numberline {80}The Solution to the Ozma Problem}{180}{section.14.80}
\contentsline {chapter}{\numberline {15}Fermions and Bosons}{183}{chapter.15}
\contentsline {section}{\numberline {81}The Periodic Table and the Pauli Principle}{183}{section.15.81}
\contentsline {section}{\numberline {82}Commutative Symmetry. Fermions and Bosons}{184}{section.15.82}
\contentsline {section}{\numberline {83}Symmetrical and Antisymmetrical Wave Functions}{185}{section.15.83}
\contentsline {section}{\numberline {84}The Superfluidity of Liquid Helium. Superconductivity}{187}{section.15.84}
\contentsline {section}{\numberline {85}Induced Light Generation and Lasers}{188}{section.15.85}
\contentsline {chapter}{\numberline {16}The Symmetry of Various Interactions}{189}{chapter.16}
\contentsline {section}{\numberline {86}The Principal Types of Interactions}{189}{section.16.86}
\contentsline {section}{\numberline {87}Isotopic Invariance of Strong Interactions. The Isotopic Spin (Isospin)}{191}{section.16.87}
\contentsline {section}{\numberline {88}Strangeness Conservation in Strong and Electromagnetic Interactions}{194}{section.16.88}
\contentsline {section}{\numberline {89}Interactions and Conservations}{197}{section.16.89}
\contentsline {section}{\numberline {90}A Curious Formula}{198}{section.16.90}
\contentsline {section}{\numberline {91}The Unitary Symmetry of Strong Interactions}{199}{section.16.91}
\contentsline {chapter}{\numberline {17}Quark-Lepton Symmetry}{203}{chapter.17}
\contentsline {section}{\numberline {92}Quarks}{203}{section.17.92}
\contentsline {section}{\numberline {93}The Charmed World}{207}{section.17.93}
\contentsline {section}{\numberline {94}Quark-Lepton Symmetry}{208}{section.17.94}
\contentsline {section}{\numberline {95}A New Discovery}{210}{section.17.95}
\contentsline {chapter}{A Conversation Between the Author and the Reader About the Role of Symmetry}{213}{chapter*.81}
\contentsline {section}{The Ubiquitous Symmetry}{213}{section*.82}
\contentsline {section}{The Development of the Concept of Symmetry}{215}{section*.82}
\contentsline {section}{Symmetry-Asymmetry}{217}{Item.48}
\contentsline {section}{On the Role of Symmetry in the Scientific Quest for Knowledge}{219}{Item.48}
\contentsline {section}{Symmetry in Creative Arts}{222}{Item.48}
\contentsline {chapter}{Literature}{231}{chapter*.86}
\contentsfinish 
