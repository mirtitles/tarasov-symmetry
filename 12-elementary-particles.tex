% !TEX root = tarasov-symmetry.tex






\chapter{The World of Elementary Particles}

\marginnote{The elementary particles are not just interesting scientific curiosities. They represent the deepest-lying  substructure of matter to which man has been able to probe;  consequently, they provide one of the most challenging problems on the current frontiers of science. - K. Ford}

\emph{Elementary particles} are the frontier of modern physics which
corresponds to the most fundamental level of probing into the physical
picture of the world. Naturally, it is here that the most important
regularities show up, which, in the final analysis, control the structure of
matter and the character of physical processes. It is of principal
importance, therefore, to gain an insight into the aspects related to
conservation and invariance in the world of elementary particles.
But before we proceed to discuss these issues, we will have to take
a look at the currently known elementary particles.

\section{Some Features of Particles}

We will single out three quantities characterizing particles - \emph{mass, electric
charge}, and \emph{spin}. The ensemble of characteristics will be extended
markedly later. In addition, it will include lifetime, specific charges
(electronic, muonic, and baryonic), isospin, strangeness, and so on.

By mass we will understand the \emph{rest mass} of a particle, that is, the mass
in the frame of reference connected with the particle itself. The smallest
mass is possessed by the electron ($m = \SI{9.1e-28}{ \gram}$); therefore, the mass
of other particles is often expressed in electron masses. Mass is
also expressed in energy units \si{\mega\electronvolt} (megaelectronvolts). The use of
energy units for mass is based on the well-known relationship due to
Einstein: $E = mc^{2}$. In terms of energy units, the electron mass is
\SI{0.511}{\mega\electronvolt}.

The \emph{electric charge} of particles is denoted by numerals: $0, \, +1, \, -1$. In
the first case, there is no charge (the particle is neutral). In the second
case, the charge is equal to that of an electron, but unlike the electron, it is
positive. In the third case, the charge coincides with the electron charge
both in magnitude and in sign. Note that the electric charge of charged
particles is exactly equal to the electron charge, that is, \SI{1.6e-19}{\coulomb}.

The \emph{spin} of a particle is the specific angular momentum of a particle
which can be called the \emph{intrinsic angular momentum} since it is not related
to motions of the particle in space; it is indestructible, its magnitude is
independent of external conditions. This angular momentum can
arbitrarily be associated with the rotation of the particle about its own
axis. An analogue of spin may be the intrinsic angular momentum of
a planet or gyroscope as discussed in Chapter 10. The squared spin is
given by the expression $\hbar^{2} \, s(s+ 1)$, where $\hbar$ is Planck's constant, $s$ is
a number characterizing this particle, which is normally referred to as its
spin (in the latter case spin is measured in units of $\hbar$). Like any angular
momentum, spin is a vector quantity. This vector is quite specific,
however, since its \emph{projection} in a given fixed direction only takes on
\emph{discrete} values (is quantized): $\hbar s, \, \hbar(s - 1), \ldots,  -\hbar s$. The total number of
spin projections is $2s + 1$. In this connection, it is said that a particle with
spin $s$ may be in one of $2s + 1$ \emph{spin states}.

For many elementary particles, specifically for the electron, spin is 1/2. These particles have two spin states, one for each of the opposite spin
directions.

Note that all the particles of this type (for example, all electrons) have
exactly the same mass, charge, and spin. It is in principle impossible for
the mass of one electron to differ from that of another one by, say, 0.001
per cent. The values of the mass, electric charge, and spin of the electron
are, according to the evidence available, the lowest values of these
quantities ever encountered in nature, save for the cases where a particle
does not have a rest mass, charge, or spin.

\section{The Zoo of Elementary Particles}

The particles in the zoo are generally classed according to their mass,
charge, and spin. They form three families:

The \emph{first family} is the smallest - it consists only of one particle. It is the
photon, the quantum of electromagnetic radiation (symbol $\gamma$). The rest
mass and electric charge of the photon are zero, $s = 1$. Note that
according to the theory of relativity, any particle with zero rest mass
cannot have an electric charge and in any inertial frame of reference it
travels with the same velocity - the velocity of light in empty space. The
photon is an example of such a particle.

The \emph{second family} consists of particles called \emph{leptons}. Up until 1975 four
leptons were known: \emph{electron} ($e^{~}$), \emph{electron neutrino} ($\mu_{e}$), \emph{muon} ($\mu^{~}$), and \emph{muon neutrino} ($\nu_{\mu}$). We have already discussed the electron. The muon has
a mass of $207m$, its electric charge is negative, $s = 1/2$. Both neutrinos are
indistinguishable in terms of the three characteristics used here (no
wonder that for a long time it was believed that there only exists one type
of neutrino in nature). Like the photon, both neutrinos have neither rest
mass nor electric charge. Unlike the photon, however, the spin of the
neutrino is $1/2$, like that of any lepton.

In 1975 a fifth lepton, \emph{tauon} ($\tau^{~}$), was discovered. It appeared to be an ultra-heavy particle: its mass is about $3500m$. The electric charge of the tauon is negative. Physicists have good reasons to think that a tauon must have a companion particle, a \emph{tauon neutrino} ($\nu_{\tau}$). Counting the third
type of neutrino, the number of leptons becomes six.

The \emph{third family} consists of particles called \emph{hadrons} (from the Greek for ``large'', ``massive''). Hadrons are numerous: several hundred of them are known.

The hadron family divides into two subfamilies: the \emph{mesons} and the \emph{baryons}. The mesons either have no spin or have integer spins, whereas the baryons have half-integral spins. Among the hadrons (both mesons and baryons) there are many particles that decay quickly, their lifetime is only \SIrange{d-22}{d-23}{\second}; these particles are called \emph{resonances}.  If we don't include the resonances, then up until 1974 physicists have identified 14 hadrons, among them five mesons and nine baryons.

The five mesons include two \emph{pions} (the neutral pion $\pi^{0}$ and the positively charged pion $\pi^{+}$), the \emph{kaons} (the positively charged kaon $K^{+}$ and the neutral kaon $K^{0}$), the neutral \emph{eta meson} ($\eta^{0}$). All of these mesons are spinless particles ($s = 0$). Their masses are as follows: $\pi^{0} =264 m, \, \pi^{+} = 273 m, \, K^{+}  = 966 m, \, K^{0} = 974m, \, \eta^{0}= 1074 m$.

The nine baryons include the \emph{nucleons} (the proton $p$ and the neutron $n$), the neutral \emph{lambda hyperon} $\Lambda^{0}$, the \emph{sigma hyperons} (the neutral one $\Sigma^{0}$ and the charged ones $\Sigma^{+}$ and $\Sigma^{~}$), the \emph{xi hyperons} (the neutral one $\Xi^{0}$ and the negatively charged one $\Xi^{-}$), the negatively charged \emph{omega hyperon} $\Omega^{-}$. The omega hyperon has the spin $3/2$; the other baryons have $s=1/2$. The masses of the above baryons are as follows: $p = 1836.1m, \, n = 1838.6m, \, \Lambda^{0}=2183m, \, \Sigma^{+}=2328m, \, \Sigma^{0}=2334m, \, \Sigma^{-}=
2343m, \, \Xi^{0}-2573m, \, \Xi^{-}=2586m, \, \Omega^{-} =3273m$.
\begin{figure}[!h]
\centering
\includegraphics[width=0.5\textwidth]{figs/ch-12/particles1.jpg}
\caption{Elementary particles showing leptons and hadrons.}
\label{particles1}
\end{figure}

Note that the particles of one group in the meson or baryon subfamily have similar masses. So with pions they only differ by 3 per cent, with kaons, by 0.7 per cent, with nucleons, only by 0.14 per cent. The members of a group mainly differ by the electric charge. In this connection the mesons or baryons of one group can be viewed as one particle characterized by \emph{several charge states}. With this approach the baryons $\Sigma^{+}\, \Sigma^{0} \, \Sigma^{-}$ are one particle (sigma hyperon) that may be in three different charge states. (The small difference in masses of the charge components is due to the difference in the sign of the electric charge.) Pion, kaon, nucleon, and xi hyperon have two charge states each. Figure~\ref{particles1} presents all the above elementary particles, with the exception of those with zero rest mass. The vertical axis in the figure shows the mass; the horizontal one, the electric charge. The yellow rectangles combine those mesons or baryons that may be treated as different charge states of a particle.

\begin{figure}[!h]
\centering
\includegraphics[width=0.45\textwidth]{figs/ch-12/particles2.jpg}
\caption{Elementary particles showing leptons and hadrons and antiparticles.}
\label{particles2}
\end{figure}

\section{Particles and Antiparticles}

The diagram of Figure~\ref{particles1} contains 17 particles. As a matter of fact, the number of particles to be considered is to be doubled. The fact is that each particle, with the exception of the photon, the neutral pion, and the eta meson, corresponds to an antiparticle. So for the electron we have its antiparticle, the positron ($e^{+}$); there are two antineutrinos, the electron one ($\nu_{e}$) and the muon one ($\nu_{\mu}$), and so on. The photon, the neutral pion, and the eta meson do not have antiparticles. We can say that each of these particles is identical to its antiparticle. Such particles are referred to as \emph{truly neutral} ones.

Including the antiparticles, the number of particles at hand becomes 37. The diagram in Figure~\ref{particles2} contains all of these particles, save for zero mass ones (photon, two neutrinos and two antineutrinos). The particles
in the figure are represented by the blue colour, the antiparticles by the red colour, and the truly neutral particles by the green colour. The diagram is far from comprehensive: it does not include the short-lived hadrons (resonances) and the hadrons discovered since 1974 (the so-called charmed hadrons).

Particles and their antiparticles have equal spins and masses, but the signs of their electric charges are opposite. So, unlike the proton, the antiproton is negative. But what if a particle is not charged? What is the difference, say, between a neutrino and antineutrino, or a neutron and antineutron? We will answer the question in more detail later in the book, for the moment we will only note that elementary particles are characterized not only by electric charge, but also by a number of other charges, which will be considered in Chapter 13. \emph{Particles and
antiparticles have opposite charges}.

It is seen in Figure~\ref{particles2} that antiparticles are normally denoted by a bar over the symbol for a particle. So $\bar{\Lambda}^{0}$ is the antiparticle for the lambda hyperon $\Lambda^{0}$, or antilambda hyperon. Since the charges of a particle and its antiparticle have opposite signs, in a number of cases (electron, muon, charged mesons) no bar is used. The electron is denoted by $e^{~}$ ; the antielectron (positron) by $e^{+}$. So $e^{+}, \, \mu^{+}, \,\pi^{-}, \, K^{~}$ are symbols for antiparticles, whereas $e^{-}, \, \mu^{-}, \,\pi^{+}, \, K^{+}$ are symbols for particles. But in the case of the sigma hyperon $\Sigma^{+}$, we cannot denote the antiparticle by
$\Sigma^{-}$, because there exists the sigma hyperon $\Sigma^{-}$. It is absolutely necessary here to use for the antiparticle the symbol $\bar{\Sigma}^{+}$. It should be remembered that the antisigma hyperon $\bar{\Sigma}^{+}$ has a negative electric charge.

But whatever the similarity between a particle and its antiparticle, they differ in a fundamental way: particle and antiparticle annihilate in merging with the result that several mesons or photons are formed. So $\Sigma^{+}$ and $\bar{\Sigma}^{+}$ are mutually destroyed, whereas the encounter of $\Sigma^{+}$ and $\Sigma^{-}$ is quite peaceful. The mutual destruction process (or in the language of physics, \emph{annihilation}) may be an indication of a meeting of a neutron and antineutron, and not of two neutrons.

Let us take some examples of events (reactions) of annihilation of particle and antiparticle:
\begin{align*}
e^{~} + e^{+} & \to \gamma + \gamma \\
p + \bar{p} & \to \pi^{+} + \pi^{~} + \pi^{0} \\
p + \bar{p}  & \to \pi^{+} + \pi^{+} + \pi^{-} + \pi^{-} \\
p + \bar{p}  & \to \pi^{+} + \pi^{+} + \pi^{-} + \pi^{-} + \pi^{0} 
\end{align*}
The first reaction is the annihilation of an electron and a positron
yielding two photons. The other three reactions are the various cases of
annihilation of a proton and an antiproton.

\section{Particles, Antiparticles and Symmetry}

The very fact that antiparticles exist is closely connected with symmetry. Without antiparticles the equations of theoretical physics describing the various types of elementary particles would be noninvariant under the Lorentz transformations, that is, under transfer from one inertial frame to another. Put another way, \emph{the existence of antiparticles in addition to particles is related to the invariance of the laws of physics under transfer from one inertial frame of reference to another}. This, unfortunately, lies beyond the scope of this book.

We live in a world of particles, antiparticles being fairly rare visitors. One can, however, imagine another world, the one built of antiparticles. We will call it the \emph{antiworld}. Antimatter in the antiworld consists of antiatoms and antimolecules. So the antiatom of hydrogen has the nucleus of an antiproton, with a positron orbiting in its field. In the antiatom of helium, two positrons orbit in the nuclear field, the nucleus consisting of two antiprotons and two antineutrons.

An encounter with the antiworld is a favourite theme of science fiction. After many years of flight at a speed near the speed of light a spaceship arrives at an unknown planet. The ship starts orbiting the planet and the astronauts start studying the planet's surface, using a most advanced optical apparatus that enables them to discern things up to one metre across from a height of several hundred kilometres. They find that the planet is inhabited and, moreover, the intelligent creatures look very much like people. At the same time, the astronauts endeavour to contact the dwellers of the planet. After having spent, say, a fortnight in orbit, the astronauts become confident that they have found a similar civilization which strikingly resembles their own terrestrial one. Meanwhile, the
space travellers and the extraterrestrials reach a measure of mutual understanding. The astronauts are kindly invited to land on the planet at a specified region. The terrestrial ship starts its engines and begins descending, approaching the lower atmosphere of the hospitable planet. And now a catastrophe happens- the shocked extraterrestrials see, high up in the sky where the spaceship was just visible, a blinding
flash \ldots

Today's science fiction reader or fan has already twigged that the hospitable planet is a piece of antiworld. Upon entry into the antimatter atmosphere, the terrestrial spacecraft was immediately destroyed due to the annihilation of the particles of the ship with the antiparticles of the atmosphere.

In this science fiction story there is one moment that is worth
examining. It is not surprising that the astronauts made the fatal mistake:
the antiworld of the planet must have appeared as natural as the world of
their Earth. The exchange of information by radio between the ship's
crew and the planet could also give no grounds to suspect that they
belonged to opposite worlds. \emph{World and antiworld are absolutely
symmetrical; the laws of nature are invariant under replacement of all the
particles by their respective antiparticles.} This invariance of the laws of
physics is normally referred to as \emph{charge invariance} (or \emph{C-invariance}). It is
not surprising then that when viewing from a distance a world built of
antiprotons, antineutrons, positrons, etc., the crew mistook it for
a conventional world, the one built of protons, neutrons, electrons, etc.
By the way, they contacted that world by the agency of photons, that is,
truly neutral particles, which are the same for both the conventional
world and the antiworld.

But the charge invariance of the laws of nature is not a completely
rigorous invariance: there are processes in which it does not hold. And
so, in principle, the space travellers could have made some experiments
on board their ship to clarify the nature of the world before them to
determine whether it was a conventional world or an antiworld. We are
going to take a closer look at this later in the book.

\emph{The symmetry of the physical properties of the world and the antiworld is
combined with the distinct asymmetry of the spatial distribution of matter
and antimatter.} It has been estimated that in our Galaxy, for each
antiparticle there are more than \num{d17} particles. It follows, in particular,
that the probability for a spacecraft to hit upon a star system made from
antimatter is infinitesimal.

This asymmetry has not yet been explained. It is to be assumed that
either the Universe as a whole is charge asymmetric or matter and
antimatter have spatially separated into isolated regions, which interact
with one another exceedingly weakly. Both assumptions pose a variety of
fundamental questions which, so far, scientists have not been able to
answer.

\section[Neutrino and Antineutrino]{Neutrino and Antineutrino (Left and Right Helices in the World
of Elementary Particles)}

\begin{marginfigure}
\centering
\includegraphics[width=0.85\textwidth]{figs/ch-12/neutrino.jpg}
\caption{The symmetry of dodecahedron.}
\label{neutrino12}
\end{marginfigure}

The spin of a neutrino is always aligned against (antiparallel to) the
neutrino's momentum. This means that if we visualize the spin through
the rotation about the neutrino's axis, the latter is always aligned with the
direction of motion of the particle. If we follow a neutrino flying off, it will
spin counterclockwise (Figure~\ref{neutrino12}~(a)). The antineutrino's rotation axis is also
parallel to the direction of motion. Unlike neutrino, however, the
receding antineutrino rotates clockwise (Figure~\ref{neutrino12}~(b)). In other words, the
neutrino can be compared to a left-handed helix and the antineutrino to
a right-handed helix. This holds good both for the electronic and for the
muonic neutrino (antineutrino).

It might appear that this model of neutrino (antineutrino) contradicts
the principle of invariance with respect to transfers from one inertial
frame of reference to another one. Suppose that a left-handed helix is
flying past us. In other words, it is a left-handed helix in the laboratory
frame of reference. Suppose further that we try to catch up with the
neutrino in a spacecraft travelling at a velocity higher than the neutrino's
velocity. Then in the frame associated with the ship the neutrino will now
move not away from us but toward us, the counterclockwise rotation of
the neutrino will not change in the process. This means that in the ship's
frame of reference the neutrino will be a right-handed helix, not a left-
handed one.

This reasoning is invalid, however. The fact is that in any inertial
frame, a neutrino travels at the velocity of light (recall that the neutrino's
rest mass is zero); therefore the ship cannot have a velocity higher than
that of the neutrino. And so the neutrino remains a left-handed helix in
any frame of reference.

\emph{Note}. The attentive reader may have noticed that such a model of
neutrino (antineutrino) does not agree with the invariance of the laws of
physics under mirror reflection. A mirror reflection turns the left-handed
helix into a right-handed one, and so a neutrino must turn into an
antineutrino. The reader is absolutely right. We will be looking at this
issue in more detail in Chapter 14.

\section{The Instability of Particles}

One of the most important features of elementary particles is that most of
them are \emph{unstable}. This means that particles decay spontaneously,
without any induction, yielding other particles. Exceptions are the
photon, neutrino, electron, and proton (with respective antiparticles);
these subatomic particles do not decay, they are stable.

A free neutron, for instance, decays spontaneously to produce three
stable particles, namely, a proton, an electron and an electron
antineutrino\sidenote{ Free neutrons are not stable, but atomic neutrons are stable. In unstable
nuclei, neutron may decay, the phenomenon being called the $\beta$-radioactivity of
atomic nuclei.}:
\begin{equation*}%
n  \to p + e^{~} + \bar{\nu}_{e}
\end{equation*}
A positively charged pion decays to yield an antimuon and a muor
neutrino
\begin{equation*}%
\pi^{+} \to \mu^{+} + \nu_{\mu}
\end{equation*}
the resultant antimuon decays, in turn, to give a positron, a muon
antineutrino, and an electron neutrino
\begin{equation*}%
\mu^{+} \to e^{+} + \bar{\nu}_{\mu} + \nu_{e}
\end{equation*}
A neutral pion disintegrates into two photons
\begin{equation*}%
\pi^{0} \to \gamma + \gamma
\end{equation*}

Figure~\ref{table-1} at the end of the chapter tabulates the principal decay schemes
for unstable particles.

The decay of an elementary particle is a phenomenon that is in need of some explanation. To begin with, we will consider the \emph{lifetime} of a particle before decay. For definiteness, we will discuss the neutron.
Suppose that at $t = 0$ we have $n_{0}$ free neutrons (assuming $n_{0}\gg 1$). In the
course of time, the neutrons will become ever fewer due to decay, that is,
$n(t)$ will be a decreasing exponential function
\begin{equation}%
n(t) = n_{0} \, \exp \left( - \dfrac{t}{\tau} \right)
%eq 12.1
\label{decay-exp}
\end{equation}
(here $e = 2.718 \ldots$ is the base of the natural logarithm). The curve of this
function is shown in Figure~\ref{decay-exp}. The constant ?, which has the dimensions
of time, is called the lifetime of the neutron. This is the time during which the number of neutrons will decrease $e$-fold. For neutrons $t=\SI{d3}{\second}$.

It is remarkable that the lifetime $\tau$ does not characterize the time of
existence of an individual neutron. An individual neutron may live just
a minute or a day. It is in principle impossible to foresee the moment at
which a concrete neutron will disintegrate. It only makes sense to speak
about the \emph{probability of decay}. The factor $e^{-t/\tau}$ on the right-hand side of
\eqref{decay-exp} is the factor describing the probability for individual neutrons to
decay during time t. It is immaterial in that case how long the neutron
has lived by $t = 0$; for all the neutrons, the probability that they may live
for time $t$ is absolutely the same. It can be said that neutrons do not age.

Despite the fact that an individual neutron may in principle live
indefinitely, the number of neutrons in a large ensemble falls off with time
following a definite law. Speaking about the lifetime, we do not mean the
lifetime of an individual neutron, but the time during which the total
number of neutrons decreases markedly (to be more exact, $e$-fold).

How then do we treat the very act of decay? One should not believe
that if a neutron decays into a proton, an electron, and an antineutrino, it
means that before the decay the neutron constituted some combination
of the above particles. The decay of a subatomic particle is by no means
a decay in the literal sense of the word. This is an \emph{act of transformation of
the initial particle into some set of new particles} in which the initial particle
is \emph{annihilated} and new particles are \emph{produced}.

It is worth noting that having discovered the $\beta$-radioactivity of atomic
nuclei (the emission of electrons by nuclei), scientists decided at first that
electrons enter the composition of nuclei. It was not until some time later
that they understood that the electrons of $\beta$-radiation \emph{are born at the time
of decay} of neutrons of $\beta$-radioactive nuclei.

Another argument against the literal interpretation of the term ``decay of a particle'' is the fact that many particles may decay in \emph{different} ways. So in the overwhelming majority of cases (more than 99 per cent), a positive pion decays following the above-mentioned scheme:
\begin{equation*}%
\pi^{+}  \to \mu^{+} + \nu_{\mu}
\end{equation*}
In other cases, however, it decays differently:
\begin{equation*}%
\pi^{+}  \to e^{+} + \nu_{e}
\end{equation*}
About a half of sigma hyperons $\Sigma^{+}$ disintegrate by the scheme
\begin{equation*}%
\Sigma^{+} \to p + \pi^{0},
\end{equation*}
whereas the other half by
\begin{equation*}%
\Sigma^{+} \to n + \pi^{+},
\end{equation*}
For a specific $\Sigma^{+}$-hyperon, it is impossible to predict how it will
disintegrate, let alone the time of the disintegration.

Let us return to the lifetime of particles. As stated above, for the
neutron this time is \SI{d3}{\second}. For comparison we will quote the lifetimes: for
muons, about \SI{d-6}{\second}; charged mesons, about \SI{d-8}{\second}; hyperons, about
\SI{d-10}{\second}. As compared with neutrons, all these particles are like butterflies
that live for only one day. And still, in terms of the microworld, they must
be brought under the heading of \emph{long-lived} particles. There are particles
that live far shorter - \SI{d-16}{\second} (the neutral pion) and even \SI{d-23}{\second} (the
so-called \emph{resonances}).

Worthy of special attention are the five subatomic particles that are
stable and do not decay at all. Three of them (the photon and two
neutrinos) move with the velocity of light in any inertial frame of
reference. The infinite lifetime of these particles is, in essence,
a consequence of the theory of relativity (see Chapter 8). More
surprisingly, two particles with nonzero rest masses-electron and
proton-also have infinite lifetimes. \emph{Their stability is based on symmetry,}
which is here expressed by specific conservation laws (see Chapter 13). It
is to be stressed that the stability of the electron and the proton is crucial
for the existence of stable atoms, and hence for our entire world.

\section{Inter-conversions of Particles}

In the world of elementary particles a wide variety of \emph{interconversions}
occur, as a result of which some particles are destroyed and others are
born. \emph{Decays of unstable particles} and \emph{annihilations} in collisions of
particles with antiparticles are examples of such interconversions.

Interconversions may also occur when \emph{particles collide with particles}.
The following are some examples of processes occurring when two
protons collide with each other:
\begin{align*}%
p + p & \to p + n +\pi^{+} \\
p + p & \to p + \Lambda^{0} + ?^{+}\\
p + p & \to p + \Sigma^{+} +?^{0}\\
p + p & \to n  + \Lambda^{0} + ?^{+} +\pi^{+} \\
p + p & \to p + \Xi^{0} + ?^{0}+?^{+} \\
? + ? & \to ? + ? + ? + \bar{?}
\end{align*}
It can easily be found that the total rest mass of the particles born in these
processes is larger than the double rest mass of a proton by a factor of
1.07, 1.36, 1.4, 1.43, 1.73, and 2, respectively. This suggests that in the
above processes the kinetic energy of the protons involved in the collision
must be sufficiently high. This energy goes into the production of the
difference of the total intrinsic energies between the born and annihilated
particles\sidenote{The intrinsic energy of a particle is the energy associated with its rest mass,
that is, the energy $mc^{2}$. For more details see Chapter 13.}. This difference is $\Delta mc^{2}$, where $\Delta m$ is the difference of the total rest masses of the particles after and before the process. By increasing the
kinetic energy of the protons, it is possible to observe the processes in which the number of particles born increases. In principle you can conjure up a really fantastic picture: two protons collide with enormous
energy to produce a galaxy!

Suppose we wish to ``split'' protons by bombarding them with photons
gradually increasing the energy of the latter. Instead of the splitting of
protons we would observe various interconversions, for example the
following:
\begin{align*}%
\gamma + p & \to p + \pi^{0}\\
\gamma + p & \to n +\pi^{+} \\
\gamma + p & \to p + \pi^{+} +\pi^{-} \\
\gamma + p & \to p + p + \bar{p}
\end{align*}

This example demonstrates that interconversions make futile any
attempts to split some particles by bombarding them with others. In
actual fact, we observe \emph{not the splitting of the particles bombarded but the
birth of new particles}. New particles are born at the expense of the energy
of the colliding particles.

Interactions of particles are studied in a chamber in which charged
particles leave a distinct track. Widely used are chambers filled with
liquid hydrogen in superheated state. A charged particle passing through
the chamber causes the hydrogen to boil leaving a clearly visible track of
small bubbles. Such chambers came to be known as \emph{bubble chambers}. If
the chamber is placed in a fairly strong magnetic field, the tracks of
particles will be curved, and particles with opposite signs will curve into
opposite directions.

\begin{figure}[!h]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-12/cloud-chamber.jpg}
\caption{(a) A photograph of a bubble chamber showing tracks of elementary particles. (b) Schematic diagram showing the tracks and identification of particles.}
\label{bubble-chamber}
\end{figure}

Shown in Figure~\ref{bubble-chamber}~(a) is a fascinating photograph of tracks taken in 1959
in the new liquid-hydrogen bubble chamber (72 inches). The chamber
was bombarded by an antiproton beam. The picture shows a rare
event-an antiproton on colliding with a proton produces a lambda
hyperon and an antilambda hyperon
\begin{equation*}%
\bar{p} + p \to \Lambda^{0} + \bar{\Lambda}^{0}
\end{equation*}
The deciphering of this photograph is given in Figure~\ref{bubble-chamber}~(b). The
antiproton collided with the proton at point $A$. Two electrically neutral
(therefore invisible in the picture) particles-$\Lambda^{0}$ and $\bar{\Lambda}^{0}$ were born. At
point $?$ the antilambda hyperon disintegrated to yield an antiproton and
a positively charged pion:
\begin{equation*}%
\Lambda^{0} \to p + \pi^{+}
\end{equation*}
Notice that the tracks of these particles ($p$ and $\pi^{+}$) diverge, which is
due to the fact that their charges have opposite signs (the chamber had
been placed into a magnetic field perpendicular to the plane of the 
photograph). At point $C$ the antiproton collided with the proton, the
annihilation following the conventional scheme:
\begin{equation*}%
\bar{p} + p \to \pi^{+} + \pi^{+} + \pi^{-} + \pi^{-}
\end{equation*}
The tracks of the pions and the antipions diverge in the photograph. At
point $D$ the lambda hyperon decayed:
\begin{equation*}%
\Lambda^{0} \to p + \pi^{-}
\end{equation*}
Note that the events recorded in the photograph include \emph{four}
generations of particles. The first generation is represented by the initial
antiproton; the second one, by the hyperon $\Lambda^{0}$ and the antihyperon $\bar{\Lambda}^{0}$.
The third generation particles are the products of disintegration of
second-generation particles. Lastly, the fourth generation includes the
particles born as a result of the annihilation of the proton and the
secondary antiproton.

\emph{Interconversions of elementary particles enable us to gain a better
understanding of the properties of the particles themselves}. It is specifically
these studies that made it possible to establish the existence of two types
of neutrino (electron and muon neutrinos). Also, these studies made 
significant contributions to our understanding of conservation laws and
invariance principles. All of these issues will be considered in later
sections.

\begin{figure}[!h]
\centering
\includegraphics[width=0.85\textwidth]{figs/ch-12/particles3.jpg}
\caption{Principal decay schemes for unstable elementary particles.}
\label{table-1}
\end{figure}

In conclusion, we will provide a table of elementary particles (Figure~\ref{table-1}).
Note that the table contains two types of kaons $K^{0}$, which have 
significantly different lifetimes: \emph{short-lived} kaons $K_{s}$ and \emph{long-lived} kaons
$K_{1}$.
%
%\begin{figure*}
%\centering
%\includegraphics[width=0.9\textwidth]{figs/ch-04/regular-polyhedra.png} 
%\caption{The five possible types of regular polyhedra. \label{regular-poly}} 
%\end{figure*}
%
%\begin{marginfigure}
% \centering
%  \includegraphics[width=0.9\textwidth]{figs/ch-04/dodecahedron-symm.png}
%  \caption{The symmetry of dodecahedron.}
%  \label{dodeca-symm}
%\end{marginfigure}
%
%
%\begin{figure}
%\centering
%\includegraphics[width=0.75\textwidth]{figs/ch-04/kepler.png}
%\caption{Scheme of the Solar System based on the regular polyhedra devised by Kepler.}
%\label{kepler-scheme}
%\end{figure}
%
%
%
%                        