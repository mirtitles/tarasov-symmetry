% !TEX root = tarasov-symmetry.tex






\chapter{Order in the World of Atoms}


\section{Molecules}
Chapter 5 was concerned\marginnote[-2cm]{ The crystal is characterized by its internal structure, arrangement of atoms, and not by its outward appearance. These atoms combine to produce a sort of huge molecules, or rather an ordered space lattice.
- H. Lindner} with symmetry in nature as seen by the unaided eye. Symmetry is also found at the \emph{atomic level}. It manifests itself in microscopic, geometrically ordered atomic structures of molecules and crystals.

\begin{marginfigure}
 \centering
  \includegraphics[width=0.8\textwidth]{figs/ch-06/water-co2.jpg}
  \caption{The planes of symmetry of water (\ce{H2O})and carbon dioxide (\ce{CO2}) molecules.}
  \label{water-co2}
\end{marginfigure}


\hyperref[water-co2]{Figure~\ref{water-co2}} presents schematically two simple molecules: (a) \emph{carbon dioxide} (\ce{CO2}), and (b) \emph{steam} (\ce{H2O}). Both molecules have a plane of symmetry (the vertical line in the figure). The mirror symmetry here comes from the fact that paired identical atoms (oxygen atoms in \ce{CO2} or hydrogen atoms in \ce{H2O}) are bound to the third atom in a similar way. Interchanging the paired atoms will change nothing - this will only amount to mirror reflections.

\begin{marginfigure}
 \centering
  \includegraphics[width=0.8\textwidth]{figs/ch-06/methane.jpg}
  \caption{The plane of symmetry of the methane molecule (\ce{CH4}).}
  \label{methane-symm}
\end{marginfigure}

In the \emph{methane} molecule (\ce{CH4}), the carbon atom C is bound to the four identical hydrogen atoms H. The four \ce{C-H} bonds being identical predetermines the spatial structure of the molecule in the shape of a \emph{tetrahedron}, with hydrogen atoms being at the corners and a carbon atom at the centre (\hyperref[methane-symm]{Figure~\ref{methane-symm}}). The symmetry of the molecule \ce{CH4} is essentially the symmetry of the tetrahedron discussed in Chapter 4. Its elements are six planes of symmetry, each of which passes through the atom C and two atoms H (for example, planes $LMQ$ and $LKR$ in \hyperref[methane-symm-02]{Figure~\ref{methane-symm-02}}), four three-fold rotation axes, each of which passes through the atom C and one of the atoms H (for example, axis $KO$ in the figure). There are three two-fold rotation axes (for example, axes $PQ$ and $SR$). 

Notice the difference between the \emph{spatial structure} of the methane molecule given in \hyperref[methane-symm]{Figure~\ref{methane-symm}} and the \emph{structural formula} of the molecule normally given in chemistry texts. 
\begin{center}
\setatomsep{20pt}
\chemfig{C(-[:0]H)(-[:90]H)(-[:180]H)(-[:270]H)}
\end{center}

\begin{marginfigure}
 \centering
  \includegraphics[width=0.8\textwidth]{figs/ch-06/methane-symm.jpg}
  \caption{The plane of symmetry of the methane molecule (\ce{CH4}).}
  \label{methane-symm-02}
\end{marginfigure}

Now suppose that one of the hydrogen atoms in the molecule (for example, the atom at $K$ in the tetrahedron shown in \hyperref[methane-symm-02]{Figure~\ref{methane-symm-02}}) is replaced by a radical OH. In that case we obtain the molecule of methyl alcohol (\ce{CH3OH}). As compared with the methane molecule, this molecule exhibits lower symmetry (even supposing that the molecule retains its tetrahedral shape). It is easily seen that out of the six planes of symmetry only three remain - those passing through C, OH, H (planes $LKR$, $KPN$, $KMT$ in \hyperref[tetra-symm-02]{Figure~\ref{tetra-symm-02}}); out of the four three-fold rotation axes, only one remains (KO in the figure). There are no two-fold rotation axes now.

\begin{marginfigure}
 \centering
  \includegraphics[width=0.8\textwidth]{figs/ch-06/tetra-02.jpg}
  \caption{The plane of symmetry of the methane molecule (\ce{CH4}).}
  \label{tetra-symm-02}
\end{marginfigure}

Notice that the chemical formula of methyl alcohol is now written \ce{CH3OH}, not \ce{CH4O}. This is no mere chance. The form \ce{CH4O} would mean that all four H atoms in the molecule are physically equivalent, which is not the case here: only three H atoms are equivalent, whereas the fourth stands alone, as it enters the OH radical.

\section{The Puzzle of the Benzene Ring}
\begin{marginfigure}
 \centering
 \setatomsep{20pt}
\chemfig{C*6((-H)=C(-H)-C(-H)=C(-H)-C(-H)=C(-H)-)}
%  \includegraphics[width=0.8\textwidth]{figs/ch-06/tetra-02.jpg}
\vspace{5pt}
  \caption{The structural symmetry of the benzene molecule (\ce{C6H6}).}
  \label{benzene-01}
\end{marginfigure}
The benzene molecule consists of six carbon and six hydrogen atoms (\ce{C6H6}). The carbon atoms are arranged in one plane to form a regular hexagon (the so-called benzene ring). It is well known that carbon is tetravalent, that is, a carbon atom provides four electrons which can realize four covalent bonds with other atoms. One of them is the bond between a carbon atom and a hydrogen atom, the other three binding a given atom to neighbouring carbon atoms in the benzene ring. The structural formula of the benzene molecule is sometimes presented as shown in  \hyperref[benzene-01]{Figure~\ref{benzene-01}}, where some pairs of carbon atoms are bound by single and others by double bonds.

At first sight everything about the structural formula given in \hyperref[benzene-01]{Figure~\ref{benzene-01}} is OK. But this is only true at first sight. The fact is that the presence of different bonds (single and double) must violate the regular shape of the benzene ring, since stronger (double) bonds correspond to smaller atomic spacings. At the same time, $X$-ray studies show that all the sides of the carbon hexagon in the benzene molecule are equal. The experimentally found symmetry of the benzene ring (the symmetry of the regular hexagon) suggests that all the \ce{C-C} bonds in the ring are equal.

What is the nature of these bonds.? These cannot be single covalent bonds, otherwise one bond in each carbon atom would be free. But they cannot be double bonds either, since for this to be the case each carbon atom lacks one valence.
\begin{marginfigure}
 \centering
 \setatomsep{20pt}
\schemestart
\chemfig{C*6((-H)-[:00,,,,lddbond]C(-H)-[:60,,,,lddbond]C(-H)-[:120,,,,lddbond]C(-H)-[:210,,,,lddbond]C(-H)-[:270,,,,lddbond]C(-H)-[:330,,,,lddbond])}
\schemestop
\vspace{5pt}
  \caption{The schematic representation of the collectivization of six electrons by the benzene ring (\ce{C6H6}).}
  \label{benzene-02}
\end{marginfigure}
The enigma of the benzene ring turned out to be exceedingly interesting. One of the valence electrons of each carbon atom participates in the formation of a bond of this atom with five atoms of the ring at once, and not with one of the neighbouring atoms. This implies that the electron is collectivized not by a pair of atoms (which is common for covalent bonds), but by the \emph{entire molecule} (or rather the entire benzene ring). In other words, the benzene molecule has six electrons not pinned up by localized bonds between atoms, but capable of freely moving around the entire benzene ring. This is generally represented by the structural formula shown in \hyperref[benzene-02]{Figure~\ref{benzene-02}}, where the solid lines denote, as usual, the localized bonds (each bond due to the collectivization of a pair of electrons by a pair of appropriate atoms), and dash lines denote non-localized bonds due to the collectivization of six electrons by the benzene ring.



\section{The Crystal Lattice}

It would seem that \emph{diamond} and \emph{graphite} have nothing in common. The diamond is unusually hard, transparent, and is a dielectric. Processed stones are used in jewelry. Graphite, on the other hand, is soft, laminar, opaque, electro-conductive. In a word, a far cry from a gem. At the same time, however, both diamond and graphite are \emph{carbon} in its pure form. The different behaviour of diamond and graphite is only explained by their different crystalline structure, or \emph{different crystalline lattices}. This is a graphic example of the important role played by the crystalline lattice in determining the properties of a solid.

\emph{The crystal lattice is a natural three-dimensional pattern}. As with plane patterns, it is dominated by some form of translational symmetry. It has been noted in Chapter 2 that there exist 14 types of spatial lattices that differ in their translational symmetry (14 types of \emph{Bravais lattices}). They form seven \emph{crystallographic systems}:

\begin{marginfigure}[2cm]
\includegraphics[width=0.9\textwidth]{figs/ch-02/cube2.jpg}
\captionsetup{labelformat=empty}
\caption{\textsf{\smaller Vectors defining a three-dimensional unit lattice.}}
\label{cube-3}
\end{marginfigure}
\begin{enumerate}[label=\textsection,leftmargin=1cm]
\item \emph{cubic} system: $a=b=c, \quad \alpha = \beta = \gamma = \ang{90}$;
\item \emph{tetragonal} system: $a=b \neq c, \quad \alpha = \beta = \gamma = \ang{90}$;
\item \emph{hexagonal} system: $a=b \neq c, \quad \alpha = \beta =\ang{90}, \,\,  \gamma = \ang{120}$;
\item \emph{trigonal} system: $a=b=c, \quad \alpha = \beta = \gamma \neq \ang{90}$;
\item \emph{rhombic} system: $a \neq b \neq c, \quad \alpha = \beta = \gamma = \ang{90}$;
\item \emph{monocline} system: $a \neq b \neq c, \quad \gamma \neq \alpha = \beta = \ang{90}$;
\item \emph{tricline} system: $a \neq b \neq c, \quad \alpha \neq \beta \neq \gamma $;
\end{enumerate}
Here $a$, $b$, and $c$ are lengths of the edges of a unit cell; and ($\alpha, \, \beta$    and $\gamma$ are angles between the edges (see the margin figure).



\section{The Face-Centred Cubic Lattice}

Suppose that we have many balls of the same diameter. We will pack them densely on a plane (the brown balls in \hyperref[fcc-0]{Figure~\ref{fcc-0}}). Over the \emph{first} layer a \emph{second} one will be placed (the red balls). It is easily seen that the second layer is packed as densely as the first one. Next we will lay a \emph{third} layer. Here we have two versions: 
\begin{enumerate*}[label=(\alph*)]
\item the centres of the balls in the third layer come exactly over the centres of the balls in the first layer; 
\item the centres of the balls of the third layer are displaced horizontally relative to the balls in the first layer.
\end{enumerate*}
Let us take the second version (the blue balls). The resultant dense multilayer packing corresponds to the \emph{face-centred cubic} lattice (f.c.c.). In other words, the centres of the balls here (second version) form an f.c. c. lattice.

\begin{figure}[!ht]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-06/fcc-0.jpg}
\caption{Arranging balls to get a dense packing.}
\label{fcc-0}
\end{figure}

\hyperref[fcc-1]{Figure~\ref{fcc-1}} shows the \emph{cubic} unit cell of the f.c.c. lattice. The sites here are the vertices of the cube and the centres of all its faces. We can readily discern in the figure the planes of the angles corresponding to the ball layers in \hyperref[fcc-0]{Figure~\ref{fcc-0}}. Let plane $DEF$ correspond to the first layer (the brown balls). Then plane $ABC$ will correspond to the second layer (the red balls). Site $K$ will now belong to the third layer.

\begin{figure}[!ht]
\centering
\includegraphics[width=0.6\textwidth]{figs/ch-06/fcc-cubic-unit.jpg}
\caption{Cubic unit of the f.c.c. lattice.}
\label{fcc-1}
\end{figure}

Each cell thus includes four sites (for example, sites $D, \, P, \, M,$ and $S$ in \hyperref[fcc-1]{Figure~\ref{fcc-1}}); the remaining sites in the figure must be assigned to neighbouring cells, and so the cell in question is a \emph{four-site} cell.
\begin{marginfigure}%[-1cm]
\includegraphics[width=0.9\textwidth]{figs/ch-06/fcc-rhombo.jpg}
\caption{Face centred cubic cell showing a one-cell rhombohedron-shaped cell.}
\label{fcc-2}
\end{marginfigure}

The f.c.c. cell, just like any Bravais lattice, can also be defined using a \emph{one-site} unit cell. Shown in \hyperref[fcc-2]{Figure~\ref{fcc-2}} (in red) is a one-cell rhombohedron-shaped cell. Crystallographers prefer using not one- but four-site cells, since it reflects in the most complete manner the elements of symmetry possessed by the f.c.c. cell.



Face-centred cubic lattices occur fairly often. This sort of lattice is to be found, for example, in \emph{aluminium, gold, copper, nickel, platinum, silver,} and \emph{lead}. The lattice of common salt (\ce{NaCl}) actually consists of two geometrically identical interlocked f.c.c. lattices, one made up of \ce{Na+} ions and the other of \ce{CI-} ions (\hyperref[fcc-3]{Figure~\ref{fcc-3}}).

\begin{marginfigure}%[2cm]
\includegraphics[width=0.9\textwidth]{figs/ch-06/fcc-nacl.jpg}
\caption{Face centred cubic cell in common salt \ce{NaCl}.}
\label{fcc-3}
\end{marginfigure}


\section{Polymorphism}

It has earlier been noted that the difference in the properties of diamond and graphite is determined by the difference in the crystal lattices of these two forms of carbon. As is seen in \hyperref[diamond-01]{Figure~\ref{diamond-01}}, the \emph{diamond lattice} is formed by two identical interlocked f.c.c. lattices, one of which is displaced relative to the other by a quarter of the edge of the f.c.c. cell along all three coordinate axes (the white circles in the figure show sites of one of the f.c.c. lattices, and the filled circle shows the site of the other f.c.c. cell). Each carbon atom in the diamond lattice is the centre of a \emph{tetrahedron}, whose vertices are the four nearest neighbours of a given atom; this is seen especially clearly in the cell shown in red in the figure.

\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\textwidth]{figs/ch-06/diamond-01.jpg}
\caption{Structure of the diamond lattice.}
\label{diamond-01}
\end{figure}

Note that the crystal lattices of \emph{germanium, silicon} and \emph{grey tin} have the diamond lattices.
\begin{marginfigure}%[2cm]
\includegraphics[width=0.9\textwidth]{figs/ch-06/graphite.jpg}
\caption{Structure of the graphite lattice.}
\label{graphite-01}
\end{marginfigure}

The \emph{graphite lattice} is given in \hyperref[graphite-01]{Figure~\ref{graphite-01}}. It is distinctly laminar in structure, with each layer being characterized by six-fold rotational symmetry. The bonds between the atoms from different layers are much weaker than within the same layer.


Diamond and graphite are good examples of two different crystalline modifications of a chemical element (or compound). This phenomenon is known as \emph{polymorphism}. Under certain conditions a substance may change from one crystal modification to the other, and the changes are called \emph{polymorphic transformations}. If, for instance, we heat graphite up to \SIrange{2000}{2500}{\kelvin} under a pressure of up to \SI{e10}{\pascal}, the crystal lattice will transform with the result that graphite will turn into diamond. In this way artificial diamonds are produced.

\section{The Crystal Lattice and the External Appearance of a Crystal}
The symmetry of the external shape of a crystal is conditioned by the symmetry of the crystal lattice. Ideally plane crystal faces are the planes that pass through the sites of the lattice. True, through lattice sites one can draw many different sets of parallel planes (\hyperref[diff-01]{Figure~\ref{diff-01}}). 

\begin{figure}[!ht]
\centering
\includegraphics[width=\textwidth]{figs/ch-06/diff-01.jpg}
\caption{Parallel planes in crystal lattice.}
\label{diff-01}
\end{figure}

These sets differ in their orientation in space, inter-planar spacing and density of packing of sites in the plane. Of especial interest are the most dense planes (in the figure they are shown in red). It is along these planes that a single crystal specimen normally fractures, and it is to these planes that the faces of a grown single crystal correspond. In general the faces of a unit cell are not parallel to those planes. One should not therefore expect that the shape of a single crystal specimen will coincide with the shape of a unit cell (compare \hyperref[fcc-3]{Figure~\ref{fcc-3}} and \ref{crystal-symm-02} showing diamond).



\section{The Experimental Study of Crystal Structures}

Crystal structure cannot be seen even through the most powerful microscope now available. The atomic structure of a crystal is identified using the \emph{diffraction of X-rays}. For the latter a crystal is a \emph{diffraction grating} produced by nature.

Let us consider the simplest $X$-ray technique. A single crystal sample is oriented in a particular way relative to an $X$-ray beam. On reflecting from various sets of the parallel planes passing through the sites of the lattice, $X$-rays produce (on a photographic film) a picture characteristic for the given orientation of the single crystal, the so-called \emph{Laue pattern} of the single crystal (from the name of the German physicist Laue). Each spot in the pattern corresponds to one of the reflected $X$-ray beams. Reflections will only be observed in directions that meet the known \emph{diffraction condition}:
\begin{equation*}%
2d \sin \theta= n \, \lambda
\end{equation*}
where $d$ is the separation between neighbouring parallel reflecting planes, $\theta$ is the angle between the direction of the reflected $X$-ray beam and the reflecting plane (equal to a half of the angle between the directions of the reflected and initial beams), $\lambda$ is the wavelength of the $X$-rays, and $n = 1, \, 2, \dots$

\begin{figure}[!ht]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-06/diff-02.jpg}
\caption{Structure of the diamond lattice.}
\label{diff-02}
\end{figure}

\hyperref[diff-02]{Figure~\ref{diff-02}} is a schematic representation of the reflection of X-rays from three sets of parallel planes passing through sites, supposing of course that the above condition is met.

\begin{marginfigure}[-1cm]
\includegraphics[width=\textwidth]{figs/ch-06/diff-03.jpg}
\caption{ Laue pattern of \emph{zinc blende} (\ce{ZnS}) single crystal for two orientations of the sample relative to the initial beam.}
\label{diff-03}
\end{marginfigure}

\hyperref[diff-03]{Figure~\ref{diff-03}} is an example of the Laue pattern of \emph{zinc blende} (\ce{ZnS}) single crystal for two orientations of the sample relative to the initial beam. One of the pictures shows a four-fold rotational symmetry and the other a three-fold one. The arrangement of spots on the pictures is a tale-tell indication of the elements of symmetry of the lattice studied.

In addition to the diffraction of X-rays, some crystal studies are made using the diffraction of \emph{electrons} and very slow \emph{neutrons}.

\section{The Mysteries of Water}

It is common knowledge that heating reduces the density of liquids and subjecting them, more viscous. \emph{Water} behaves differently, however. Heating from \SIrange{0}{4}{\celsius} increases the density of water, and pressurizing it reduces its viscosity.

The mystery was unravelled after the atomic structure of water was investigated. It turned out that water molecules interact with one another in a \emph{directed way} (just like the carbon and hydrogen atoms in the methane molecule). Each water molecule may thus form bonds with four neighbouring molecules so that their centres will form a \emph{tetrahedron}. This is shown schematically in \hyperref[tetra-01]{Figure~\ref{tetra-01}} where the balls stand for water molecules.

\begin{marginfigure}%[2cm]
\includegraphics[width=0.9\textwidth]{figs/ch-06/tetra-01.jpg}
\caption{Water molecules form tetrahedral structures for interaction.}
\label{tetra-01}
\end{marginfigure}

This arrangement corresponds to the fairly loose, \emph{skeleton-like} molecular structure, where each molecule has only four nearest neighbours. By way of comparison, for the dense packing of balls the number of the nearest neighbours is twelve.

Note that, unlike crystals, the molecular structure of water should be viewed as a manifestation of \emph{short-range} order. Near each molecule the neighbouring molecules are arranged in an orderly manner, the order gradually diminishing with the distance from a given molecule.

The skeleton-like molecular structure of water provides a good explanation for its physical properties. Water increases in density when heated from \SIrange{0}{4}{\celsius} because the heating \emph{disturbs the molecular bonds} causing the arrangement of molecules to pack more densely. Further heating reduces the density because a conflicting effect sets in: the thermal expansion of spacing between oxygen and hydrogen atoms in the water molecule. This accounts for the well-known fact that water has the \emph{highest} density at \SI{4}{\celsius}.

The skeleton-like molecular structure of water (near \SI{0}{\celsius}) also explains another property of water - the drop in its viscosity with increasing external pressure. Pressurizing, just like heating, disrupts molecular bonds and thus reduces viscosity.


\section{Magnetic Structures}

The orbiting of electrons in the field of the atomic nucleus may produce an atomic magnetic field. \emph{Magnetic materials} may have their atomic magnetic fields ordered. So, in a \emph{ferromagnetic} magnetized to saturation, the magnetic fields of all atoms are oriented in one direction (that of the magnetizing field), whereby the magnetic properties of the substance become especially apparent.

Also of interest is the magnetic order in a special type of magnetic material-the so-called \emph{anti-ferromagnetics}, which are currently widely used in logical elements and memories of modern computers. The direction of the atomic magnetic field in those materials \emph{alternates in a regular fashion} from one atom to the next, with the result that apart from a crystal lattice a \emph{magnetic lattice} is present as well. For simplicity, \hyperref[magnet-01]{Figure~\ref{magnet-01}~a} shows a plane square lattice, the dash lines delineating a unit cell. \hyperref[magnet-01]{Figure~\ref{magnet-01}~b} shows the same lattice for the antiferromagnetic. The directions of the atomic magnetic field are shown by arrows at sites, the dash lines delineating a magnetic unit cell. It is easily seen that the linear size of the unit cell is twice the size of the crystal cell.

\begin{marginfigure}[-.5cm]
\includegraphics[width=0.9\textwidth]{figs/ch-06/magnetic.jpg}
\caption{Magnetic and anti-ferromagnetic lattices.}
\label{magnet-01}
\end{marginfigure}

For a real (three-dimensional) example of an anti-ferromagnetic, let us take \emph{manganese oxide} (\ce{MnO}), its crystal lattice is given in \hyperref[magnet-02]{Figure~\ref{magnet-02}}. It consists of two identical interlocked f.c.c. lattices, one of which contains manganese ions \ce{Mn^{2+}} and the other oxygen ions \ce{O^{2-}}. Oxygen ions have no magnetic field. The magnetic fields of manganese ions in the planes are represented in the figure by the same colour (for example, red), they are oriented in the same way, whereas the magnetic fields of manganese ions belonging to the planes of different colour are oriented in opposite directions (the directions of magnetic fields are perpendicular to the planes chosen).

\begin{figure}[!ht]
\centering
\includegraphics[width=0.7\textwidth]{figs/ch-06/anti-ferromagnetic.jpg}
\caption{Structure of an anti-ferromagnet \emph{manganese oxide} (\ce{MnO}) crystal lattice.}
\label{magnet-02}
\end{figure}

\section{Order and Disorder}

In the surrounding world \emph{order and disorder are inseparable}. Whatever the degree of order of a given atomic structure, it still has some elements of disorder. Specifically, this is true of the atomic structure of crystals.

It is to be noted first of all that atoms are by no means fixed at lattice sites, but are instead involved in \emph{thermal vibrations} near those sites, the amplitude being the larger the higher the temperature. Thermal vibrations make individual atoms leave their sites and wander (diffuse) about the crystal. In the lattice some unoccupied sites (so-called \emph{vacancies}) appear. Both vacancies and atoms at gaps between sites (interstices) will obviously distort the geometry of the lattice by influencing the arrangement of neighbouring atoms (\hyperref[defect-01]{Figure~\ref{defect-01}}). What is more, any real crystal may have some foreign atoms, the so-called \emph{impurities}. These atoms may be at interstices, but may also substitute for lattice atoms by ``driving them away'' from their places.

\begin{figure*}
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-06/lattice-defect-01.jpg} 
\caption{Vacancies and interstices in a crystal lattice.} 
\label{defect-01}
\end{figure*}


Substantial disorder in the lattice is caused by so-called \emph{dislocations} - violations of the regular arrangement of atomic planes. Some idea of dislocations may be obtained in \hyperref[defect-02]{Figure~\ref{defect-02}}, which represents the so-called \emph{edge} dislocation.

The presence of defects in a crystal lattice is the decisive factor influencing the \emph{strength} and \emph{plasticity} of a material. At present, special techniques enable filament crystals to be grown, such that their lattice is essentially defect-free. The strength of such crystals may be as high as \SI{e10}{\pascal}, hundreds of times larger than for conventional crystals. According to modem thinking, the plasticity of a material is controlled by the \emph{migration of defects}, above all dislocations, over the sample. Interestingly, if the density of the defects grows, they eventually begin to hinder the migration with the result that the plasticity of a material. drops. This is what occurs in certain kinds of processing (forging, annealing etc.)

\begin{figure}[!ht]
\centering
\includegraphics[width=\textwidth]{figs/ch-06/lattice-defect-02.jpg}
\caption{Dislocations in a crystal lattice.}
\label{defect-02}
\end{figure}

Consequently, a material can be strengthened by two \emph{opposite} ways-either by preventing defect formation, or by hindering the migration of defects about the sample (that is, by increasing the density of defects). The first technique means growing defect-free crystals, and the second one a special processing of materials.
               

%
%\begin{figure*}
%\centering
%\includegraphics[width=0.9\textwidth]{figs/ch-04/regular-polyhedra.png} 
%\caption{The five possible types of regular polyhedra. \label{regular-poly}} 
%\end{figure*}
%
%\begin{marginfigure}
% \centering
%  \includegraphics[width=0.9\textwidth]{figs/ch-04/dodecahedron-symm.png}
%  \caption{The symmetry of dodecahedron.}
%  \label{dodeca-symm}
%\end{marginfigure}
%
%
%\begin{figure}
%\centering
%\includegraphics[width=0.75\textwidth]{figs/ch-04/kepler.png}
%\caption{Scheme of the Solar System based on the regular polyhedra devised by Kepler.}
%\label{kepler-scheme}
%\end{figure}
%
%
%
%                        