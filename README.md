The project has the LaTeX source files for the book This Amazingly Symmetrical
World by Lev Tarasov. We have used the scans of the book for images and have
not recreated any of the images. 

Though I would have liked to create all the pages, especially the cover and the
art till the title page using TiKZ, for now I have created pdfs for those pages
in Inkscape. Purists may not like it, but at least in this version, that is 
how things are.

The typsetting of the book lets itself to be very well typeset with Edward
Tufte's style implemented in LaTeX. 

If there are any errors while typesetting, for example references, or typos 
please report them so that they can be addressed. 

As usual, you will need all the package files to compile the book correctly.

