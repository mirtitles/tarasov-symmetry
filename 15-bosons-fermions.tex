% !TEX root = tarasov-symmetry.tex






\chapter{Fermions and Bosons}

\marginnote{All the particles in nature are either fermions or bosons. Thus, there
occur only  antisymmetric or only symmetric states of the same  particles. \newline - P. Dirac}

\section{The Periodic Table and the Pauli Principle}

The advances of atomic physics made it possible to provide
a substantiation for the Periodic System. According to modern thinking,
as the number of chemical elements increases, the electron shells are 
gradually filled up. The first to be filled up are shells that have the strongest
binds with the atomic nucleus, that is, the closest to it. The first (the
closest to the nucleus) shell takes only two electrons to be filled, whereas
the second and third take eight electrons, and the fourth and fifth,
eighteen, and so on. We thus obtain the sequence: $2, \, 8, \, 8, \, 18, \, 18, \ldots$ The
number of the chemical elements in the first five periods of the Periodic
Table are exactly like these.

The first shell may contain only two electrons, the second not more
than eight Why? The answer to this question is two-fold. First, to each
shell there corresponds a \emph{definite} number of possible states of the
electron-two for the first shell, eight for the second shell, and so on.
Second, \emph{in each state there may be only one electron}. This means that in
the atom you cannot find two electrons with the same characteristics that
define its state, such as energy, orbital angular momentum, its projection,
spin projection. And so at least one of these characteristics must be
different

This rule that forbids two or more electrons from occupying the same
state is known as the \emph{Pauli exclusion principle}. In the simplest terms, the
principle can be understood as a rule that more than two electrons
cannot reside in the place, and these electrons must have opposite spins
(or must be in different spin states). Like any prohibition principle, the
Pauli exclusion principle expresses a certain \emph{symmetry of natural laws}.
This is the so-called \emph{commutative symmetry}.

\section{Commutative Symmetry. Fermions and Bosons}

This is the symmetry \emph{with respect to commutation of any two particles of
the same type}, specifically as applied to electrons. Physically, nothing
changes if an electron in state 1 is placed in state 2, and the electron in 2 is
placed in 1. This symmetry implies that all the electrons in the Universe
are \emph{identical}. Also identical are all the protons, all the neutrons, all the
hydrogen atoms, all the oxygen atoms, and so on.

Our planet consists of about \num{d50} atoms. And this prodigious number
of atoms consists of only several dozen varieties. Furthermore, we are
confident that the entire Galaxy, the entire Metagalaxy, and the entire
Universe are constructed of several hundred various building blocks. All
the chemical elements of the Universe can be arranged as a table that has
about a hundred cells.

This, however, does not exhaust the profound meaning of commutative
symmetry. \emph{This symmetry dictates that all the particles in nature are split
into two categories that behave differently in an ensemble}. One category
obeys the rule that particles of the same type, for example electrons, must
avoid one another. According to these rules, identical particles may only
be alone in a state. All the particles of this category are known
collectively as \emph{fermions} (from the name of the Italian physicist Fermi).
The other category is governed by precisely opposite rules, which not
only allow but even dictate that identical particles concentrate densely in
states. These particles are known as \emph{bosons} (from the name of the Indian
physicist Bose).

There is a connection between the spin s of a particle and its behaviour
in an ensemble. All the particles with \emph{half-integral} spin ($s = 1/2, 3/2, \ldots$)
are fermions, whereas all the particles \emph{without a spin} or \emph{integer} spin are
bosons. Apart from electrons, fermions include other leptons, and
barions. All of them obey the exclusion principle formulated above for
electrons: \emph{if a state is occupied by a fermion, then no other fermion of this
type may be in this state}. Other bosons are photons and mesons. In any
given state there may be any number of the same bosons. Moreover, \emph{the
more densely is a given state populated the higher the probability that other
bosons of this type will come to it}.

We have thus, on the one hand, clearly expressed individualism
(leptons and barions), and, on the other hand, as clearly expressed
collectivism (photons and mesons). In this connection it is worth noting
that there is a marked difference between leptons and baryons, for one
thing, and photons and mesons for the other. This is primarily explained
by the fact that for the former there \emph{exist} conservation laws according to
which the difference between particles and antiparticles remains 
unchanged (conservation laws for electron, muon, and baryon numbers),
whereas for the latter \emph{there are no} such laws.

Taking the Periodic Table as an example, we can clearly see just how
fundamental is the fact that electrons are fermions. It is exactly the
fermion nature of electrons that accounts for the peculiarities of the
population of the atomic levels by electrons. Should the Pauli exclusion
principle suddenly fail to apply to electrons, then in that case and also in
all atoms all the electrons would occupy the level with the lowest energy.
And this would destroy the diversity of elements.
Note also that it is the fermion nature of electrons that does not allow
atomic nuclei in a solid to approach one another too closely. At close
distances the electron shells would overlap, in other words, many
electrons would be in one place. But this is forbidden by the Pauli
exclusion principle. As a result, the atoms remain separated by fairly
decent distances from one another (about \SI{d-10}{\metre} or more), which is no
less than \num{d3} times larger than the size of atomic nuclei.

\section{Symmetrical and Antisymmetrical Wave Functions}

It is to be stressed that a fermion does not admit other sister fermions to
the state it occupies, just like a boson attracts other bosons. The fact \emph{is in
no way connected with any special forces} (repulsion or attraction) which
act between the particles. The fermion or boson nature of particles is their
fundamental property associated not with force interactions but with the
\emph{symmetry under commutations of particles}. Therefore, it would be
instructive to explain even briefly how commutative symmetry leads to
the presence of fermions and bosons in nature.
	
Note that in quantum mechanics the state of a microobject is described
using some function called the \emph{wave function}. It is significant that
a physical meaning is attached not to the wave function but to its squared
modulus, which describes the \emph{probability of finding the microobject in
a given state}.

Let $\psi_{1} (I)$ be the wave function of particle $I$ in state 1, and $\psi_{2} (II)$ is the
wave function of particle $II$ in state 2. Consider the microobject as
a system of particles $I$ and $II$. The wave function of the microobject $\Psi (I, \, II)$ can be expressed as the product of the wave functions of the
constituent particles. Since the particles are assumed to be identical, it is
then unknown which of them is really in state 1 and which in state 2. We
will then have to take into account both $\psi_{1} (I) \, \psi_{2} (II)$ and $\psi_{2} (I) \, \psi_{1} (II)$ (as
if the particles in the microobject were continually exchanging their
places). \emph{Commutative symmetry} requires that the wave function $\Psi (I,\, II)$ of
the object meet the condition
\begin{equation*}%
|\Psi (I,\, II)|^{2} = |\Psi (II, \,I)|^{2}
\end{equation*}
Out of the combinations of the above products of single-particle wave
functions we can construct two functions that meet this condition
\begin{equation*}%
\Psi_{S} (I, \, II) = \psi_{1} (I) \, \psi_{2} (II) + \psi_{2} (I) \, \psi_{1} (II)
\end{equation*}
and
\begin{equation*}%
\Psi_{A} (I, \, II) = \psi_{1} (I) \, \psi_{2} (II) - \psi_{2} (I) \, \psi_{1} (II)
\end{equation*}
The first of these is \emph{symmetrical}, it \emph{does not change} its sign under
commutation: $\Psi_{S} (I, \, II)  = \Psi_{S} (II, \, I)$  This function describes a system of
\emph{bosons}. The second function is \emph{antisymmetric}, it \emph{changes} its sign under
commutation of particles: $\Psi_{A} (I, \, II)  = - \Psi_{A} (II, \, I)$. This function
describes a system of \emph{fermions}. This can be readily verified. If we assume
that both particles are in the same state, for example state 1, then it
follows from the expression for  $\Psi_{A}$ that this function vanishes. Hence this
situation is impossible.

\section{The Superfluidity of Liquid Helium. Superconductivity}

At extremely low temperatures (under 2.19 K), \ce{He^{4}} forms a liquid that
has a highly interesting property: its motion along a narrow capillary is
characterized by the \emph{total absence of viscosity}.

Liquid helium flows undergoing no resistance from the walls. This
phenomenon is called \emph{superfluidity}. This remarkable phenomenon comes
from \ce{He^{4}} atoms being bosons. Note that a system consisting of an even
number of fermions behaves as a boson. A commutation of two such
systems amounts to a commutation of an even number of fermion pairs.
A commutation of each pair of fermions changes the sign of the total
wave function. If the sign changes an even number of times this means
that it remains the same.

At very low temperatures, when the effect of the thermal motion of
atoms, which scatters them over different states, becomes negligible, the
rule manifests itself in full measure, which states that bosons must
concentrate in one state. As a result, all the \ce{He^{4}} atoms concentrate in
a state characterized by a definite momentum, and so they move along
the capillary as a single whole. In that case, the liquid displays no
viscosity: the presence of viscosity requires that different regions of the
liquid travel with different velocities.

Unlike \ce{He^{4}}, the atoms of \ce{He^{3}} are fermions. No wonder then, that when
cooled down to \SI{2}{\kelvin}, helium containing the isotope \ce{He^{3}} does not become
superfluid. But the superfluid \ce{He^{3}} still exists. It was obtained in 1974 at
stronger cooling-down to  \SI{0.0027}{\kelvin}. At such extremely low
temperatures, a highly curious effect occurs in helium: \ce{He^{3}} atoms are
paired. Each pair is clearly a boson. As a result we observe superfluidity

Note one more curious phenomenon - the \emph{superconductivity of metals}
It is well-known that at temperatures near absolute zero many metals
begin to conduct electric current essentially without resistance. So, lead
goes over to a superconductive state at \SI{7.26}{\kelvin}, tin at \SI{3.69}{\kelvin}, aluminium
at \SI{1.14}{\kelvin}, zinc at \SI{0.79}{\kelvin}. The phenomenon of superconductivity can be
viewed as the phenomenon of the superfluidity of the electron fluid
formed in a metal by conduction electrons. The fact is that at low
temperatures electrons combine to form pairs that behave like bosons.
This is due to the interaction of electrons with crystal ions.
It is easily seen that superconductivity and superfluidity are essentially
the same in nature. They are conditioned by the fact that \ce{He^{4}} atoms,
pairs of \ce{He^{3}} atoms, and electron pairs are bosons.

\section{Induced Light Generation and Lasers}
In recent years great strides have been made in \emph{quantum electronics}, a new
area that came into life when an amazing light generator, the laser, was
invented in 1960. The principle of the laser is the \emph{induced emission} of light
by matter.

The gist of this phenomenon is as follows. Suppose that in a substance
we have excited atoms, each of which, when jumping back to the initial
(unexcited) state can emit a photon with a definite energy. Under normal
conditions atoms accomplish such transitions in an \emph{uncorrelated} manner,
at different times, the photons emitted being sent out in \emph{different}
directions. This is called the \emph{spontaneous} emission of light. One can,
however, control the process to make the excited atoms return to the
initial state at the \emph{same time}, having emitted photons in a \emph{definite}
direction. This is the \emph{induced} emission of light.

The phenomenon of induced emission is directly related to the boson
nature of photons. A photon that flies past the excited atoms, when its
energy is equal to that of the transition in the atoms, will \emph{initiate} massive
production of new photons in the \emph{same} state in which it is itself. This
phenomenon is used in the laser.

%
%\begin{figure*}[!h]
%\centering
%\includegraphics[width=0.9\textwidth]{figs/ch-04/regular-polyhedra.png} 
%\caption{The five possible types of regular polyhedra. \label{regular-poly}} 
%\end{figure*}
%
%\begin{marginfigure}
%\centering
%\includegraphics[width=0.9\textwidth]{figs/ch-04/dodecahedron-symm.png}
%\caption{The symmetry of dodecahedron.}
%\label{dodeca-symm}
%\end{marginfigure}
%
%
%\begin{figure}[!h]
%\centering
%\includegraphics[width=0.75\textwidth]{figs/ch-04/kepler.png}
%\caption{Scheme of the Solar System based on the regular polyhedra devised by Kepler.}
%\label{kepler-scheme}
%\end{figure}
%
%
%
%                        