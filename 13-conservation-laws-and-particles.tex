% !TEX root = tarasov-symmetry.tex






\chapter{Conservation Laws and Particles}

\marginnote{The strong hint  emerging from recent studies of elementary particles is that the only inhibition imposed upon the chaotic flux of events in the world of the very small is that imposed by the conservation laws. Everything that can happen without violating a conservation law does happen. - K. Ford}

\section{Conservation of Energy and Momentum in Particle Reactions}

In relativity theory the energy $E$ of a particle having a rest mass $m$ and
momentum $p$ is given by
\begin{equation}%
E =\sqrt{(mc^{2})^{2} + (pc)^{2}}
\label{energy-momentum}
%eq-13.1 (1)
\end{equation}
The dependence $E(p)$ is plotted in Figure~108. At zero momentum ($p = 0$)
the expression \eqref{energy-momentum} yields the well-known \emph{Einstein's formula}
\begin{equation*}%
E = mc^{2}
\end{equation*}
\begin{marginfigure}
 \centering
 \includegraphics[width=0.9\textwidth]{figs/ch-13/energy-mom.jpg}
 \caption{Energy momentum relationship for elementary particles.}
 \label{energy-mom}
 \end{marginfigure}
The energy $mc^{2}$ is the intrinsic energy of a particle (the energy possessed
by the particle in the frame associated with the particle). Suppose that the
particle's momentum is not zero but small as compared with $mc$. In that
case, the right-hand side of \eqref{energy-momentum} becomes
\begin{equation*}%
mc^{2} \sqrt{1 + \left( \dfrac{pc}{mc^{2}} \right)^{2}} \approx mc^{2} \left[ 1 + \dfrac{1}{2}  \left( \dfrac{pc}{mc^{2}}\right)^{2} \right]
\end{equation*}
Hence
\begin{equation*}%
E \approx mc^{2} + \dfrac{p^{2}}{2m}
\end{equation*}
The first term describes the intrinsic energy of the particle, the second one
is the well-known expression for kinetic energy. At sufficiently large
momenta ($p \gg mc$), the expression \eqref{energy-momentum} becomes
\begin{equation*}%
E \approx pc
\end{equation*}
For particles with zero rest mass the relationship $E = pc$ clearly holds
at any momentum (see the dash line in Figure~\ref{energy-mom}).

Suppose that a particle with rest mass $m_{1}$ decays into a particle with
mass $m_{2}$ and a particle with mass $m_{3}$, the energy and momentum of these
latter being $E_{2}, \, p_{2}$ and $E_{3}, \, p_{3}$, respectively. In the frame of reference
associated with the initial particle the \emph{laws of conservation of energy and
momentum} have the forms:
for energy
\begin{equation*}%
m_{1}c^{2} = \sqrt{(m_{2}c^{2})^{2} + (p_{2}c)^{2}} + \sqrt{(m_{3}c^{2})^{2} + (p_{3}c)^{2}}
\end{equation*}
for momentum
\begin{equation*}%
0 = \vec{p}_{2} + \vec{p}_{3}
\end{equation*}
It follows that for the decay to take place the following inequality must
hold:
\begin{equation*}%
m_{1} > (m_{2} + m_{3})
\end{equation*}

Or, in other words, \emph{in a decay the total mass of the products must be
smaller than the rest mass of the initial particle}.

Significantly, in a decay the magnitude of the momentum of a decaying
particle is immaterial. No matter how we accelerate, say, a charged pion,
all the same it can only decay into particles such that their total rest mass
is smaller than the rest mass of the pion. With collisions the situation is
different. By increasing the momentum of the particles involved in
a collision, processes can be realized in which particles will be born with
the total rest mass larger than that of the colliding particles

Consider the process production of an electron-positron pair by
collision of two photons
\begin{equation*}%
\gamma + \gamma \to e^{-} + e^{+}
\end{equation*}
The initial particles here have no rest mass at all, nevertheless their
collision produces two particles with rest mass.

Here is an example of \emph{radiation-to-matter conversion}.

Let $\vec{p}_{1}$ and $\vec{p}_{2}$ be the momenta of the photons, and $\vec{p}_{3}$ and $\vec{p}_{4}$ the
momenta of the electron and the positron. The laws of conservation will
then read:
\begin{equation*}%
p_{1}c + p_{2}c = \sqrt{(mc^{2})^{2} + (p_{3}c)^{2}} + \sqrt{(mc^{2})^{2} + (p_{4}c)^{2}}
\end{equation*}
for energy, and
\begin{equation*}%
\vec{p}_{1} + \vec{p}_{2} = \vec{p}_{3} + \vec{p}_{4}
\end{equation*}
for momentum.

It is easily seen that the total energy of the photons must be larger than
$2mc^{2}$. If the photons with equal momenta collide head on, then $\vec{p}_{3} + \vec{p}_{4} = 0$. This means that an electron-positron pair may have essentially zero momenta, and so the required total energy of the photons appears to be minimal and equal to $2mc^{2}$.

\section{The Conservation of Electric Charge and Stability of the Electron}

In all the processes occurring in the world of elementary particles, the \emph{law of conservation of electric charge holds: the total electric charge of the primary particles is exactly equal to the total electric charge of the
secondary particles.}

The form of symmetry underlying this conservation law is more subtle than those concerned with spatial and temporal translations, that is, the laws of conservation of energy, momentum, and angular momentum But
to formulate this principle we would have to resort to quantum mechanics. We will, therefore, confine ourselves to the remark that underlying the law of conservation of electric charge is the \emph{symmetry of physical laws with respect to changes in the magnitude of the intensity of electric field}. The familiar statement that the magnitude of a potential has no physical meaning (and it is only the potential \emph{difference} that  matters), thus amounts to the statement that electric charge is conserved.

The stability of the electron is one of the most important consequences
of the law of conservation of electric charge. Since the electron is
a particle with \emph{smallest} nonzero rest mass, its decay can only give rise to
zero rest mass particles (recall that this conclusion follows from the laws
of conservation of energy and momentum). But all the zero-mass
particles are electrically neutral. Accordingly, the decay of an electron is
forbidden by the law of conservation of electric charge.

\section{The Three Conservation Laws and Neutrino}

The existence of the neutrino had been predicted long before it was found experimentally, and the prediction was based on \emph{energy conservation}.

It was found that the energy of an electron produced in the beta decay
of a nucleus turns out to be different in different decay events and at all
times it is smaller than half the total energy released in the process.
Experimenters placed a beta-active sample within a heat-insulating lead
chamber whose walls did not let in a single electron. Accurate
measurements have shown that the chamber heats up to a lesser degree
than might be expected from the heat budget. It was suggested that in the
beta fissure of nuclei the conservation of energy is invalid. The prominent
Swiss physicist Wolfgang Pauli (1900-1958) came up with another
explanation of the enigma of beta decay. Assuming that the \emph{law of
conservation of energy is also valid in the microworld}, in 1930 he came to
the conclusion that in beta decay in addition to the electron some neutral
particle is produced, which could not be recorded by the experimental
set-up. It was this particle that carries away the energy that is obtained if
from the energy liberated in the process we subtract the energy carried
away by the electron. The famous Italian physicist Enriko Fermi
(1901-1954), the author of the theory of beta decay, christened this neutral
particle ``neutrino'', which is the Italian for ``small neutron''. So in the list
of subatomic particles, appeared another entry, the neutrino. For a long
time this particle had actually been a phantom, a particle only deduced
from symmetry.

Neutrino was found experimentally in 1956. Frankly, by that time
nobody questioned the very fact of the existence of the ``elusive'' neutrino.
This was because the neutrino hypothesis was based not only on the law
of conservation of \emph{energy} but also on the laws of conservation of
\emph{momentum} and \emph{angular momentum}. Let us take a simple example, the beta
decay of a neutron; the neutron decays following the equation mentioned
above\sidenote{Note that the decay scheme includes not a neutrino but an antineutrino
(electron). This was revealed later.}
\begin{equation*}%
n \to p + e^{-} + \bar{\nu}_{e}
\end{equation*}
We will consider this decay in the frame of reference associated with
the neutron. For sufficiently slow neutrons, this frame is essentially the
lab frame. If this decay did not produce an antineutrino, then from
momentum conservation, the proton and the electron would have to move
off in opposite directions, as is shown in Figure~\ref{decay-mom}~(a). It was found,
however, that the proton and the electron scatter in a different manner
( Figure~\ref{decay-mom}~(b)). This suggests that another particle is produced here whose
momentum determined the observed picture (see  Figure~\ref{decay-mom}~(c)). The law of
conservation of momentum dictates that the vector sum of the. three
momenta (the proton, electron, and third particle, that is, the
antineutrino) is zero in the frame of reference associated with the 
neutron.
\begin{figure}[!h]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-13/decay-momentum-conser.jpg}
\caption{Neutrino creation during neutron decay.}
\label{decay-mom}
\end{figure}
The angular momentum must also be conserved here. Before the decay
the angular momentum was determined by the neutron's spin $s = 1/2$. If
the decay products only consisted of a proton ($s = 1/2$) and an electron 
($s = 1/2$), the law of conservation of angular momentum would not hold
in this case. In fact, the proton and the electron may have either parallel
or antiparallel spins, and so the total spin may be either $1$ or $0$, and by no
means $1/2$. For the angular momentum to be conserved, another particle
is necessary, such that its spin is $1/2$. This explains Figure~\ref{angular-mom-cons}, where the
momentum vectors are shown in blue and the angular momentum
vectors are shown in red (notice that the antineutrino is a right-handed
helix).
\begin{figure}[!h]
\centering
\includegraphics[width=0.85\textwidth]{figs/ch-13/angular-mom-cons.jpg}
\caption{Angular momentum conservation during neutrino creation.}
\label{angular-mom-cons}
\end{figure}

\section{Experimental Determination of Electron Antineutrino}

In 1956 the American physicists Cohen and Reines obtained direct
experimental proof of the existence of the antineutrino (and hence
neutrino). They used the process
\begin{equation*}%
\bar{\nu}_{e} + p \to n + e^{+}
\end{equation*}
This sort of a process is highly unlikely. It is only known that the
neutrino and the antineutrino interact with matter extremely
weakly-they, essentially unhindered, pierce all barriers, the entire globe
and even the Sun. To record such a particle it is necessary to use
a sufficiently dense beam and advanced experimental techniques.
\begin{marginfigure}
 \centering
 \includegraphics[width=0.75\textwidth]{figs/ch-13/neutrino-cons.jpg}
\caption{Discovering the neutrinos experimentally.}
\label{neutrino-cons}
\end{marginfigure}
Cohen and Reines used an antineutrino beam from a high-capacity
nuclear reactor. Into this beam they placed a special-purpose detector
consisting of several layers of water separated by a scintillator capable of
detecting individual photons (Figure~\ref{neutrino-cons}~(a)). There is a tiny probability for
an antineutrino to interact with a proton in the water to cause the above
process that yields a neutron and a positron. It should be stressed that
these events are, if any, exceedingly rare. It is for this reason, that the
scintillator was utilized. A produced positron is stopped and undergoes
an annihilation with an atomic electron
\begin{equation*}%
 e^{+} + e^{-} \to \gamma + \gamma 
\end{equation*}
As a result, two photons are created that fly off in opposite directions
so that they can be recorded simultaneously in two adjacent scintillator
layers (Figure~\ref{neutrino-cons}~(b)). As for the neutron, it diffuses in a layer of water for
a relatively long time (about \SI{d-6}{\second}), until it is caught by a cadmium
nucleus (some cadmium is added to the water). After having absorbed the
neutron, the cadmium nucleus emits a photon or photons, which are
caught by a scintillator layer (Figure~\ref{neutrino-cons}~(b)). Consequently, the scintillator
must respond to an antineutrino-proton collision with three pulses: first
a pair of simultaneous pulses are recorded using adjacent scintillators,
and then, in about \SI{d-6}{\second}, another pulse. Both of the first two pulses
correspond to a \SI{0.5}{\mega\electronvolt} photon, and the third pulse to a \SI{10}{\mega\electronvolt}
photon. In Cohen and Reines's experiment this specific picture of pulses
was actually observed (approximately three times per hour). Thereby the
existence of the antineutrino was proved.

At the time of the experiment just described, nobody suspected that
there are two forms of neutrino (antineutrino), the electron and the muon
neutrino. It is only natural that nobody knew that it was the electron
neutrino that was found. The second type of the neutrino (muon) was
discovered in 1962 by a group of experimentalists at Columbia
University, USA. After it had been established that there exist two types
of neutrino, two independent specific conservation laws were 
formulated - the \emph{conservation of electron} and \emph{muon numbers}.

\section{Electron and Muon Numbers. Electron and Muon Neutrinos}

The \emph{electron} and \emph{muon numbers} are specific charges of elementary
particles. They have nothing to do with the electric charge. The \emph{electron number} of the electron and the electron neutrino is assumed to be $1$, and that of their antiparticles, $-1$. With all the other particles and
antiparticles the electron number is zero. The \emph{muon number} of the muon and the muon neutrino is $1$, that of their antiparticles, $-1$. With all other particles (antiparticles) the muon number is zero.

\emph{In each process the total electron number of the reactants must be conserved}. It follows, for example, that the creation of an electron must be accompanied with the creation of either an electron antineutrino or
a positron, or with the annihilation of an electron neutrino:
\begin{align*}%
\bar{n} & \to p + e^{-} + \bar{\nu}_{e}\\
 \gamma + \gamma & \to e^{-} + e^{+} \\
  n + \nu_{e} & \to p + e^{~}
\end{align*}
Apart from the electron number, in each process the total muon number
must also be conserved. So the creation of a muon must be accompanied
by the creation of a muon antineutrino or the annihilation of a muon
neutrino:
\begin{align*}%
\pi^{-} & \to \mu^{-} + \bar{\nu}_{\mu} \\
n + \bar{\nu}_{\mu} & \to p + \mu^{-}
\end{align*}
The conservation of electron and muon numbers in the decay of a muon dictates that in addition to an electron, an electron antineutrino and a muon neutrino must be produced:
\begin{equation*}%
\mu^{-} \to e^{-} + \bar{\nu}_{e} + \nu_{\mu}
\end{equation*}
Specifically, the discovery of conservation of electron and muon
numbers is associated with the solution of one problem that for a long
time was unanswered-the so-called $\mu - e - \gamma$-problem. It was noted long
ago that nobody ever observed the muon reaction $\mu^{-} \to e^{-} + \gamma$. The
answer came from the laws of conservation of electron and muon
numbers: in this scheme \emph{neither the electron nor the muon number is
conserved}. This reaction thus appears to be forbidden twice (by two
conservation laws).

The existence of \emph{two independent conservation laws} (for electron and
muon numbers) is closely related to the existence of \emph{two different
neutrinos (antineutrinos)}. The electron neutrino (antineutrino) takes part
in processes where an electron or positron is created or annihilated,
whereas a muon neutrino (antineutrino) takes part in other processes,
ones in which a muon or an antimuon is created or annihilated.\sidenote{If the existence of the third type of neutrino (tauon) will be established experimentally, we will have to introduce another conserved number, the tauon number.}

That the electron and the muon neutrinos are two absolutely different
particles was established in 1962 in the aforementioned experiment at
Columbia University. A powerful beam of high-energy protons was
directed from an accelerator at a target to produce a beam of pions $\pi^{+}$
and antipions $\pi^{-}$. Pions and antipions are known to decay in 99 per cent
of all cases into antimuons $\mu^{+}$ and muons $\mu^{-}$, and hence muon
neutrinos and antineutrinos:
\begin{align*}%
\pi^{+} & \to \mu^{+} + \nu_{\mu} \\
\pi^{+} & \to \mu^{-} + \bar{\nu}_{\mu}
\end{align*}
With the help of a 10-m thick iron wall, all the particles were trapped
except for the above-mentioned muon neutrinos and antineutrinos. Into
a flux of these neutrinos and antineutrinos a hydrogen-containing target
was placed to study the products of processes occurring in rare collisions
of a muon antineutrino with a proton. If the muon antineutrino were
identical with the electron one, the following processes might be observed
with equal probability:
\begin{align*}%
\bar{\nu}_{\mu} + p & \to n + \mu^{+} \\
\bar{\nu}_{e} + p & \to n + e^{+}
\end{align*}
This was not the case, however. During 300 hours the experimenters
recorded 30 tracks of antimuons $\mu^{+}$ and found no sign of positrons.
Thereby the existence of two different types of neutrino and antineutrino
was clearly established. 

\section{The Baryon Number and Stability of the Proton}

We will now turn to the various processes involving baryons but not
antibaryons. It was found that in these processes the number of baryons
is always unchanged. For example, in the process
\begin{equation*}%
n \to p + e^{-} + \bar{\nu}_{e}
\end{equation*}
the baryon $n$ decays just to give birth to another baryon $p$. In the process
\begin{equation*}%
p + p \to n + \Lambda^{0} + K^{+} \pi^{+}
\end{equation*}
two baryons $p$ annihilate and two baryons ($n$ and $\Lambda^{0}$) are produced.
Consequently, the annihilation of some baryons is compensated for by
the production of others, the total number of baryons remaining the
same.

Let us assign a specific number to each baryon, which is assumed to be
unity. We will call it the \emph{baryon number}. Photons, both neutrinos, an
electron, muon and mesons have no such number (or it is zero). The fact
that the number of baryons in various processes remains unchanged can
be viewed clearly as the \emph{law of conservation of baryon number}.

Further, we will remember that there are antibaryons. If the baryon
number of the baryons is unity, then for antibaryons it must be set at $? 1$
(recall that the antiparticles have the opposite signs of all the numbers).
From baryon number conservation, processes must occur with pair
creation or annihilation of an antibaryon and baryon. Such processes are
actually observed. For example,
\begin{align*}%
p + \bar{p} & \to \pi^{+} + \pi^{-} + \pi^{0} \\
p + \bar{p} & \to \Lambda^{0} + \bar{Lambda}^{0} \\
p + p & \to p + p + p + \bar{p}
\end{align*}
The \emph{law of conservation of baryon number} is at present considered to be
well established. According to that law, \emph{in any process the difference
between the number of baryons and the number of antibaryons remains
unchanged}. Accordingly, for the entire Universe too, the difference
between the total number of baryons and the total number of
antibaryons is unchanged It is worth noting here that by \emph{the law of
conservation of electron number the difference between the number of
particles of an electron family and the number of their antiparticles remains
unchanged}. In consequence, the \emph{law of conservation of muon number leaves
unchanged the difference between the number of particles of the muon family
and the number of their antiparticles}. Curiously, there is no similar law for
photons or mesons.

If the stability of the electron stems from the law of conservation of
electric charge, then the \emph{stability of the proton follows from the law of
conservation of baryon number}. Among the baryons, the proton has the
smallest mass, therefore there cannot be any baryons among its decay
product If this were not the case, the decay of a proton would lead to an
uncompensated annihilation of a baryon. But such a process is
prohibited by the law of conservation of baryon number.

In the rather large family of baryons only the proton is a stable
particle. All the remaining baryons (neutron and hyperons) are unstable;
each of them disintegrates to produce a lighter baryon. As regards the
proton, according to the law of conservation of baryon number, it simply
cannot decay into anything. The mesons, muon and electron, which lie
along the mass scale, have a zero baryon number.

The world around us (and hence we as well) could not exist if protons
and electrons were not stable. This goes to prove the exclusive role of
conservation laws, specifically the law of conservation of baryon number
and electric charge.

\section{Discrete Symmetries. $CPT$-Invariance}

We have considered seven conservation laws: for energy, momentum,
angular momentum, electric charge and three numbers (electron, muon,
and baryon). The first four are related to the well-known properties of the
symmetry of physical laws. One can expect that the remaining
conservation laws (for electron, muon, and baryon numbers) express
some symmetries, but we do not yet know which symmetries.

The above conservation laws are said to be \emph{absolute} laws: they hold
true at all times, in all the transformations of elementary particles.\sidenote{Later in the book we will get acquainted with the laws that are not absolute. They hold in some transformations and do not in others.} To
the seven absolute conservation laws we must add the eighth one- the
\emph{law of conservation of CPT-symmetry}.

\emph{CPT-symmetry} is a conservation law for the combination of three
sufficiently clear symmetries: the \emph{symmetry with respect to the replacement
of all particles by their respective antiparticles} (the so-called 
\emph{charge-conjugation symmetry} or $C$-invariance), \emph{mirror symmetry} (generally called
$P$-invariance), \emph{symmetry in time}, that is, \emph{symmetry with respect to time
reversal} (the so-called $T$-invariance).

$CPT$-symmetry means that if all particles were \emph{simultaneously} replaced
by appropriate antiparticles and mirror-reversed, and then the sense of
time were reversed, the laws of physics would remain unchanged and all
the physical processes would proceed as before. This statement is usually
referred to as the \emph{CPT-theorem}. The $CPT$-theorem is so firmly
entrenched in the foundations of physics that, if it were to turn out not to
be true, physical theory would be in shambles. ``All hell will break loose'',
was how Abraham Pais once expressed it.

Turning to the symmetries underlying this conservation law, we note
first of all that the symmetries we dealt with earlier in the book are
associated with the conservation of energy, momentum, angular
momentum, and electric charge, and they are \emph{continuous} symmetries. In
all of them a change that leaves the laws of physics unchanged can be
made \emph{arbitrarily} small; this change can be brought about smoothly, 
gradually, in other words, continuously. Apart from continuous symmetries
there exist other symmetries that are considered in relation to changes
that inherently cannot be continuous. They are associated with jumps, or
are said to be \emph{discrete}. All three symmetries involved in the CPT-theorem
are \emph{discrete}. It is clear that an object cannot be partially reflected in
a mirror; the reflection is either possible or not. Likewise, one cannot
replace a proton by an antiproton partially, the replacement either occurs
or does not. The same is true of the reversal of the sense of time.

\emph{C-symmetry} was covered in Chapter 12, \emph{P-symmetry} in Chapter 9. We
only have to discuss \emph{T-symmetry}.

Everyday experiment indicates that time flows in one direction only. It
might appear that in the world around us there is no invariance under
time reversal. If we run a motion-picture film backward, events on it will
look grotesque: people will walk backwards, the fragments of a broken
vase will come together to form a whole vase, a swimmer will not dive
into the water but, on the contrary, he will be expelled from the water feet
first, and so forth.

An examination of transformations of subatomic particles shows,
however, that \emph{both senses of time are physically equivalent}. So, besides the
process $e^{-}+ e^{+} \to \gamma + \gamma$ the reversed process is possible: $ \gamma + \gamma \to e^{+} + e^{-}$. And apart from the process $p + p \to p + n + \pi^{+}$ the reversed process
$p + n + \pi^{+} \to p + p$ is possible. True, ``possible'' by no means implies
``equiprobable''. In the last example, the reversed process is less likely
than the direct one. This stems from the small probability of the meeting
of three particles at once.

It is the low probability of reversed processes that accounts for the
apparent noninvariance of physical laws under mental time reversal. The
laws of physics as such are symmetrical with respect to the future and the
past. But for any specific chain of events, a certain sequence, as a rule,
turns out to be more \emph{likely} than the opposite order.

Returning to \emph{CPT-symmetry}, we will note that this \emph{means
CPT-invariance}, that is, invariance under three simultaneous operations:
replacement of particles by antiparticles, mirror reflection and time
reversal. In this connection, recall that energy can be defined as
a quantity whose conservation is a consequence of invariance under
a shift in time, momentum is a quantity conserved as a consequence of
invariance under spatial translations, angular momentum is a quantity
conserved as a consequence of invariance under spatial rotation (see
Chapter 11). \emph{CPT-symmetry is the product of three qtumtities: 
charge-conjugation parity (C-parity), space parity (P-parity)},\sidenote{Space parity is often referred to as just parity.} and \emph{time parity
(T-parity)}. Each of these is a conserved quantity that corresponds to some
discrete symmetry, namely charge conjugation, mirror reflection, and
time reversal.

It is only natural that the issue of \emph{conservation laws for charge-conjugation parity, space parity, and time parity presents itself}. It has been found recently that, unlike $CPT$-symmetry, these conservation laws are
not absolute. This interesting issue will be the subject of the following chapter.
%
%\begin{figure*}
%\centering
%\includegraphics[width=0.9\textwidth]{figs/ch-04/regular-polyhedra.png} 
%\caption{The five possible types of regular polyhedra. \label{regular-poly}} 
%\end{figure*}
%
%\begin{marginfigure}
% \centering
%  \includegraphics[width=0.9\textwidth]{figs/ch-04/dodecahedron-symm.png}
%  \caption{The symmetry of dodecahedron.}
%  \label{dodeca-symm}
%\end{marginfigure}
%
%
%\begin{figure}
%\centering
%\includegraphics[width=0.75\textwidth]{figs/ch-04/kepler.png}
%\caption{Scheme of the Solar System based on the regular polyhedra devised by Kepler.}
%\label{kepler-scheme}
%\end{figure}
%
%
%
%                        