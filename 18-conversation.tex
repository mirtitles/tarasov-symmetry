% !TEX root = tarasov-symmetry.tex






\chapter*{A Conversation Between the Author and the Reader About the Role of Symmetry}
\label{conversation}
\addcontentsline{toc}{chapter}{A Conversation Between the Author and the Reader About the Role of Symmetry}

\marginnote{In the 20th century the principle of symmetry encompasses an ever-increasing number of  domains. From  crystallography, solid-state  physics, it expanded into the science of chemistry, molecular processes and atomic physics. It is beyond doubt that we will find its  manifestations in the world of the electron, and quantum phenomena will obey it as well. \newline - V. Vernadsky}

\marginnote{ \ldots The thing that makes the world go round \newline - A. Pushkin}


\section*{The Ubiquitous Symmetry}
\addcontentsline{toc}{section}{The Ubiquitous Symmetry}

\begin{dialogue}
\speak{\txtauth} In 1927 the prominent Soviet scientist V. Vernadsky wrote: ``A
new element in science is not the revelation of the principle of symmetry,
but the revelation of its universal nature.'' I think that this book provides
plenty of evidence to support the notion of the \emph{universality of symmetry}.
\speak{\txtread}  The universality of symmetry is startling. Symmetry
establishes the internal relations between objects or phenomena which
are not outwordly related in any way. The game of billiards and the
stability of the electron, the decay of the neutron and the reflection in
a mirror, a pattern and the structure of diamond, a snowflake and
a flower, a helix and the DNA molecule, the superconductor and the
laser \ldots
\speak{\txtauth} The ubiquitous nature of symmetry is not only in the fact that it
can be found in a wide variety of objects and phenomena. The very
principle of symmetry is also very general, without which in effect we
cannot handle a single fundamental problem, be it the problem of life or
the problem of contacts with extraterrestrial civilizations. Symmetry
underlies the \emph{theory of relativity, quantum mechanics, solid-state physics,
atomic and nuclear physics, particle physics}. These principles have
their most remarkable manifestations in the properties of the \emph{invariance
of the laws of nature}.
\speak{\txtread}  It is quite obvious that here are involved not only the laws of
physics, but also others, for example, the laws of biology.
\speak{\txtauth}  Exactly. An example of a biological conservation law is the \emph{law
of heredity}. It relies on the invariance of the biological properties when
passing from one generation to the next. It is quite likely that without
conservation laws (physical, biological and others) our world simply
\emph{could not exist}.
\speak{\txtread}  Without energy conservation the world would be just a 
holocaust of random explosions associated with random appearances of
energy from nothing.
\speak{\txtauth}  Imagine that on a nice day the laws of conservation of the
electric charge and baryon number just cease to function. What would
result then?
\speak{\txtread}  Electrons and protons would then become unstable particles.
And there would not be a single stable particle with a nonzero rest mass.
Author You can easily imagine what sort of world we would live in. It
would be a giant cluster of photons and neutrinos. Here and there some ephemeric formations would emerge only to decay quickly (in \SIrange{d-10}{d-8}{\second}) into some kind of photon-neutrino chaos
And now imagine that suddenly the character of the symmetry of the
wave electron function changes so that electrons become bosons.
\speak{\txtread}  Maybe our world would then become a world of
superconductivity? Electric current would pass along wires in this kind of
world without resistance.
\speak{\txtauth}  Serious doubts emerge here in relation to wires themselves.
Having stopped to obey Pauli's exclusion principle, electrons in all atoms
would have to undergo a transition to the electron shell that is closest to
the nucleus. The entire Periodic System would be thrown into disarray.
\speak{\txtread}  Yes, indeed, symmetry permeates our world much deeper than
it is apparent to our eyes.
\speak{\txtauth}  It took several centuries to conceive this, and the 20th century
has been especially remarkable in this respect. As a result, the very
concept of symmetry has undergone a substantial overhaul.
\end{dialogue}

\section*{The Development of the Concept of Symmetry}
\addcontentsline{toc}{section}{The Development of the Concept of Symmetry}

\begin{dialogue}
\speak{\txtauth}  I hope that you understand how strongly today's picture of the
\emph{physically symmetrical world} differs from the \emph{geometrically symmetrical
cosmos} of the ancients. From antiquity to the present, the notion of
symmetry has undergone a lengthy development. From a purely
\emph{geometrical} concept it turned into a fundamental notion lying at the
foundation of \emph{nature's laws}. We now know that symmetry is not only that
which is visible to our eyes. Symmetry is not just \emph{around us} but, moreover,
it is \emph{at the root of everything}.
\speak{\txtread}  So it is not without good reason that the book is divided into
two parts: ``Symmetry Around Us'' and ``Symmetry at the Heart of
Everything'', isn't it?
\speak{\txtauth}  Yes, it is with good reason. The first part was devoted to
geometrical symmetry. The second part was meant to show that the
notion of symmetry is much deeper and that to grasp it one requires not
so much a visual conception as thinking. As we pass on \emph{from the first to
the second part we cover the path from the symmetry of geometrical
arrangements to the symmetry of physical phenomena.}
\speak{\txtread}  As far as I understand, from the most general point of view the
notion of symmetry is related to the \emph{invariance under some
transformations}. Invariance may be purely geometrical (the conservation
of geometrical shape), but may have nothing to do with geometry, for
example, the conservation of energy or biological properties. In exactly
the same way transformations may be geometrical in nature (rotations,
translations, commutations), and may be of another nature (the
replacement of particles by antiparticles, the transition from one
generation to the next).
\speak{\txtauth}  According to modern views, the concept of symmetry is
characterized by a certain structure in which three factors are combined:
\begin{enumerate*}[label=(\arabic*)]
\item an \emph{object (phenomenon)} whose symmetry is considered, 
\item \emph{transformations} under which the symmetry is considered, 
\item \emph{invariance (unchangeability, conservation)} of some properties of an object that
expresses the symmetry under consideration. 
\end{enumerate*} Invariance exists not in
general but only in as far as something is conserved.
\speak{\txtread}  I heard that there exists a specially worked out \emph{theory} of
symmetry with its own mathematical tools.
\speak{\txtauth}  Such a theory does exist. It is called the \emph{theory of groups of
transformations}, or for short, just \emph{groups theory}. The term ``groups'' was
introduced by the father of this theory, the outstanding French
mathematician Evariste Galois (1811-1832). These days any serious
studies in the field of quantum physics, solid-state physics, and particle
physics use the procedures of groups theory.

But back to the notion of symmetry. We will take a closer look at the
fundamental nature of symmetry.
\speak{\txtread}  This fundamentally is well grasped if one remembers that
\emph{symmetry limits the number of possible forms of natural structures, and also
the number of possible forms of behaviour of various systems.} This has been
repeatedly stressed in the book
\speak{\txtauth}  It can be said that there are three stages in our cognition of the
world. At the lowest stage are \emph{phenomena}; at the second, the \emph{laws of
nature}, and lastly at the third stage, \emph{symmetry principles. The laws of
nature govern phenomena, and the principles of symmetry govern the laws of
nature}. If the natural laws enable phenomena to be predicted, then the
symmetry principles enable the laws of nature to be predicted. In the final
analysis the predominance of symmetry principles can be judged from the
actual presence of symmetry in everything that surrounds us.
\speak{\txtread}  But perhaps there is some contradiction here with the fact that
the most interesting discoveries in particle physics are related to
\emph{violations} of conservation laws, that is \emph{violations of symmetry}.
\speak{\txtauth}  The violation of $P$-invariance in weak interactions is
compensated for by the conservation of $CP$-parity. We can speak about
the existence of a sort of \emph{law of compensation for symmetry}: once
symmetry is lowered at \emph{one} level it is conserved at another, \emph{larger} level.
But this issue is worth considering in more detail. It is directly related to
the problem of symmetry-asymmetry.
\end{dialogue}
\section*{Symmetry-Asymmetry}
\addcontentsline{toc}{section}{Symmetry-Asymmetry}


\begin{dialogue}

\speak{\txtauth}  Faith in the primordial symmetry (harmony) of nature has
always been an inspiration for scholars. And today it inspires them from
time to time to undertake a search for a unified theory and universal
equations.
\speak{\txtread}  This search is not altogether without success. Suffice it to
mention Einstein's theory of relativity or the discovery by Gell-Mann of
unitary symmetry in strong interactions.
\speak{\txtauth}  That is all very well. But it should be noted that discoveries of
new symmetries in the surrounding world by \emph{no means brings us any
nearer to the cherished unified theory}. The picture of the world, as we
progress along the path of cognizing it, \emph{becomes ever more complicated},
and the very possibility of the existence of such universal equations
becomes ever more doubtful. In the book \emph{In Search of Harmony} by
O. Moroz there is a rather graphic comment: ``Physicists are chasing
symmetry just as wanderers in the desert chase the elusive mirage. Now
on the horizon a beautiful vision appears, but when you try and approach
it, it disappears, leaving a feeling of frustration \ldots'' What is the matter
here?
\speak{\txtread}  Perhaps, it is that symmetry must be treated as no more than
\emph{ideal norm}, from which there are always \emph{deviations} in reality.
\speak{\txtauth}  Right. But the problem of symmetry-asymmetry must be
understood more deeply. Symmetry and asymmetry are so closely
interlinked that they must be viewed as \emph{two aspects of the same concept}.
Our world is not just a symmetrical world. It is a  symmetrical-asymmetrical world. The French poet Paul Valery (1871-1945) was right in saying: ``The world has orderly shapes strewn over it.'' He
went on to observe that ``events that are most astounding and most
asymmetrical in the short run acquire a measure of regularity in the
longer run.''
\speak{\txtread}  In the preliminary remarks it was stressed that the world exists
owing to the \emph{unity of symmetry and asymmetry}.
\speak{\txtauth}  The gist of the matter consists in that the unity of symmetry
and asymmetry is the \emph{unity of dialectically opposite categories}. It is
similar, for example, to the unity of essence and phenomenon, necessity
and chance, the possible and actual. The Soviet philosopher V. Gott in
his book \emph{Symmetry and Asymmetry} notes that 
\begin{quote}
symmetry discloses its content and meaning through asymmetry, which in itself is a result of changes, or violations, of symmetry. Symmetry and asymmetry is one of the manifestations of the general law of dialectics-the law of unity and conflict of opposites.
\end{quote}

Like two dialectically opposite categories, symmetry and asymmetry
\emph{cannot exist independently}. We have already said that in an \emph{absolutely}
symmetrical world you would observe nothing no objects, no phenomena. In exactly the same way, an \emph{absolutely} asymmetrical world is impossible too.
\speak{\txtread}  It seems that the more we grasp the symmetry of nature, the
more asymmetry comes out.
\speak{\txtauth}  Exactly. Therefore, any search for a unified theory or universal
equations is bound to fail, as is any attempt to consider symmetry
\emph{separately from asymmetry.}
\end{dialogue}

\section*{On the Role of Symmetry in the Scientific Quest for Knowledge}
\addcontentsline{toc}{section}{On the Role of Symmetry in the Scientific Quest for Knowledge}

\begin{dialogue}
\speak{\txtauth}  Symmetry principles are extremely important in the great
mystery called the \emph{scientific quest for knowledge}. Any \emph{scientific
classification} is based on revealing the symmetry of the objects being
classified. Objects or phenomena are grouped together by their common
features that are conserved under some transformations.

A good example is the Periodic System of elements suggested by the
great Russian chemist D. Mendeleev (1834-1907). From period to period
is preserved the community of properties of elements, that enter a column
of the table, for example lithium, sodium, potassium, rubidium, caesium.
The behaviour of elements varies in the same way within a period for
various periods.
\speak{\txtread}  It seems to me that any classification is based not only on
\emph{symmetry}, but also on the \emph{asymmetry} of properties.
\speak{\txtauth}  Right. It would make no sense to note the common properties
of lithium, sodium, potassium, if these properties were also possessed by
other elements within a period. The symmetry of properties of
appropriate elements from various periods is only of significance in
combination with the asymmetry of properties of elements within the
same period. \emph{Classification also presupposes both the conservation
(community) and change (differences) of properties of the objects being
chssified.}
\speak{\txtread}  Now I am beginning to understand the thesis about the dialectic
unity of symmetry and asymmetry. This is the \emph{unity of conservation and
change}, and the \emph{unity of community and difference}.
\speak{\txtauth}  Speaking about symmetry principles, we should always keep
this unity in mind.

Symmetry thus lies at the root of all classifications. Crystals, for
example, are classified by the type of symmetry of the crystalline lattice,
by the properties of atomic binding forces, by electric and other
properties. The classification of atoms is based on the community and
differences in the structure of their emission spectra.

When dealing with an unknown object or phenomenon, one should
above all identify the factors that \emph{are conserved under given
transformations}. Hermann Weyl noted that when one has to have
something to do with some object having a structure, one should try and
determine the transformations that leave the structural relations 
unchanged. You may hope that following this path you will be able to get
a deep insight into the inner structure of the object.

By applying symmetry to the development of scientific classifications
in structural studies, one can in the final analysis make \emph{scientific
predictions}. I think that some examples of such predictions are already
known to you.
\speak{\txtread}  For example, Mendeleev predicted a number of then-unknown
chemical elements and gave a correct description of their properties. It is
also worth noting Gell-Mann's prediction of the existence of omega
hyperon.
\speak{\txtauth}  No less instructive is the example of the prediction of the
\emph{displacement current}. The outstanding British physicist James Clerk
Maxwell (1831-1879) was keen enough to uncover in the phenomenon of
electromagnetic induction discovered by Faraday the production of the
alternating electric current by the alternating magnetic field. Having
assumed that there exists also a \emph{similar} inverse effect (the alternating
magnetic field is produced by the alternating electric field), Maxwell put
forward the famous hypothesis of the displacement current, which then
enabled him to formulate the laws of electromagnetism. Moroz writes in
his book \emph{In Search of Harmony}: 
\begin{quote}
When we try to solve the enigma of
what prompted Maxwell to that decisive step, what led him to the idea of
the displacement current, the circumstances lead us to quite a probable
answer-symmetry. The symmetry between electricity and magnetism.
The fact that Maxwell noticed it could be that insight without which no
great discovery can be made.
\end{quote}
\speak{\txtread}  Perhaps in the example of Maxwell one should speak not so
much about \emph{symmetry} as about \emph{analogy}?
\speak{\txtauth}  The \emph{method of analogy} is based on the principle of symmetry It
presupposes looking for common properties in different objects
(phenomena) and the extension of this community to include other
properties. Speaking about the role of symmetry in the process of
scientific quest, we should pay special attention to the application of the
method of analogy. In the words of the French mathematician D. Poia
\begin{quote}
there do not, perhaps, exist discoveries either in elementary or higher
mathematics, or even any other field, which could be made without
analogies.
\end{quote}
\speak{\txtread}  It seems to me that Poia has somewhat exaggerated the role of
analogies.
\speak{\txtauth}  No more than the role of symmetry. The universality of the
method of analogy, which has really been widely used in all the scientific
disciplines \emph{without exception}, is, in essence, the universality of the
principles of symmetry. \emph{Physical models} of objects (phenomena) are
constructed exactly by the use of analogies. The DNA molecule is
modeled as a screw. The spin of a particle is modeled as the angular
momentum of a body that spins like a top about its axis. The collision of
a photon with an electron in the Compton effect is modeled as the
collision of billiard balls.

Analogies between different processes allow us to describe them using
\emph{general equations}. A simple example: the swinging of the common 
pendulum, oscillations of atoms in a molecule, oscillations of the
electromagnetic field in an oscillatory circuit with a capacitance and an
inductance are symmetrical (analogous) in the sense that all these
processes are described by the same mathematical equation (the
differential equation of simple harmonic motions). One equation is
suitable for describing the process of radioactive decay, the process of
discharging a charged capacitor, the variation of air density with altitude
when there is no wind, the decrease in the intensity of a light beam
propagating through a medium (the differential equation of exponential
decay). The \emph{unity of the mathematical nature} of the processes at hand,
which makes it possible to regard them as analogous, points the presence
of \emph{deep symmetry}.
\speak{\txtread}  It is just amazing how wide in its scope our talk about the role
and place of symmetry appeared.
\speak{\txtauth}  It would have been much wider if we had included the
questions connected not only with the \emph{scientific} activities of man but also
with other aspects of his life, for example, \emph{engineering, architecture, arts}.
\speak{\txtread}  Symmetry is apparent in engineering and architecture. As
regards painting, music, poetry, here, it seems, the dominance of
symmetry is doubtful.
\end{dialogue}

\section*{Symmetry in Creative Arts}
\addcontentsline{toc}{section}{Symmetry in Creative Arts}

\begin{dialogue}

\speak{\txtauth}  Note, above all, that the creative endeavour of man in all its
manifestations \emph{gravitate toward symmetry}. In his book \emph{The Architecture
of the 20th Century} the French architect Le Corbusier wrote: 
\begin{quote}
Man needs order, without it all his actions lose their concordance, logical interplay.
The more perfect is the order, the more comfortable and confident is
man. He makes mental constructs on the basis of the order that is
dictated to him by the needs of his psychology-this is the creative
process. Creation is an act of ordering.
\end{quote}
\speak{\txtread} These are the words of an \emph{architect}. It is well known that the
principles of symmetry are the governing principles for any architect. In
some cases the architect can do with the primitive symmetry of the
rectangular parallelepiped, in other cases he uses a more refined
symmetry, as, for example, in the building of the Council of Mutual
Economic Cooperation in Moscow (Figure~\ref{symmetric-building}).
\begin{figure}[!h]
\centering
\includegraphics[width=0.75\textwidth]{figs/ch-18/symmetric-building.jpg}
\caption{Symmetry in the building of the Council of Mutual
Economic Cooperation in Moscow.}
\label{symmetric-building}
\end{figure}
\speak{\txtauth}  It would be better to speak not about ``primitive'' and ``refined''
symmetry but about how an architect solves the question of the
\emph{correlation between symmetry and asymmetry}. A structure that is
asymmetrical on the whole may represent a harmonic composition out of
symmetrical elements.
\begin{figure}[!h]
\centering
\includegraphics[width=0.75\textwidth]{figs/ch-18/moscow.jpg}
\caption{Cathedral of St. Basil the Blessed on Red Square in Moscow.}
\label{moscow}
\end{figure}
An example is the Cathedral of St. Basil the Blessed on Red Square in
Moscow (Figure~\ref{moscow}). One cannot help admiring this bizarre composition
of ten different parts, however the cathedral as a whole features neither
mirror nor rotational symmetry. The architectural volumes of the
cathedral, as it were, are superimposed on one another, intersecting,
rising, competing with one another and culminating in a central cone.
Everything is so full of harmony that it evokes the feeling of festivity. In
his book \emph{Moscow} M. Ilyin writes: 
\begin{quote}
At first glance at the cathedral one
can imagine that the number of architectural forms used in it is extremely
large. It soon becomes clear, however, that the builders made use of only
two architectural motifs the eight-sided cylinder and the semicircle. The
former defines the faceted forms of major volumes, whereas the second
is represented by a wide variety of forms, from wide and graceful arches
of the ground-floor to the peaked ogee gables.
\end{quote}
\speak{\txtread}  It turns out that the symmetry of the cathedral manifests itself
in the repetition (conservation) of the two main motifs throughout the
structure.
\speak{\txtauth}  Not just \emph{conservation}, but also \emph{variations}, or rather
\emph{development}, of them. The two main motifs do not simply recur in the
various parts of the cathedral, but they seem to be developing as the
glance of the observer traces the entire structure. This is a highly 
successful solution of the problem of symmetry-asymmetry. It is clear that
without this startling asymmetry the cathedral would have lost much of
its individuality.
\speak{\txtread}  Obviously, it is impossible to calculate preliminarily so 
successful a solution of the problem symmetry-asymmetry. This is a work of
genuine \emph{art}. It comes from the genius of the builder, his artistic taste, his
understanding of the beautiful.
\speak{\txtauth}  Quite so. It can be said that the art of architecture starts exactly
when the architect hits upon the elegant, harmonic and original
relationship between symmetry and asymmetry.
\speak{\txtread}  True, in the modern massive construction of standard
dwellings there is no question of the correlation between symmetry and
asymmetry.
\speak{\txtauth}  These days this problem takes on a \emph{new perspective}. It is now
solved not on the level of a separate house, but on the level of
a neighbourhood or even a town. In earlier times an architectural
ensemble distinguished by its individuality was a house (temple, palace,
etc.). But now more frequently this role is played by a group of houses, for
example, a district. And it is on this level that present-day builders are
called upon to solve the problem of symmetry-asymmetry.
\speak{\txtread}  \emph{Architecture} provides plenty of examples of the dialectical unity
of symmetry and asymmetry.
\speak{\txtauth}  In \emph{music} and \emph{poetry} we have the same thing. In 1908 a Russian
physicist, G. Vulf, wrote: 
\begin{quote}
The heart of music - rhythm - consists in regular periodic repetition of parts of a musical piece. But regular
repetition of similar parts to make up the whole constitutes the essence of symmetry. We can apply the concept of symmetry to musical piece all the more so since this piece is put down using notes, i.e., it acquires a spatial
geometrical image, whose parts we can overview. 
\end{quote}
He wrote elsewhere:
\begin{quote}
Just like musical pieces, symmetry may be inherent in literary works, especially poems.
\end{quote}
\speak{\txtread}  As for poems, you surely mean the symmetry of rhymes,
stressed syllables, that is, in the final analysis \emph{rhythm} again.
\speak{\txtauth}  Right indeed. But both in music and in poetry symmetry
cannot be reduced just to rhythm and cadence. Any good work (musical
or literary) includes some \emph{invariants of meaning} which permeate, varying
their form, throughout the work. In his symphony a composer generally
returns to the main theme many times, each time varying it.
\speak{\txtread}  We have already seen something of the sort in the example of
the Cathedral of St. Basil the Blessed.
\speak{\txtauth}  The preservation of a theme and its \emph{varying (developing)} is here
the \emph{unity of symmetry and asymmetry}. And the more successful is an
architect, a composer, or a poet in solving the problem of the correlation
between symmetry and asymmetry, the higher is the \emph{artistic value} of the
produced piece.

Of direct concern to symmetry is \emph{composition}. The great German poet
Johann Wolfgang Goethe argued that ``any composition is based on
latent symmetry.'' To master the \emph{laws of composition} is to master the laws
of symmetry. The three principal laws of composition dictate the
translation-identical repetition of elements of structure, contrasted
repetition, varied repetition.
\speak{\txtread}  This appears as an \emph{ornament in time}.
\speak{\txtauth}  Really, temporal pattern of sorts. We will always admire the
``patterns'' produced by the great Russian poet A. Pushkin. Here is
a relatively simple, elegant Pushkinian pattern:
\begin{small}
That year was extraordinary,
The autumn seemed so loth to go;
Upon the third of January,
At last, by night, arrived the snow.
Tatyana, still an early riser,
Found a white picture to surprise her:
The courtyard white, a white parterre,
The roofs, the fence, all moulded fair:
The frost-work o'er the panes was twining;
The trees in wintry silver gleamed;
And in the court gay magpies screamed;
While winter's carpet, softly shining,
Upon the distant hills lay light,
And all she looked on glistened white.
(Tranlsated by Babette Deutsch)
\end{small}
We will not here examine the intonation in this excerpt. Let us reread it
again and again to absorb the splendour of these poetical ornaments.
\speak{\txtread}  Let us now turn to \emph{painting}. Where does symmetry come in
here?
\speak{\txtauth}  A picture is not a colour photograph, not by any means. The
arrangement of figures, the combinations of stances and gestures, the
countenances, the palette, the combination of shades, these all are
carefully thought out in advance by the painter, whose main concern is to
affect the beholder in the way he desires. By using asymmetrical elements,
the painter strives to create something that on the whole features \emph{latent
symmetry}. The Russian painter V. Surikov wrote: 
\begin{quote}
And how much time it
takes for the picture to settle down so that it would be impossible to
change anything. The real sizes of each object are to be found. It is
necessary to find the lock to piece together the elements. This is
mathematics.
\end{quote}
\speak{\txtread}  To analyze the symmetry of an image it would be better perhaps
to take a picture with a simple composition.
\speak{\txtauth}  We can take the picture \emph{Madonna Litta} (Figure~\ref{madonna}) by the Italian
genius Leonardo da Vinci.
\begin{figure}[!h]
\centering
\includegraphics[width=0.75\textwidth]{figs/ch-18/madonna}
\caption{\emph{Madonna Litta} by Leonardo da Vinci.}
\label{madonna}
\end{figure}
Notice that the figures of the madonna and the child are inscribed into an \emph{isosceles triangle}, which is especially clearly perceived by the beholder's eye. This immediately brings the mother and the child to the
\emph{forefront}. The madonna's head is placed absolutely exactly, but at the same time naturally, between the two symmetrical windows in the background. Through the window we can see the gentle horizontal lines of low hills and clouds. All of this adds to the air of the peace, which is intensified by the harmony of the blue colour with the yellowish and reddish tones.
\speak{\txtread}  The inner symmetry of the picture is apparent. But what can be
said about asymmetry?
\speak{\txtauth}  Asymmetry manifests itself, for example, in the body of the
child, which cuts through the triangle. Furthermore, there is one highly
expressive detail. Owing to the \emph{closeness}, completeness of the lines of the
madonna's figure an impression is produced that the madonna is totally
indifferent to the surrounding world, and to the beholder too. The
madonna is all concentrated on the child, she holds him gently and looks
at him tenderly. She is thinking only about him. And suddenly all the
closeness of the picture is gone, once we meet the stare of the child. And it
is here that the inner poise of the composition is \emph{disturbed}: the serene and
attentive stare of the child is directed at the beholder, and so the picture
\emph{achieves its contact with the world}. Just try and remove that marvellous
asymmetry, turn the face of the child to the mother, connect their glances.
This will kill the expressiveness of the picture.
\speak{\txtread}  It so happens that each time when we, marvelling at a piece of
art, speak about harmony, beauty, emotional intensity, we thereby touch
on the same inexhaustible problem - the problem of the correlation of
symmetry and asymmetry.
\speak{\txtauth}  As a rule, in a museum or a concert hall we \emph{are not concerned
with that problem}. After all it is impossible \emph{at the same} time to perceive
and analyze the perception.
\speak{\txtread}  The example of Leonardo's picture has convinced me that the
analysis of symmetry-asymmetry is still useful: the picture begins to be
perceived more acutely.
\speak{\txtauth}  We see thus that symmetry is dominant not only in the \emph{process of scientific quest} but also in the \emph{process of its sensual, emotional perception of the world}. Nature-science-art In all of these we find the age-old competition of symmetry and asymmetry.
\end{dialogue}

%\newpage

\chapter*{Literature}
\addcontentsline{toc}{chapter}{Literature}

H. Weyl, \emph{Symmetry} (Princeton, Princeton University Press, 1952)

M. Gardner, \emph{The Ambidextrous Universe} (New York, Charles Scribner's Sons, 1979)

H. Lindner, \emph{Das Bild der modernen Physik} (Leipzig, Urania-Veriag, 1975)

R. Feynman, \emph{The Character of Physical Law} (London, Pergamon Press, 1965)

K. Ford, \emph{The World of Elementary Particles} (New York, Blaisdell Publishing Company, 1963)

A. Kitaigorodsky, \emph{Order and Disorder in the World of Atoms} (Moscow, Mir Publishers, 1980)

P. Davies, \emph{The Forces of Nature} (Cambridge, Cambridge University Press, 1979)

E. Harrison, \emph{Cosmology. The Science of the Universe} (Cambridge, Cambridge University Press,
1981)

%\begin{figure*}[!h]
%\centering
%\includegraphics[width=0.9\textwidth]{figs/ch-04/regular-polyhedra.png} 
%\caption{The five possible types of regular polyhedra. \label{regular-poly}} 
%\end{figure*}
%
%\begin{marginfigure}
%\centering
%\includegraphics[width=0.9\textwidth]{figs/ch-04/dodecahedron-symm.png}
%\caption{The symmetry of dodecahedron.}
%\label{dodeca-symm}
%\end{marginfigure}
%
%
%\begin{figure}[!h]
%\centering
%\includegraphics[width=0.75\textwidth]{figs/ch-04/kepler.png}
%\caption{Scheme of the Solar System based on the regular polyhedra devised by Kepler.}
%\label{kepler-scheme}
%\end{figure}
%
%
%
%                        