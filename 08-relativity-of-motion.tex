% !TEX root = tarasov-symmetry.tex






\chapter{Symmetry and The Relativity of Motion}

\marginnote{ The laws governing natural events are independent of the state of motion of the reference
frame in which these events occur if the frame travels without acceleration. - Albert Einstein}

The concept of symmetry is not confined to the symmetry of objects. It also covers \emph{physical events} and \emph{laws} that govern them. The symmetry of physical laws resides in their \emph{unchangeability}, or rather invariance, under one or another of transformations related, for example, to the conditions under which the phenomenon is observed.
Scientists got interested in the issue of symmetry in the laws of physics in connection with the studies that had led to the development of special theory of relativity. The symmetry here is the symmetry (invariance) of the laws of physics \emph{under transition from one inertial reference frame to another inertial reference frame}, or the symmetry in relation to uniform motion.

\section{The Relativity Principle}

Suppose that a railway carriage moves smoothly and uniformly. You travel in the carriage and want to find out whether it is moving or it is at rest. Could this be done without looking out of the window? The reader must know the answer. In that case it is in principle impossible to find this out because all the physical processes occur in the \emph{same manner} in a carriage at rest and in a carriage in uniform motion.

This is known as the \emph{principle of relativity} for inertial reference frames. Recall that a reference frame is said to be inertial if a body in it moves uniformly when not exposed to the action of external forces. Any two inertial reference frames are in uniform motion relative to each other. In the above example of the train carriage one inertial frame is related to bodies at rest on the ground, the other is related to the carriage, which moves uniformly and rectilinearly. We can here ignore the rotation of the Earth and its revolution about the Sun.
The principle of relativity can be formulated as follows. \emph{Any process in nature occurs in the same manner in any inertial reference frame; in all inertial frames a law has the same form.}

As applied to mechanical phenomena the relativity principle was established by Galileo. The principle was generalized to apply to all processes in nature, including \emph{electromagnetic} ones, by the foremost physicist of the 20th century Albert Einstein (1879-1955), the father of relativity theory. Drawing on the very essence of the relativity principle, Einstein postulated that the \emph{velocity of light in a vacuum must be the same in all inertial reference frames}. Einstein's discovery was a great break- through in science, since it subjected all the age-old views of space and time to an overhaul.


\section{The Relativity of Simultaneous Events}

It follows from the invariance of the speed of light with respect to translations from one inertial reference frame to another that two spatially separated events that are \emph{simultaneous} in one frame may be \emph{non-simultaneous} in the other. Let us take a simple example.

\begin{figure}
\centering
\includegraphics[width=0.75\textwidth]{figs/ch-08/frames-of-ref.jpg}
\caption{Two inertial frames of reference with relative velocity $v$.}
\label{frame-01}
\end{figure}
Consider two inertial frames of reference $xyz$ and $x'y'z'$. Let frame $x'y'z'$ be travelling relative to frame $xyz$ along $x$- and $x'$-axes with a speed $v$ (Figure~\ref{frame-01}). In frame $x'y'z'$ there are a light source $A$ and two 
detectors, $B$ and $C$, that lie at equal distances from $A$ along the $x'$-axis. Light source $A$ sends out two light impulses simultaneously, one in the direction of $B$, the other in the direction of $C$. Since $AB = AC$ and both signals travel with the same speed, the observer in $x'y'z'$ will see detectors $B$ and $C$ operate \emph{simultaneously}.

Let's turn now to the observer in $xyz$. In this frame of reference the light signal that travels to the left will have to cover a smaller distance from production to registration than the signal that travels to the right. The speed of light in $xyz$ and $x'y'z'$ being the same, for the observer in frame $xyz$ detector $B$ will operate \emph{earlier} than $C$.

\section{The Lorentz Transformations}

We will stay with our inertial frames of reference $xyz$ and $x'y'z'$ shown in (Figure~\ref{frame-01}. Let an event occur at a time $t$ at a point $(x,\,y,\,z)$ in the frame $xyz$. In the frame $x'y'z'$ the same event occurs at the time $t'$ at the point $(x', y', z')$. The space and time coordinates of the event in the frames $xyz$ and $x'y'z'$ are related by

\begin{equation}%
\left.
\begin{split}
x' & = \dfrac{x - vt }{\sqrt{1 - \dfrac{v^{2}}{c^{2}}}} \,\, ;\\
y' & = y \,\, ; \\
z' & = z \,\, ; \\
t' & = \dfrac{t - \dfrac{vx}{c^{2}} }{\sqrt{1 - \dfrac{v^{2}}{c^{2}}}} \,\,,
\end{split}
\quad \quad \right\}
\label{eq-8.1}
\end{equation}

where $c$ is the speed of light in empty space. These relationships are called the \emph{Lorentz transformations} (from the Dutch physicist Hendrik Lorentz who first derived them).

The symmetry of physical laws with respect to changes from one inertial reference frame to another are mathematically expressed in that the relevant mathematical expressions must conserve their form if $x, \, y, \,z, \,t $in them are replaced by $x', \, y', \, z', \, t'$ according to \eqref{eq-8.1}. Put another way, the mathematical expressions of physical laws must possess \emph{symmetry with respect to the Lorentz transformations}. We will illustrate this important statement with reference to the physical law that states the constancy of the speed of light in all the inertial coordinate systems.

Suppose that a light signal originates from point $x = y = z = 0$ at $t = 0$ along the $x$-axis in the system $xyz$. At a time t it will be registered at the point $x = ct, \,\, y = z = 0$. If the speed of light is the same in all the inertial systems, then, substituting $x/t = c$ into \eqref{eq-8.1}, we arrive at $x't' = c$. We will now see that this is so. Dividing the first of \eqref{eq-8.1} by the last one gives
\begin{equation*}%
\dfrac{x'}{t'} = \dfrac{x - vt}{t - \dfrac{vx}{c^{2}}} = \dfrac{\dfrac{x}{t} - v}{ 1 - \dfrac{vx}{c^{2}t}} = \dfrac{c -v}{1 -\dfrac{v}{c}} = c
\end{equation*}
Using \eqref{eq-8.1}, we can easily demonstrate the relative nature of the simultaneousness of events. Let two events have in frame $xyz$ the space-time coordinates $x_{1}, \,t_{1}$ and $x_{2}, \, t_{2}$ (in this case we can neglect the coordinates $y$ and $z$), and in frame $x' \, y' \, z'$ the coordinates  $x'_{1}, \,t'_{1}$ and  $x'_{2}, \,t'_{2}$, respectively. From \eqref{eq-8.1}, we get
\begin{equation*}%
t'_{2} - t'_{1} = \dfrac{(t_{2} - t_{1}) - \dfrac{v}{c^{2}} (x_{2} - x_{1)}}{\sqrt{1 - \dfrac{v^{2}}{c^{2}}}} 
\end{equation*}
Suppose that the events are simultaneous in frame $xyz$, but at different spatial points. This means that $t_{2} = t_{1}, \,\, x_{2} \neq x_{1}$. It is easily seen that in this case $t'_{2} \neq t'_{1}$; in other words, in frame $x'y'z'$ the events are not simultaneous.

If the speed of the relative motion of $xyz$ and $x'y'z'$ (speed $v$) is much lower than the speed of light, then relationships \eqref{eq-8.1} simplify to yield
\begin{equation*}%
\left.
\begin{split}
x' &= x - vt\\
y' &= y\\
z' &= z\\
t' &= t.
\end{split}
\quad \quad \right\}
\end{equation*}


These relationships are known as the \emph{Galilean transformations}. They reflect the principle of relativity in classical mechanics due to Galileo. From the viewpoint of the special theory of relativity, the Galilean transformation equations are a \emph{special case} of the Lorentz transformation equations. which is valid at $v \ll c$.

It is worth noting that, while recognizing the relative nature of spatial coordinates ($x' = x - vt$), the Galilean transformation equations at the same time assumed that \emph{time is absolute} ($t' = t$). The notion of absolute time is deeply rooted in the human mind. We are in the habit of thinking that the phrase ``the event is occurring \emph{now}, at this moment" has the same sense for all reference systems and for the whole of the Universe.

Physically, the fundamental difference between the Galilean and Lorentz transformations lies in the fact that in the first case we ignore the finiteness of the propagation of light velocity signals, whereas in the second case we take it into account. When dealing with relatively slow motions the approximation is quite justified. As follows from \eqref{eq-8.1}, with finite speeds \emph{we have to forego the absolute nature o f time and consider the spatial and temporal coordinates jointly}. This is a consequence of the \emph{symmetry of physical laws with respect to changing from one inertial reference frame to another, which manifests itself when we take into account the finiteness of the speed of light}.

\section{The Relativity of Time Periods}

Suppose two events occur at the same point in frame $x'y'z'$ separated by a time $\tau'$; in symbols: $x_{2}' - x_{1}'= 0, t_{2}' - t_{1}' = r\tau'$. Suppose further that frame $x'y'z'$ is connected, say, with a spacecraft travelling at a velocity $v$ relative to the Earth, and the above-mentioned \emph{events} are the ``astronaut left his chair'' and ``the astronaut returned to his chair''. The frame $x'y'z'$ is called the \emph{rest frame} for these events, since they are, as it were, at rest, i.e., occur at the same point in space. The time period between the two events in the rest frame is called the \emph{proper time}.

Let us now turn to a frame $xyz$ connected with the Earth. The above events, if considered in frame $xyz$, i. e. from the Earth, will occur at different spatial points: $x_{2}$ and $x_{1}$. The events are separated by the time span $ \tau = t_{2} - t_{1}$ by the clock of the terrestrial observer. Using (\eqref{eq-8.1}), we can easily find that
\begin{equation*}%
x = \dfrac{x' + vt }{\sqrt{1 - \dfrac{v^{2}}{c^{2}}}} \,\, ;\quad t = \dfrac{t' + \dfrac{v x'}{c^{2}} }{\sqrt{1 - \dfrac{v^{2}}{c^{2}}}} 
\end{equation*}
Hence
\begin{equation*}%
\tau = t_{2} - t_{1} = \dfrac{t_{2}' - t_{1}' + \dfrac{v}{c^{2}} \, (x_{2}' - x_{1}')}{\sqrt{1 - \dfrac{v^{2}}{c^{2}}}} \,\, ;\quad  = \dfrac{\tau'}{\sqrt{1 - \dfrac{v^{2}}{c^{2}}}} 
\end{equation*}
Consequently, the time between two events depends on the choice of the reference frame. This time is minimal in the rest frame for the given events (proper time). The time increases by a factor of $(1 - v^{2}/c^{2})^{-1/2}$ in the frame that moves at a speed $v$ relative to the rest frame.\footnote{In connecting the rest frame with a spaceship, we consider the motion of the Earth relative to the ship. It will be recalled that it makes no sense to find out which of the two inertial frames is actually moving and which is at rest, since there only exists relative motion.}

Suppose that the speed of the ship is fairly close to the speed of light; for example, $v/c = 0.9999$ (maybe such ships will be available in the future). In this case $\tau \approx  70 \tau'$. The astronaut has only left his chair for 20 minutes, and on Earth the clock showed all of 24 hours.

However, it can easily be surmised that this situation is \emph{reversible}. You, on Earth, will take half an hour to read this section, and on the spaceship this time span will again be 24 hours. The situation will only be irreversible if the ship returns back to Earth in the long run. But this is a separate topic that lies beyond the scope of the book.

\section{The Speed in Various Frames}

Before leaving the subject of the special theory of relativity we will consider how the \emph{speed of a body} changes when we go over from one inertial frame to another. We can easily feel the contradiction between the classical composition of speeds and the postulate that speed of light is constant in all inertial frames. The special theory of relativity requires that the classical rule be replaced by another, more general one.

Let a body move in a frame $xyz$ uniformly with a speed $V$ along the $x$-axis, and let the speed of the body be $v'$ in a frame $x'y'z'$ that moves with a speed $v$ relative to $xyz$ (see Figure~\ref{frame-01}). Considering that $V = x/t$ and $V' = x'/t'$, from \eqref{eq-8.1} we directly obtain the rule
\begin{equation*}%
V' = \dfrac{V - v }{\sqrt{1 - \dfrac{Vv}{c^{2}}}} \,\, ;\quad V = \dfrac{V' + v }{\sqrt{1 + \dfrac{V'v}{c^{2}}}} 
\end{equation*}

If $v \ll c$, then the denominator becomes unity, and so we arrive at the classical rule: 
\begin{equation*}%
V = V' + v
\end{equation*}


%
%\begin{figure*}
%\centering
%\includegraphics[width=0.9\textwidth]{figs/ch-04/regular-polyhedra.png} 
%\caption{The five possible types of regular polyhedra. \label{regular-poly}} 
%\end{figure*}
%
%\begin{marginfigure}
% \centering
%  \includegraphics[width=0.9\textwidth]{figs/ch-04/dodecahedron-symm.png}
%  \caption{The symmetry of dodecahedron.}
%  \label{dodeca-symm}
%\end{marginfigure}
%
%
%\begin{figure}
%\centering
%\includegraphics[width=0.75\textwidth]{figs/ch-04/kepler.png}
%\caption{Scheme of the Solar System based on the regular polyhedra devised by Kepler.}
%\label{kepler-scheme}
%\end{figure}
%
%
%
%                        