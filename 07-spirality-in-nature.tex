% !TEX root = tarasov-symmetry.tex






\chapter{Spirality In Nature}


\marginnote{Pasteur was more right than many of his colleagues suspected when he wrote eloquently of left-right asymmetry as a key to the mystery of life. At the heart of all living cells on earth are right-handed coils of nucleic acid. This asymmetric structure is surely the master key of life. - M. Gardner} 

Mirror asymmetry (also called \emph{left-right asymmetry}) widely occurs in nature and is of principal importance for living things. A characteristic example of a mirror-asymmetric object is a \emph{helix} or a \emph{spiral}. This explains why left-right asymmetry is often referred to as spirality. Also used is the term handedness. Recall that the \emph{hand} is an example of a mirror- asymmetric object.

\section{The Symmetry and Asymmetry of the Helix}
The figure in \hyperref[helical-01]{Figure~\ref{helical-01}} appears to be unmoved if turned about axis $OO$ through \ang{60} and translated along the same axis through distance $a$. The axis is a six-fold \emph{helical axis} with translation period $a$. \emph{Helical symmetry} is a symmetry relative to a combination of a turn and a translation along the rotation axis. An example of an object featuring helical symmetry is the spiral staircase.

For an object having helical symmetry we can draw a \emph{helical line}, or spiral (the dash line in \hyperref[helical-02]{Figure~\ref{helical-02}}). The helical line can be constructed as follows. Cut a right-angled triangle out of paper ($ABC$ in \hyperref[helical-01]{Figure~\ref{helical-01}}). Take a circular cylinder and glue to its surface the leg $BC$ of triangle $ABC$ so that the leg coincides with the generatrix of the cylinder surface. Next wrap the triangle around the cylinder tightly pressing the paper to the cylinder surface, the hypotenuse $AB$ will give the helical line. Two methods of turning the triangle around the cylinder are possible, both of which are shown in \hyperref[helical-01]{Figure~\ref{helical-01}}. One of them produces a \emph{left-handed} and the other a \emph{right-handed} spiral.

\begin{figure}[!ht]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-07/helical-lines.jpg}
\caption{Construction of a helical line.}
\label{helical-01}
\end{figure}
\begin{marginfigure}[-6cm]
 \centering
  \includegraphics[width=0.65\textwidth]{figs/ch-07/helical-lines02.jpg}
  \caption{A helical line.}
  \label{helical-02}
\end{marginfigure}

The type of a helical line is identified rather simply. Let us mentally move along a helical line, the motion will have two components - \emph{along} the helical axis and \emph{around} the axis (the straight and circular arrows in \hyperref[helical-01]{Figure~\ref{helical-01}}). Let us place the observer so that the structure moved along the helical axis \emph{away from him}. If in the process the circular motion is \emph{clockwise}, then the helical line is termed \emph{right-handed}, and if \emph{counter-clockwise}, it is called \emph{left-handed}. In other words, if a point moving away from the observer in a helical line appears to him to turn clockwise, the spiral is a right-handed one, if counterclockwise, a left-handed one. When reflected in a mirror, a left-handed spiral will become a right- handed one, and vice versa. The left- and right-handed spirals form a pair of \emph{enantiomorphs}.

Note that speaking about the helical line or the screw, the term ``spiral'' is often used. It will be remembered that we have here a \emph{spatial} spiral.


\section{Helices in Nature}

We use helices widely in technology. It is perhaps of interest that helices are also encountered in nature. Some examples of natural helices are shown in \hyperref[helical-03]{Figure~\ref{helical-03}}: 
\begin{enumerate*}[label=(\alph*)]
\item the tusk of the narwhal, a small cetacean animal endemic of northern seas, is a left-handed helix, 
\item the shell of a snail \emph{Note: Each type of shell has a certain spirality. Any ``freaks", which are encountered from time to time, having the opposite spirality, are especially valued}, 
\item the umbilical cord of a new-born is a triple left-handed spiral formed by two veins and one artery, 
\item (horns of the Pamir sheep are enantiomorphs (one horn is left-handed and the other right-handed spiral).
\end{enumerate*}

\begin{figure}[!ht]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-07/natural-helices.jpg}
\caption{Some natural helices.}
\label{helical-03}
\end{figure}

In plants we may find numerous signs of helical symmetry in the arrangement of leaves on the stalk, branches of the stem, the structure of cones, some flowers, and so on. Creeping plants are remarkable helices. Examples are known of entangled creeping plants with different handedness, for example\emph{bindweed} and \emph{honeysuckle}. This gives rise to bizarre arrangements, which repeatedly attracted poets. Crystal lattices, as a rule, have mirror symmetry. But there exist also mirror-asymmetric lattices, some of them are characterized by a helical structure. An example of a twisted crystal lattice is \emph{quartz}. Its base is a \emph{tetrahedron} with a silicon atom at the centre and oxygen atoms at its vertices. Along the main crystal axis the tetrahedra lie along a \emph{helical line}. The quartz lattice may be twisted either to the left or to the right. Therefore, there exist two enantiomorphic forms of quartz. The left and right single crystals of quartz are represented in \hyperref[crystals-03]{Figure~\ref{crystals-03}}. It is easily seen that one is a mirror image of the other.

\begin{figure}[!ht]
\centering
\includegraphics[width=0.6\textwidth]{figs/ch-07/handed-crystals.jpg}
\caption{The handedness in crystals.}
\label{crystals-03}
\end{figure}
Another realm of natural screws is the world of  ``living molecules'', those molecules that play an important role in biological processes.

These molecules include, above all, \emph{protein molecules}, the most complex and numerous of all the carbon compounds. In man there are up to \num{d5} types of proteins. All the human organs, including bones, blood, muscles, sinews, and hair, contain proteins. The many ferments and hormones are proteins as well.
The protein molecule contains atoms of carbon, hydrogen, oxygen, and nitrogen. Each molecule has an enormous number of atoms, of the order of \numrange{d3}{d6}. Each giant molecule contains many links (amino acids) connected into a chain. The frame of the chains is twisted as a right- handed spiral. In chemistry it is called \emph{Pauling's alpha-spiral} (after the prominent American chemist Linus Pauling). The molecules of sinew fibres are triple alpha-spirals. Alpha-spirals twisted repeatedly with each other form molecular helices such as those found in hair fibres, in horny substance, and so on.

Especially important in living nature are the molecules of \emph{deoxyribonucleic acid} (DNA), which are bearers of hereditary information in living organisms. The DNA molecule has the structure of a \emph{double right-handed spiral} which, in a sense, is the main natural helix. Let us take a closer look at the structure of that molecule.



\section{The DNA Molecule}
\begin{marginfigure}
 \centering
  \includegraphics[width=0.8\textwidth]{figs/ch-07/dna-schematic.jpg}
  \caption{A schematic diagram of the DNA molecule.}
  \label{dna-01}
\end{marginfigure}
A schematic diagram of the DNA molecule is given in \hyperref[dna-01]{Figure~\ref{dna-01}}. The molecule consists of many links called \emph{nucleotides}, the links being connected into two chains (in the figure the nucleotides are shown by red rectangles). Each nucleotide contains a \emph{sugar} molecule, a phosphoric acid molecule (\emph{phosphate}), and a molecule of a nitrogen-containing compound (\emph{nitrous compound}). The nitrogenous bases of two nucleotide strands are linked by hydrogen bonds shown by dash lines in \hyperref[dna-01]{Figure~\ref{dna-01}}. The structure in the figure looks like a ladder, in which the vertical elements are \emph{sugar-phosphate chains}, and the rungs are \emph{pairs of nitrogenous bases}.
\begin{figure}[!ht]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-07/dna-structural.jpg}
\caption{The structural formulae for the ATGC nitrogenous bases in DNA molecule.}
\label{dna-02}
\end{figure}
There are four nitrogenous bases: \emph{adenine} (A) and \emph{guanine} (G) (purines), \emph{thymine} (T) and \emph{cytosine} (C) (pyrimidines). Each rung contains either adenine and thymine (\ce{A-T}or \ce{T-A}), or guanine and cytosine (\emph{G-C} or \emph{C-G}). \hyperref[dna-02]{Figure~\ref{dna-02}} represents the structural formulae of the adenine-thymine pair and the guanine-cytosine pair, which enter the DNA molecule. There are no combinations of adenine with guanine or thymine with cytosine.

Considering the above, the ``ladder'' schematically representing the structure of DNA assumes the form shown in \hyperref[dna-03]{Figure~\ref{dna-03}}. The arrangement of the pairs AT, TA, GC, and CG along the ``ladder'' is the \emph{genetic code} of a living organism. Despite the fact that there exist only four types of rungs, the enormous number of these rungs on the ladder enables DNA to include all hereditary information.

\begin{marginfigure}[-3cm]
 \centering
  \includegraphics[width=0.7\textwidth]{figs/ch-07/dna-ladder.jpg}
  \caption{A schematic diagram of the structure of the DNA molecule showing the ``ladder''.}
  \label{dna-03}
\end{marginfigure}


This information is retained when cells are reproduced. The DNA molecule divides into two halves (along the red line in Figure~\ref{dna-03}), each of which is essentially a sugar-phosphate chain with nitrogenous bases arranged normally to the chain. Since each base may only bond to
a \emph{specific} base (A to T, T to A, G to C, C to G), then each of the halves will build up to form a molecule that is a complete replica of the initial DNA. 

In passing from the schematic representation of the DNA molecule to its real \emph{spatial} structure, it is necessary to take into account the fact that each sugar-phosphate chain represents a right-handed spiral, so that on the whole the DNA molecule appears as a \emph{double right-handed spiral} and
looks like a spiral staircase, rather than a ladder (Figure~\ref{dna-04}).

\begin{marginfigure}
 \centering
  \includegraphics[width=0.7\textwidth]{figs/ch-07/dna-spiral.jpg}
  \caption{A schematic diagram of the structure of the DNA molecule showing the double right-handed spiral.}
  \label{dna-04}
\end{marginfigure}

It is worth noting that all the spirals in the DNA molecules in man are right-handed. Among the prodigious wealth of them not a single left- handed one is to be found. Note that the DNA strands in one cell may be up to a metre in length. All in all, there are \SI{e11}{\km} of DNA in man.

The structure of DNA was discovered in 1953 by a team of scientists
that included the American Watson and the Englishmen Crick and Wilkins. This discovery is justifiably regarded as one of the most remarkable contributions to biology in the 20th century.

\section{The Rotation of the Plane of Light Polarization}

Some media possess a fascinating property: when a plane-polarized light beam is passed through them, the light polarization plane turns by a certain angle. Such media are termed \emph{optically active}. The media may be \emph{levorotary} or \emph{dextrorotary}. Suppose that the beam strikes our eye. If the polarization planes turn \emph{clockwise}, the medium is called \emph{dextrorotary} (Figure~\ref{polarisation-01}~a); if \emph{counterclockwise}, \emph{levorotary} (Figure~\ref{polarisation-01}~b). \footnote[][-4cm]{ Note that the ``left-handed'' (``right-handed'') combination of the straight and circular arrows in Figure~\ref{polarisation-01} differs from the appropriate combination in Figure~\ref{helical-01}. This implies that in a \emph{levorotary} medium the polarization plane turns following in fact a \emph{right-handed helix}, and in a \emph{dextrorotary} medium it turns following a \emph{left-handed} helix.}

\begin{figure}[!ht]
\centering
\includegraphics[width=0.8\textwidth]{figs/ch-07/stereo-isomerism.jpg}
\caption{The action of  \emph{levorotary} and \emph{dextrorotary} media on plane-polarized light.}
\label{polarisation-01}
\end{figure}


We are not going to consider the nature of the phenomenon of polarization plane rotation. We will only note that an optically active medium must possess a left-right asymmetry, which dictates the rotation of the polarization plane in one direction or the other.

An example of optically active media is the quartz crystal.

The direction of the turn of the polarization plane depends on to which enantiomorphic variety a given crystal belongs. Dextrorotary crystals are generally called right-handed, and levorotary crystals are called left- handed ones. The optical activity of quartz is associated with the left- right asymmetry of the lattice. If a quartz crystal is dissolved in a liquid, no rotation of the plane of light polarization is observed.

It would seem that the presence of a left-right asymmetry of crystal structure is a necessary condition for a polarization plane to turn. You can imagine the surprise of the prominent 19th century physicist Jean-Baptiste Biot when he discovered optical activity in aqueous solutions of some organic compounds, for instance, \emph{sugar} and \emph{tartaric acid}. It thus followed that the left-right asymmetry could be associated not only with the structure of a medium as a whole but also with the structure of molecules in the medium. This gave rise to the terms ``left'' (levorotary) and ``right'' (dextrorotary) molecules.

\section{Left and Right Molecules. Stereoisomerism}

In Chapter 6 we discussed the molecules of methane and methyl alcohol (see Figure~\ref{methane-symm}). Both molecules are identical to their mirror images. This is only natural since they feature mirror symmetry (six planes of symmetry in the methane molecule and three planes of symmetry in the methyl alcohol molecule).

Let us now substitute radical \ce{CH3} for one of the three identical hydrogen atoms in the molecule of methyl alcohol, that is, pass from the structural formula
\begin{center}
\setatomsep{20pt}
\chemfig{C(-[:0]OH)(-[:90]H)(-[:180]H)(-[:270]H)} \qquad \chemfig{C(-[:0]OH)(-[:90]H)(-[:180]\ce{CH3})(-[:270]H)}
\end{center}
\begin{marginfigure}
 \centering
  \includegraphics[width=0.8\textwidth]{figs/ch-07/ethyl-alcohol.jpg}
  \caption{Ethyl alcohol .}
  \label{ethyl-01}
\end{marginfigure}
This is the formula of \emph{ethyl alcohol}. The tetrahedral model of this
molecule is provided in Figure~\ref{ethyl-01}~a, and the model of its mirror image in
Figure~\ref{ethyl-01}~b. It appears that in this case too the mirror image is identical to
its original molecule: to obtain a mirror image we will have to turn the molecule of ethyl alcohol by \ang{180} about axis $AB$ (see the figure). In other words, the molecule of ethyl alcohol Gust like those of methane and methyl alcohol) has no enantiomorphic versions. This is because despite the gradual reduction of the symmetry of molecular tetrahedron (in passing from methane through methyl alcohol to ethyl alcohol), it still remains mirror-symmetrical. It is easily seen that the molecule of ethyl alcohol has a plane of symmetry (plane $ABD$ in Figure~\ref{ethyl-01}~a.
                      
A different situation emerges if we take, for example, the molecule of  \emph{butyl alcohol}
\begin{center}
\setatomsep{20pt}
 \chemfig{C(-[:0]OH)(-[:90]H)(-[:180]\ce{CH3})(-[:270]\ce{C2H5})}
\end{center}
The spatial tetrahedral model of the molecule is presented in Figure~\ref{butyl-01}~a, and its mirror image in Figure~\ref{butyl-01}~b. The molecule has no plane of symmetry, it is \emph{mirror-asymmetrical}. Therefore, both it and its mirror image are enantiomorphs, and no turns can effect a coincidence of the molecules shown in Figure~\ref{butyl-01}. One of the molecules can be termed ``left'', and the other ``right''.
\begin{marginfigure}
 \centering
  \includegraphics[width=0.8\textwidth]{figs/ch-07/butyl-alcohol.jpg}
  \caption{Butyl alcohol .}
  \label{butyl-01}
\end{marginfigure}
Thus, if the spatial structure of a molecule excludes planes of symmetry, then it can have two forms, which are enantiomorphs. These forms are called \emph{stereoisomers}.

\emph{Stereoisomerism} is a manifestation of left-right asymmetry in the world of molecules. Stereoisomers are molecules that in addition to the same chemical composition have the same geometrical shape, the same struc- tural elements, and the same inner bonds. At the same time they are \emph{different} molecules. As different as, say, left and right shoes. The existence in nature of left- and right-handed molecules was suggested by observations of the rotation of polarization planes.

A special case among stereoisomers are, obviously, molecules with spiral structures but differing in handedness.

\section{The Left-Right Asymmetry of Molecules and Life}

Studies of the optical activity of solutions of organic compounds, initiated by Biot, were carried on by the famous French scientist Louis Pasteur. He came to the conclusion that if in \emph{inorganic} nature left- and right-handed molecules occur with equal frequency, in \emph{living} organisms mirror-asymmetrical molecules occur only in one enantiomorphic form. Pasteur speculated that it is here that the boundary between organic and inorganic nature lies. Further scientific evidence supported Pasteur's conjectures.

Recall that the alpha-spiral, which determines the structure of protein molecules, is invariably a right-handed spiral. The double spiral of the DNA molecule is also right-handed. Various mirror-asymmetrical molecules that enter the compositions of cells are, as a rule, represented either only by left-handed or only right-handed stereoisomers. So, amino acid molecules in proteins are always left-handed.

This all goes to prove that on the molecular level, a \emph{living organism is characterized by distinct left-right asymmetry}. An organism is constructed out of  ``helices", so to speak, some being only right-handed and others only left-handed.

One manifestation of that highly interesting thing is that left- and right-handed stereoisomers of a substance exert different actions of the human body. Man consumes in food those stereoisomers that \emph{correspond to the nature of his own asymmetry}. In a number of cases modern chemistry is able to obtain mirror-reflected stereoisomers. And then we can observe unexpected reactions of the human organism to them. So, the ``reflected" stereoisomer of vitamin C exerts essentially no effect on the body. The ``reflected" form of nicotine (never to be found in tobacco) is much less offensive. Right-handed aspartic acid is sweet, left-handed is tasteless. Minor additions of right-handed phenylalanine to food have no unpleasant implications, whereas additions of the left-handed form produce drastic metabolic disturbances (phenylketonuria) accompanied by mental disorders.

Suppose now that after a long space journey a man stepped onto an unknown planet. Suppose further that the planet is very much like the Earth (in its atmospheric composition, climate, landscape, plants, etc.). And so, the astronaut holds in his hand a fragrant apple just picked from an extraterrestrial appletree. But should he eat the apple? The enantiomorphic form of organic compounds in it is unknown. It is quite possible that an innocent-looking apple may appear to be biologically poisonous for an Earthling. In other words, mirror-asymmetrical molecules of a foreign plant world may turn out to be \emph{incompatible} with the mirror-asymmetrical human organism, just as a \emph{left-threaded} nut is incompatible with a \emph{right-threaded} bolt.

The beautiful children's book \emph{Through the Looking Glass} by Lewis Carroll contains a scene that today has deep scientific meaning. About to pass through the looking glass into the looking-glass house, Alice asks her kitten: 
\begin{quote}
\begin{smaller}
How would you like to live in Looking-Glass House, Kitty? I wonder if they'd give you milk in there? Perhaps looking-glass milk isn't good to drink?
\end{smaller}
\end{quote}
 As a matter of fact, milk includes many mirror-asymmetrical compounds, such as fats, lactose (milk sugar), and proteins. On passing over from the conventional world to the looking- glass world, all the asymmetrical molecules should have turned from some stereo isomers to other ones. Therefore, it is highly unlikely that the looking-glass milk would have been wholesome for the kitten. Although, if we were consistently to follow the situation described by Carroll in his book, we are to assume that in the looking-glass world both Alice and the kitten themselves would turn into their respective mirror images. And in that case the looking-glass milk would, of course, be for them as palatable and useful as the conventional, ``unreflected'' milk had been before.

We will conclude the chapter with the words of M. Gardner from his
book \emph{The Ambidextrous World}: 
\begin{quote}
\begin{smaller}
One of the most remarkable and least mentioned characteristics of life as we know it is the ability of an
organism to take compounds from its immediate environment, many of which are symmetrical in their molecular structure, and to manufacture asymmetrical carbon compounds that are right- or left-handed. Plants take symmetrical inorganic compounds such as water and carbon dioxide and from them manufacture asymmetrical starches and sugars. The bodies of all living things are with asymmetrical carbon molecules, as
well as the asymmetrical helices of proteins and nucleic acids.
\end{smaller}
\end{quote}

%
%\begin{figure*}
%\centering
%\includegraphics[width=0.9\textwidth]{figs/ch-04/regular-polyhedra.png} 
%\caption{The five possible types of regular polyhedra. \label{regular-poly}} 
%\end{figure*}
%
%\begin{marginfigure}
% \centering
%  \includegraphics[width=0.9\textwidth]{figs/ch-04/dodecahedron-symm.png}
%  \caption{The symmetry of dodecahedron.}
%  \label{dodeca-symm}
%\end{marginfigure}
%
%
%\begin{figure}
%\centering
%\includegraphics[width=0.75\textwidth]{figs/ch-04/kepler.png}
%\caption{Scheme of the Solar System based on the regular polyhedra devised by Kepler.}
%\label{kepler-scheme}
%\end{figure}
%
%
%
%                        