% !TEX root = tarasov-symmetry.tex






\chapter{Conservation Laws}

\marginnote{  There exist several basic laws of nature that have the mathematical
form of conservation laws. A conservation law states that in a closed system some physical quantity, for example, the total momentum or energy, remains constant at all times. J. Orear}

\section{An Unusual Adventure of Baron M\"unchhausen}

You may have read the story of M\"unchhausen mired in a bog with his horse. The galant baron relates: "Now the whole of my horse's body has sunk into the stinking mud, now my head too began to sink into the bog with only the plait of my wig sticking above the water. What was to be done? We would have perished if it had not been for the prodigious strength of my hands. I am awfully strong. And so I took hold of my plait, jerked with all my might and easily pulled out of the bog both myself and my horse, whom I squeezed tightly with my legs like with pincers." The reader can easily catch the esteemed baron in the lie. Really, according to Newton's third law (action and reaction) the plait acts on the hand with a force equal in magnitude and opposite in direction to the force with which the baron's hand acts on the plait. And since the hand and the plait are parts of the same physical system (the baron), the resultant force exerted by the baron on himself will clearly be zero.

It is thus in principle impossible to lift oneself by the hair. In a more general context, this prohibition can be viewed as a consequence of the \emph{momentum conservation law}. According to this law, the \emph{momentum of a system will not change as a result of the interaction of the components of the system with one another}. As applied to our case, this means that the baron's momentum directed downwards into the mire cannot change as a result of the interaction of the baron's hand with his plait.

If the system is not subject to external influences (it is called closed system), then there are no reasons for the system's momentum to change. In that case, the total momentum does not change with time or, as it is conventionally put, it is \emph{conserved}. The law of conservation of the \emph{momentum} of a closed system is one of the three important conservation laws. The two other laws are the laws of conservation of \emph{energy} and of \emph{angular momentum}.

\section{The Problem of Billiard Balls}

The game of billiards provides a good opportunity to illustrate the action of conservation laws for \emph{momentum} and \emph{energy}. We will take a cue into our hands and try and push one of the billiard balls so that it rolls exactly along the line connecting the centre of this ball with the centre of another ball. This sort of collision is called a \emph{head-on collision}. It is interesting that the first (striking) ball comes to rest at the moment of collision \emph{no matter what its initial speed}. The second ball starts moving along the line with a speed exactly equal to that of the first ball before the collision (Figure~\ref{head-on-collision}). 

\begin{marginfigure}
 \centering
  \includegraphics[width=0.9\textwidth]{figs/ch-10/collision-1.jpg}
  \caption{Before and after in a head-on collision.}
  \label{head-on-collision}
\end{marginfigure}

This result can be computed using the laws of conservation of energy
and momentum for the balls involved in the collision. Let $v$ be the speed of the striking ball, the second ball being at rest before the collision. Further, let $v_{1}$ and $v_{2}$ be the speeds of the first and second balls, respectively, after the collision (at the moment we do not know that the first ball will come to rest after the collision). Billiard balls collide in an \emph{elastic manner}, which means that no energy is lost in the collision. For an elastic collision the \emph{law of conservation of energy} is
\begin{equation*}%
\dfrac{mv^{2}}{2} =  \dfrac{mv_{1}^{2}}{2} + \dfrac{mv_{2}^{2}}{2}
\end{equation*}
where $m$ is the ball's mass. It follows that in the collision the total kinetic energy of the balls is conserved.

We will now turn to the \emph{law of conservation of momentum}. Recall that the momentum of a body with mass $m$ and speed $\vec{v}$ is $m\vec{v}$. Momentum is a \emph{vector} quantity, and so we have to take into account its direction. In a head-on collision we have the only direction singled out physically (the red dash line in Figure~\ref{head-on-collision}). Clearly, the momenta of the balls after the collision can only be directed along this direction. The \emph{one-dimensional} nature of the problem enables us to consider, in the law of conservation of momentum, solely the numerical values of the momenta, that is, to write the law in its scalar form:
\begin{equation*}%
mv =  mv_{1} + mv_{2}
\end{equation*}
Combining both laws, we arrive at the set of equations in $v_{1}$ and $v_{2}$
\begin{equation*}%
\left.
\begin{split}
v^{2}	 & =  v_{1}^{2} + v_{2}^{2} \\
v & = v_{1} + v_{2}
\end{split}
\; \right\}
\end{equation*}
Squaring both sides of the second equation gives
\begin{equation*}%
v_{1}^{2} + 2 v_{1} v_{2} + v_{2}^{2} = v^{2}
\end{equation*}
From the first equation we obtain 
\begin{equation*}%
2v_{1} \, v_{2} = 0
\end{equation*}

Physically, it is clear that $v_{2} \neq= 0$ (the second ball cannot remain at rest after the impact). We therefore conclude that $v_{1} = 0$ and hence $v_{2} = v$. If billiard balls collide not in a central way (\emph{off-centre} collision), then after the impact both balls will move apart in different directions. It is remarkable that in all cases the balls will move apart at a right angle, which can easily be tested in practice. But this result can be predicted from the laws of conservation of energy and momentum for colliding balls .
\begin{marginfigure}
 \centering
  \includegraphics[width=0.8\textwidth]{figs/ch-10/collision-2.jpg}
  \caption{Before and after in a side collision.}
  \label{side-collision}
\end{marginfigure}


We will denote by $\vec{v}$ the speed of the striking ball, and by $\vec{v}_{1}$ and $\vec{v}_{2}$ the speeds of the balls moving off after the impact at an angle $\alpha$. (Figure~\ref{side-collision}). Show that $\alpha = \ang{90}$.

The \emph{law of conservation of energy} will have the same form as for a central collision:
\begin{equation*}%
\frac{mv^{2}}{2} =  \frac{mv_{1}^{2}}{2} + \frac{mv_{2}^{2}}{2}
\end{equation*}
but the \emph{law of conservation of momentum} must now be written in a vector form:

\begin{equation*}%
m\vec{v}=m\vec{v}_{1} +m\vec{v}_{2}
\end{equation*}

\begin{marginfigure}
 \centering
  \includegraphics[width=0.8\textwidth]{figs/ch-10/collision-3.jpg}
  \caption{Resolution of vectors in a collision.}
  \label{side-collision2}
\end{marginfigure}


Figure~\ref{side-collision2} presents vector $m\vec{v}$ as a sum of vectors $m\vec{v}_{1}$ and $m\vec{v}_{2}$. Let us consider the triangle $ABC$ $(|AC|=mv, \,\,  |AB| = mv_{1}, \,\, |BC|=mv_{2})$ and apply the \emph{law of cosines}
\begin{equation*}%
|AC|^{2} =|AB|^{2} + |BC|^{2} - 2|AB| \cdot  |BC| \cos \beta
\end{equation*}
where $\beta$ is the angle between the sides $AB$ and $BC$. This relationship can be written as
\begin{equation*}%
v^{2}=v_{1}^{2} +v_{2}^{2}+ 2v_{1}v_{2} \cos \alpha
\end{equation*}

(considering that $\beta = \ang{180} - \alpha$, and hence $\cos \beta = - \cos \alpha)$.

This is the most convenient form of the law of conservation of momentum for the two colliding balls. Since from the law of conservation of energy $v^{2}= v_{1}^{2} +v_{2}^{2}$ we have $2v_{1}v_{2} \cos \alpha = 0$. And since $v_{1} \neq 0$ and $v_{2} \neq 0$, then $\cos \alpha = 0$, that is $\alpha = \ang{90}$.

\begin{figure}
\centering
\includegraphics[width=0.75\textwidth]{figs/ch-10/billiard-table.jpg}
\caption{Conservation of momentum on a billiard table.}
\label{billiard-table}
\end{figure}

Shown in Figure~\ref{billiard-table} is a specific situation on a billiard table. The directions from ball 2 to the two pockets (see dash lines) form a straight angle. Therefore, it is better to drive ball 1 against ball 2, not 3, since a good stroke may send both ball 2 and ball 1 to the respective pockets.


\section{On the Law of Conservation of Momentum}

Let us take a closer look at the \emph{law of conservation of momentum}. In our thought experiment we will replace the billiard table with a large flat surface on which $n$ balls move about colliding randomly with one another. We will suppose that in the general case the masses of the balls are different: $m_{1}, \, m_{2} , \ldots , m_{n}$. We will denote the speeds of the balls at
time $t$ as $\vec{v}_{1} (t), \, \vec{v}_{2} (t), \ldots , \vec{v}_{n}(t)$. We  will then have the sum
\begin{equation*}%
m_{1}\vec{v}_{1}(t) + m_{2}\vec{v}_{2} (t)+ \ldots +m_{n}\vec{v}_{n}(t),
\end{equation*}
which can be conveniently represented as
\begin{equation*}%
\sum_{i=n}^{n}m_{i}\vec{v}_{i}(t)
\end{equation*}
According to the law of conservation of momentum for a system of colliding balls, this sum must remain unchanged in time. Although individual summands here change in collisions, the sum as a whole remains a constant value (is conserved). The law of conservation of momentum can be written as
\begin{equation*}%
\sum_{i=n}^{n}m_{i}\vec{v}_{i}(t) = \vec{p}
\end{equation*}
where $\vec{p}$ is the total momentum of the balls. The vector $\vec{p}$ is constant, so that the collisions of balls with one another do not affect its direction and magnitude. The above equation implies that the total momentum of some system (in our case, a system of balls) \emph{does not change when parts of the system interact with one another}.

In classical mechanics the law of conservation of momentum can be derived from \emph{Newton's} third and second \emph{laws}. The derivation is instructive, and so we will reproduce it here. Suppose that within a system of balls, two balls with masses $m_{1}$ and $m_{2}$ collide. Ball $m_{2}$ exerts a force $\vec{f}_{1}$ on ball $m_{1}$, and ball $m_{1}$ exerts a force $\vec{f}_{2}$ on ball $m_{2}$. According to Newton's third law,
\begin{equation}%
\vec{f}_{1} = - \vec{f}_{2}
\label{eq-10.1}
\end{equation}
From Newton's second law, we have
\begin{equation*}%
\vec{f}_{1} = m_{1}\vec{a}_{1}, \quad \vec{f}_{2} = m_{2} \vec{a}_{2}
\end{equation*}
where $\vec{a}_{1}$ and $\vec{a}_{2}$ are the accelerations of $m_{1}$ and $m_{2}$, respectively. Let $\Delta t$ be the duration of the collision, and  $\Delta \vec{v}_{1}$ and $\Delta \vec{v}_{2}$ the changes of the speeds of the balls during the collision. For small enough $\Delta t$ we can assume that
\begin{equation*}%
\vec{a}_{1} = \dfrac{\Delta \vec{v}_{1} }{\Delta t}, \quad \vec{a}_{2} = \dfrac{\Delta \vec{v}_{2} }{\Delta t}
\end{equation*}
As a result, equation \eqref{eq-10.1} reads
\begin{equation*}%
m_{1} \, \dfrac{\Delta \vec{v}_{1} }{\Delta t} = - m_{2} \, \dfrac{\Delta \vec{v}_{2} }{\Delta t}
\end{equation*}
We will now substitute $\Delta \vec{v}_{1} = \vec{v}_{1}' -  \vec{v}_{1}$ where $\vec{v}_{1}'$ and $\vec{v}_{1}$ are the speeds of the ball $m_{1}$ before and after the collision, respectively. In a similar way we obtain $\Delta \vec{v}_{2} = \vec{v}_{2}' -  \vec{v}_{2}$. Now equation \eqref{eq-10.1} reads
\begin{equation*}%
m_{1} \,  (\vec{v}_{1}' -  \vec{v}_{1} )= - m_{2} \, (\vec{v}_{2}' -  \vec{v}_{2})
\end{equation*}
or
\begin{equation*}%
m_{1}\vec{v}_{1}' +m_{2}\vec{v}_{2}'  = m_{1}\vec{v}_{1}  + m_{2} \vec{v}_{2}
\end{equation*}
The last relation implies that as a result of the collision of the balls their total momentum does not change. We thus arrive at the law of conservation of momentum.


\section{The Vector Product of Two Vectors}
Before we proceed to consider the law of conservation of angular momentum, we will take up a purely mathematical concept of the \emph{vector product of two vectors}. Suppose that a vector $\vec{a}$ is to be multiplied in a vector way by a vector $\vec{b}$, or it is required to find a vector $\vec{c}$ that is the vector product of the initial vectors. In symbols we have $\vec{c} = (\vec{a} \times \vec{b})$. Let us translate one of the multipliers, say $\vec{b}$, parallel to itself so that both vectors ($\vec{a}$ and $\vec{b}$) have common origin. Denote by $S$ the plane passing through $\vec{a}$ and $\vec{b}$ and by $\varphi$ the angle between them. The magnitude of $\vec{c}$ will be given by
\begin{equation*}
c= a \, b \, \sin \varphi,
\end{equation*}
\begin{marginfigure}
 \centering
  \includegraphics[width=0.9\textwidth]{figs/ch-10/vector-addition-1.jpg}
  \caption{Vector addition.}
  \label{vector-addition-1}
\end{marginfigure}
and its direction is perpendicular to $S$. True, there are two directions perpendicular to the plane; these are mutually opposite. To have the \emph{required} direction for $\vec{c}$ we will turn the first multiplier $(\vec{a})$ to the second multiplier $(\vec{b})$ through the smaller angle. The direction of $\vec{c}$ will then obey the so-called \emph{right-handed screw rule} (Figure~\ref{vector-addition-1}~(a)). It is easily seen that transposing the multipliers changes the sign of their vector product: 
\begin{equation*}%
(a \times b) = - \, (b \times a) 
\end{equation*}
(compare Figure~\ref{vector-addition-1}~(a) and (b)).

Note that the direction of  $\vec{c}$ is an arbitrary notion, since it is associated with the convention to use the \emph{right-handed screw}. Nothing forbids us in principle from starting of with the \emph{left-handed screw}, not the \emph{right-handed one}. Vectors, whose direction is not conditioned physically, but is arbitrarily related to the right (left)-handed screw, are called \emph{axial} vectors. On the contrary, conventional vectors whose direction is defined physically are called \emph{polar} vectors. The vector product of two polar vectors is an axial vector.


\begin{marginfigure}
 \centering
  \includegraphics[width=0.9\textwidth]{figs/ch-10/vector-addition-2.jpg}
  \caption{Polar and axial vectors on reflection.}
  \label{vector-addition-2}
\end{marginfigure}



Notice that an \emph{axial vector is reflected in a mirror in a different way from a conventional (polar) vector}	 (see Figure~\ref{vector-addition-2}, where  $\vec{a}$ is a polar vector,  $\vec{c}$ an axial vector). The handedness of the axial vector can be seen in Figure~\ref{vector-addition-3}, where the axial vector  $\vec{c}$ is regarded as the vector product of the polar vector  $\vec{a}$ and the polar vector  $\vec{b}$. We can grasp the handedness of  $\vec{c}$ by reflecting $\vec{a}$ and  $\vec{b}$, and noticing the change in the direction of rotation from  $\vec{a}$ to  $\vec{b}$ (see circular arrow in Figure~\ref{vector-addition-3}).

\begin{figure}[!h]
\centering
\includegraphics[width=0.75\textwidth]{figs/ch-10/vector-addition-3}
\caption{Direction of resultant vector under mirror reflection.}
\label{vector-addition-3}
\end{figure}


\section[Kepler's Second Law]{Kepler's Second Law - the Law of Conservation of the Angular Momentum of a Planet}

The second of Kepler's three laws describing planetary motion is known as the \emph{law of areas}: an imaginary straight line connecting a planet with the Sun "sweeps" in the planet's orbital plane \emph{equal areas in equal time periods}. Figure~\ref{keplers-law}~(a) shows the elliptical orbit of some planet, with the Sun being at focus $O$. The areas of the figures $AOB$ and $COD$ shaded in the figure are equal; therefore, by Kepler's second law, the planet covers segments $AB$ and $CD$ in equal times. Accordingly, the closer the planet approaches the Sun, the higher is its orbital velocity.

\begin{marginfigure}
 \centering
  \includegraphics[width=0.8\textwidth]{figs/ch-10/keplers-law.jpg}
  \caption{Illustration of Kepler's law.}
  \label{keplers-law}
\end{marginfigure}

The law of areas discovered by Kepler corresponds to the \emph{law of conservation of the orbital momentum} of the planet. We will clarify the concept of \emph{angular momentum} with reference to Figure~\ref{keplers-law}~(b).

Suppose at a time $t$ a planet lies at a point $A$ on its orbit. We will draw from $O$ to $A$ a vector  $\vec{r}(t)$ -  the position vector of the planet at time $t$. The planet has a velocity $\vec{v}(t)$ and momentum $m\vec{v}(t)$ (where $m$ is the planet's mass). The vectors $\vec{r}(t)$ and $\vec{v}(t)$ make an angle $\varphi (t)$. The \emph{orbital angular momentum} of the planet, $\vec{M}$,is the vector product of the position vector $\vec{r}$ by the momentum vector $m\vec{v}$:
\begin{equation*}%
\vec{M}= (\vec{r} \times  m \vec{v}(t))
\end{equation*}
Its magnitude is determined by the product $rmv \sin \varphi$, and its direction, according to the right-handed screw rule, is normal to the orbital plane (in Figure~\ref{keplers-law} this is the direction into the page).

Here $\vec{r}$ and $m\vec{v}$ are conventional (polar) vectors, and $\vec{M}$ is an \emph{axial} vector.

We will now show that \emph{Kepler's law of areas corresponds to the law of conservation of the orbital angular momentum}. Let $A$ and $F$ be two close points on the orbit at which the planet lies at $t$ and $t + \Delta t$. respectively (Figure~\ref{keplers-law}~(c)). The time increment $\Delta t$ is assumed to be small enough - such that the arc $AF$ could be replaced by a segment and the orbital speeds of the planet at $A$ and $F$ could be considered essentially the same. The area of the shaded triangle $AOF$ in Figure~\ref{keplers-law}~(c) will be $\Delta S$. Then from vertex $F$ we will drop the perpendicular $FE$ to the extension of $AO$. It is easily seen that
\begin{equation*}%
\Delta S= \dfrac{1}{2} |AO| \cdot |FE| = \dfrac{1}{2} | AO| \cdot |AF| \cdot \sin \varphi (t) 
\end{equation*}
Since $|AO|=r(t)$ and $|AF|= v(t) \Delta t$, then
\begin{equation*}%
\dfrac{\Delta S}{\Delta t} = \dfrac{1}{2} r(t) \, v(t) \, \sin \varphi (t) 
\end{equation*}
Thus,
\begin{equation*}%
\dfrac{\Delta S}{\Delta t} = \dfrac{M}{2m} 
\end{equation*}

The left-hand side of this equation includes the areas ``swept out'' by the position vector of the planet in a unit time, and on the right-hand side we have $M$, that is, the orbital angular momentum of the planet. According to Kepler's law, the quantity $\Delta S/ \Delta t$ does not vary with time. It follows that $M$, too, must be independent of time.

True, Kepler's second law and the law of conservation of the orbital angular momentum of a planet are not completely equivalent. The law of conservation of momentum contains more information than Kepler's law of areas, since its content is the conservation of not only the \emph{magnitude} but also of the \emph{direction} of angular momentum in space. The conservation of the direction of angular momentum accounts for the fact that the orientation of the orbital plane of a planet is unchanged in space.

\section{Conservation of the Intrinsic Angular Momentum of a Rotating Body}

Apart from the \emph{orbital} angular momentum, a planet also has an \emph{intrinsic} angular momentum. Whereas orbital angular momentum is associated with the motion of a planet in its orbit, the intrinsic angular momentum arises as it rotates about its own axis. The intrinsic angular momentum of a planet is also conserved. Its direction makes an angle with the orbital plane, which does not change in the course of time. The conservation of intrinsic angular momentum is responsible for the constant alternation of night and day; the conservation of the direction of that momentum is responsible for the unchanged (for a given latitude) variation of the length of day during the different seasons.

The constancy of angular velocity and direction of rotation of a gyroscope or common top is also related to the conservation of the intrinsic angular momentum of these bodies. When a figure skater pirouetting on ice extends his arms to come to a halt swiftly, he takes advantage of the law of conservation of intrinsic angular momentum. Extending the arms sidewards shifts some mass of the skater away from the axis of rotation, which by the law of conservation of angular momentum is compensated for by a reduction in the angular velocity of rotation. We will explain this using Figure~\ref{anugular-momentum}.
\begin{figure}
\centering
\includegraphics[width=0.75\textwidth]{figs/ch-10/anugular-momentum.jpg}
\caption{Conservation of momentum of a rotating body.}
\label{anugular-momentum}
\end{figure}

For simplicity, in Figure~\ref{anugular-momentum} instead of the skater we will consider a system of two masses $m_{1}$ and $m_{2}$ slid on a massless rod. The masses rotate on it about point $O$. In the initial situation both masses lie at the same distance $r$ from $O$ (see Figure~\ref{anugular-momentum}~(a)). When the system is set in rotation, the \emph{angular velocity} $\omega$ is related to the \emph{orbital velocity} $v$ by the relation $\omega =v/r$. Then the magnitude of the total angular momentum of the system will be
\begin{equation*}%
M =r \, (m_{1}+ m_{2}) v = (m_{1}+m_{2}) \omega r^{2}
\end{equation*}
We will further suppose that m2 shifts to r 1 (see Figure~\ref{anugular-momentum}~(b)). This more or less corresponds to the skater extending his arms. Now the angular momentum will be
\begin{equation*}%
M_{1} = \omega_{1} (m_{1}r^{2}+m_{2}r_{1}^{2})�
\end{equation*}
Since $r_{1} > r$ and $M_{1} = M$ (the angular momentum is conserved), $\omega_{1} < \omega$. 

This law finds widespread use in engineering. So, this is the principle behind the gyroscope, a sort of top whose spin axis stays constant in space. Gyroscopes are used in gyrocompasses, automatic guidance devices, large gyros are used on some ships to achieve stability against
rolling.

%
%\begin{figure*}
%\centering
%\includegraphics[width=0.9\textwidth]{figs/ch-04/regular-polyhedra.png} 
%\caption{The five possible types of regular polyhedra. \label{regular-poly}} 
%\end{figure*}
%
%\begin{marginfigure}
% \centering
%  \includegraphics[width=0.9\textwidth]{figs/ch-04/dodecahedron-symm.png}
%  \caption{The symmetry of dodecahedron.}
%  \label{dodeca-symm}
%\end{marginfigure}
%
%
%\begin{figure}
%\centering
%\includegraphics[width=0.75\textwidth]{figs/ch-04/kepler.png}
%\caption{Scheme of the Solar System based on the regular polyhedra devised by Kepler.}
%\label{kepler-scheme}
%\end{figure}
%
%
%
%                        