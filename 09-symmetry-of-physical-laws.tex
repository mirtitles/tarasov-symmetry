% !TEX root = tarasov-symmetry.tex






\chapter{The Symmetry of Physical Laws}

\marginnote{ When learning about
the laws of physics you find that there is a large number of complicated and detailed laws, laws of gravitation, of electricity and magnetism, nuclear interactions, and so on. But across the variety of these detailed laws there sweep great general principles which all the laws seem to follow. Examples of these are the principles o f conservation, certain qualities of symmetry \lips - Albert Einstein}

The symmetry of the laws of physics with respect to the Lorentz transformations (or relative to changing from one inertial frame to another) is one of the most striking examples of this kind of symmetry. Also there are other forms of symmetry of physical laws.

\section{Symmetry Under Spatial Translations}

On a wide board we will install several physical devices: a mathematical pendulum, a pair of communicating vessels, an electric circuit consisting of a battery, a switch, connecting wires, and three identical ammeters, two of which are connected in parallel. Let us now check that our elementary physical laboratory functions in full conformity with the laws of physics: the period of the pendulum's swing is controlled by its length according
to the formula $T = 2 \pi sqrt{l/g}$, the level of water in both vessels is the same, the reading of either of the parallel-connected ammeters is one half that of the third ammeter. Let us now transfer our laboratory to another room. Clearly, it will function there precisely as before. This simple example is a graphic illustration of the \emph{invariance of the laws of physics under spatial translations}.


Just to get an idea of how important this type of symmetry is, let us try and imagine what would happen if physical laws were changed by spatial translations.  You move to another flat and to your surprise find that your TV set, which has functioned perfectly, now will not work. You shift a clock on your table and it either stops or goes wrong. Swimmers perform differently in different swimming pools since the water resistance changes from place to place. Results obtained in an experiment carried out in Moscow University cannot be checked at Oxford using exactly the same equipment. And so on and so forth. It is easily seen that the demise of the symmetry of physical laws with respect to spatial translations immediately leads us to a picture of some absurd, unreliable world.

To be sure, when speaking about translational symmetry we should be aware of the fact that relative translations of objects may affect the intensity and nature of their interaction. Naturally, transferring a pendulum to the Moon would change its period. Moving a clock may indeed render it inoperative if it is put on a strong magnet. Shifting a TV set farther away from a transmitting aerial will adversely affect its operation or put it out of the aerial's range. In shifting something from one point in space to another, one should consider the environment, or rather the degree of its influence on the functioning of the device in question.

Feynman writes: 
\begin{quote}
It is necessary, in defining this idea, to take into account everything that might affect the situation, so that when you move the thing you move everything.
\end{quote}
The invariance of physical laws under spatial translations can be shown referring to the example of the law of \emph{gravitation}:
\begin{equation*}%
F = G \dfrac{m_{1}m_{2}}{R^{2}}
\end{equation*}
To be more specific, we will assume that $m_{1}$ and $m_{2}$ are the solar and terrestrial masses respectively. Then $R$ is the separation between the centres of the celestial bodies, and $F$ is their mutual attraction. 
\begin{figure}
\centering
\includegraphics[width=0.75\textwidth]{figs/ch-09/vector-addition.jpg}
\caption{Spatial transformation.}
\label{vector1}
\end{figure}

The constant $G$ is known as the gravitational constant. Suppose now that the positions of the Earth and Sun relative to some point within the Galaxy chosen as the origin of coordinates are given by the position vectors $r_{1}$ and $r_{2}$ respectively. The attracting force $F$ for a given time can be found from the law of gravitation, where we put $R = |\vec{r}_{2} - \vec{r}_{1}|$ (Figure~\ref{vector1}). After a \emph{spatial translation} we will have to add to $\vec{r}_{1}$ and $\vec{r}_{2}$ the translation vector $r$ (see the figure). It is easily seen that this does not change $R$ in the expression
\begin{equation*}
R=|(\vec{r}_{2} +\vec{r})- (\vec{r}_{1} +\vec{r})|=|r_{2} - r_{1}|
\end{equation*}
It follows that a translation does not affect the law of gravitation. The invariance of the laws of physics under spatial translations is
normally described by the term the \emph{homogeneity of space}.

\section{Rotational Symmetry}

The laws of nature are invariant not only under \emph{translations} but also
under \emph{rotations} in space. It is immaterial to a TV set in which direction its
screen faces (although this is of consequence for the viewers).
Experimental results are not influenced by the fact that the experimental
set-up is oriented to north, not to east (save for geophysical
measurements). Together with our Earth we are all involved in the
complex motion around the Sun, this motion is a combination of
rotations and translations. Should physical laws not be invariant under
rotations and translations in space, they would change in time with
a period of a year. In that case the finding obtained in one or another of physical experiments would, in addition, depend on the month in which the experiment would be carried out.

The invariance of the laws of nature under rotations is normally described by the term the \emph{isotropicity of space}.

The notion ofisotropicity of space has not come easily to humanity. In ancient times it was widely believed that the Earth is flat and so the vertical direction is an absolute one. When the concept of the sphericity of the Earth took root, the vertical direction became a relative one (varying from point to point on the terrestrial surface). It is well known that for a long time the Earth was thought to be the centre of the Universe. Within the framework of this model of the world, not all of the directions in space were considered identical, but only those that passed through the centre of the Earth. In other words, that model only provided for spatial isotropicity at one point, the Earth's centre; for any other point in space it was always possible to indicate physically different directions, for example the direction to the Earth's centre and one perpendicular to it. Transferring the centre of the Universe from the Earth to the Sun will, clearly, leave the situation unchanged, that is, some directions will remain singled out. And only a negation of all centres of the Universe squares with the idea of the isotropicity of space.

\section{Symmetry in Time}
One often hears that today's physics has nothing to do with the physics of earlier times. Even special terms are used to distinguish between them- ``classical physics'' and ``modern physics''. This terminology reflects the developments in the science of physics, which just as any other science does not and cannot be at a standstill.

The absolutely natural historical process of the development of physics in no way suggests that the laws of physics change over time. One of the most important symmetries of physical laws is their constancy in time, or rather their \emph{invariance under a shift in time}.

The law of gravitation put forward by Newton describes the mutual attraction of bodies that does not change with the passage of time; the attraction had existed before Newton and it will exist in the centuries to come. The laws governing the behaviour of ideal gas, which were established in the 17th and 18th centuries, are widely used in modern science and engineering. No wonder that today's schoolchildren study, say, Archimedean principle, which was discovered in the 3rd century B. C. It will never occur to anybody that a TV set may go wrong because the physical laws governing the behaviour of the electron beam in electric and magnetic fields will change with time.

If the laws of nature changed with time, then each piece of research in physics would only have an ``up-to-the-minute'' significance. Each research worker would have to start from scratch, and there would be no continuity between generations of scientists, without which science cannot exist and progress. In a world without symmetry in time, the same causes would have certain effects today and other effects tomorrow.

The invariance of the laws of nature under shifts in time is normally called the \emph{homogeneity of time}.

\section{The Symmetry Under Mirror Reflection}

Figure~\ref{butyl-01} presents the left-handed and right-handed molecules of butyl alcohol. One molecule is the looking-glass companion of the other; when reflected, each atom of the left-handed molecule coincides with the appropriate atom of the right-handed molecule, and vice versa. Suppose that the \emph{atom-by-atom} mirror reflection was accomplished not with a single molecule but with a macroobject as a whole, for example with some physical device. As a result, we would obtain the looking-glass twin of the initial device that, up to the handedness, yields an exact replica not only of the \emph{appearance} but also of the \emph{internal} structure of the device, down to the atomic structure. If the original device contained right- handed helices or spirals, they will turn into left-handed ones; and right (left)-handed molecules or groups of molecules will turn into left (right)- handed ones. Such a mirror image may in principle exist as a real object (not only as a mental image).

Suppose then that we have the mirror image of a conventional clock, we will call it a ``mirror'' clock. It is quite obvious that such a clock will function in exactly the same way as the original clock. True, the hands of the ``mirror'' clock will turn in the opposite direction to the original clock, and so the face will look different as well. We might as well think of a ``mirror'' TV set, a ``mirror'' electric network, a ``mirror'' optical system, and so on. All of these must work in similar ways to the conventional devices, networks, systems, and so on. This means that we here have another symmetry of the laws of nature, the \emph{invariance under mirror reflection}.

The following is a graphic illustration of such a symmetry. Suppose you sit in a cinema theatre, the back wall of which is replaced by a large flat mirror. If you turn about and watch the film in the mirror, you will notice nothing unusual physically. The events in the mirror screen will happen exactly as on the usual screen. True, on the mirror screen you will have difficulties reading inscriptions that might appear on it, and furthermore, a familiar landscape or familiar asymmetrical objects will appear unfamiliar. But the latter effect of nonrecognition associated with the replacement of ``right'' by ``left'' has nothing to do with the laws of physics.


Up until 1956 physicists treated mirror symmetry as being similar to those corresponding to homogeneity of space and time, isotropicity of space, invariance under the Lorentz transformations. Put another way, they held that mirror symmetry is inherent in all the laws of physics, without exception. In 1956 the American physicists Lee and Yang suggested that invariance under mirror reflection must not apply to the group of laws that describe the decay of elementary particles. This prediction was proved by a direct experiment in 1957. Much to their surprise, physicists found that mirror symmetry is inherent not in all
phvsical laws and that in some phenomena nature exhibits \emph{left-right asymmetry}. We will take a closer look at that most interesting issue in Chapter 14. We will only note here that the discovery of mirror asymmetry in the decay of elementary particles seems to provide a clue to the striking fact of the asymmetry of living molecules (to be discussed in some detail in Chapter 7).

\section{An Example of Asymmetry of Physical Laws}

In order not to leave you with the impression that the laws of physics are invariant under any transformation, we will furnish an instructive example of transformations under which the laws of physics are \emph{noninvariant}. Such an example is the transformations that involve changes in the spatial scale, or rather similarity transformations.

\emph{AII the laws are noninvariant under similarity transformations.} In other words, the geometrical similarity principle is, strictly speaking, inapplicable to the laws of physics.

True, the notion of similarity has struck deep roots in human mind. So it enjoys wide use in literature and the arts. Suffice it to remember Jonathan Swift, who sent his Gulliver first to Lilliput and then to the giants of Brobdingnag. The same idea lay at the foundation of conjectures ventured at the turn of the century that the atom is a solar system in the microscopic world.

It would seem that if we were to build a new set-up, each part of which would be several-fold larger (or smaller) than the appropriate part of the original set-up, the new installation would function in exactly the same way as the original one. It is not for nothing that aerodynamic and hydrodynamic structures are tentatively tested on scale models.

It is well known, however, for those dealing with the tests that models should not be scaled down too much. It was Galileo who first established that the laws of nature are not symmetrical with respect to changes in scale. He came to that conclusion while speculating about the strength of bones of animals as their size is increased. Similar reasoning is provided in Feynman's book \emph{The Character of Physical Laws}. We can construct a toy gothic cathedral out of matches. Why then can we not construct a similar cathedral out of huge logs? The answer is: should we undertake the project the resultant structure would be so high and heavy that it would collapse. You might object that when comparing two things one should change everything that constitutes the system. Since the small cathedral is subject to the pull of the Earth, to be consistent we would have to subject the larger cathedral to the pull of the Earth increased appropriately, which would be even worse.

From the point of view of modern physics, the invariance of the laws of
nature with respect to similarity transformations has a clear and
comprehensive explanation - \emph{atoms have a size whose order of magnitude
is absolute, identical for the whole of the Universe}. Atomic size is
determined by the universal physical constant - \emph{Planck's constant} $\hbar \,\, (\hbar =
\SI{1.05d-34}{\joule\second})$; being given by the relationship $\hbar^{2}/me^{2}$, where $m$ and
$e$ are the mass and charge of the electron, respectively (the minimum
known mass at rest and the minimum known electric charge in nature).

This relationship yields \SI{d-10}{\metre} for the linear size of the atom. It follows, by the way, that reducing the linear dimensions of some real installation having a volume of \SI{0.1}{\metre\cubic}, say, billion-fold, we will then be left with only about one hundred atoms! Clearly, no workable apparatus can be manufactured out of such a small number of atoms.

A striking example of the asymmetry of physical laws with respect to scale changes is the fundamental fact that sufficiently strong scaling down puts the laws of classical mechanics, specifically Newton's laws, out of business. The \emph{laws of} the microworld, \emph{quantum mechanics}, take over.



%
%\begin{figure*}
%\centering
%\includegraphics[width=0.9\textwidth]{figs/ch-04/regular-polyhedra.png} 
%\caption{The five possible types of regular polyhedra. \label{regular-poly}} 
%\end{figure*}
%
%\begin{marginfigure}
% \centering
%  \includegraphics[width=0.9\textwidth]{figs/ch-04/dodecahedron-symm.png}
%  \caption{The symmetry of dodecahedron.}
%  \label{dodeca-symm}
%\end{marginfigure}
%
%
%\begin{figure}
%\centering
%\includegraphics[width=0.75\textwidth]{figs/ch-04/kepler.png}
%\caption{Scheme of the Solar System based on the regular polyhedra devised by Kepler.}
%\label{kepler-scheme}
%\end{figure}
%
%
%
%                        