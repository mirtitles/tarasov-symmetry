% !TEX root = tarasov-symmetry.tex




\chapter*{A Conversation Between\\
the Author and the Reader\\
About What Symmetry Is}
\addcontentsline{toc}{chapter}{A Conversation}

\begin{dialogue}

\speak{\txtauth} There is an old parable about the
`Buridan's ass'. The ass starved to death
because he could not decide on which heap of
food to start with (\hyperref[buridan]{Figure~\ref{buridan}}). The allegory of the ass is, of course, a joke. But take a look at the
two balanced pans in the figure. Do the pans
not remind us in some way of the parable?

\begin{figure}[!ht]
\centering
\includegraphics[width=.75\textwidth]{figs/ch-00/buridans-ass.jpg}
\caption{The ass starved to death as he could not decide which heap to start with.}
\label{buridan}
\end{figure}


\speak{\txtread} Really. In both cases the left and the
right are so identical that neither can be given
preference.


\speak{\txtauth} In other words, in both cases we
have a \emph{symmetry}, which manifests itself in the
total equity and balance of the \emph{left} and \emph{right}.
And now tell me what you see in  \hyperref[pyramid]{Figure~\ref{pyramid}}.

\begin{figure}[!ht]
\centering
\includegraphics[width=.75\textwidth]{figs/ch-00/pyramid.jpg}
\caption{The pyramids are symmetric structures.}
\label{pyramid}
\end{figure}


\speak{\txtread} The foreground shows a pyramid. Such pyramids were erected in Ancient Egypt. In the background are hills.

\speak{\txtauth} And maybe in the foreground you see a hill as well?

\speak{\txtread} This is no hill. This looks like an artificial structure. A hill normally does not possess that regular, symmetric shape.

\speak{\txtauth} Quite so. Perhaps you could provide quite a few examples of regular
configuration (symmetry) of objects or structures created by man?

\speak{\txtread} A legion of them. To begin with. architectural structures. For example, the building of the Bolshoi Theatre in Moscow. Essentially all vehicles, from a cart to a jet liner. Household utensils (furniture, plates,
etc.). Some musical instruments: a guitar, a violin, a drum, \lips

\speak{\txtauth} Man-made things are often symmetrical in shape. Although this is not always the case, just remember a piano or a harp. But why do you think symmetry is so often present in human products?

\speak{\txtread} The symmetry of shape of a given object may be determined by purpose. Nobody wants a lop-sided ship or a plane with wings of different lengths. Besides, symmetrical objects are beautiful.

\speak{\txtauth} That reminds me of the words of the German mathematician Hermann Weyl (1885-1955), who said that through symmetry man always tried to `perceive and create order, beauty and perfection'.

\speak{\txtread} The idea of symmetry, it seems, is inherent in all spheres of human endeavour.

\speak{\txtauth} Exactly. It would be erroneous
however, to think that symmetry is mostly
present in human creations whereas nature
prefers to appear in non-symmetrical (or
\emph{asymmetrical}) forms. Symmetry occurs in
nature no rarer than in man-made things.
Since the earliest times nature taught man to
understand symmetry and then use it. Who
has not been moved by the symmetry of
snow-flakes, crystals, leaves, and flowers?
Symmetrical are animals and plants.
Symmetrical is the human body.



\speak{\txtread} Really, symmetrical objects seem to
surround us.

\speak{\txtauth} Not just objects. Symmetry can also
be seen in the regularity of the alternation of
day and night, of seasons. It is present in the
rhythm of a poem. Essentially we deal with
symmetry everywhere where we have
a \emph{measure} of order. Symmetry, viewed in the
widest sense, is the opposite of chaos and
disorder.


\speak{\txtread} It turns out that symmetry is
balance, order, beauty, perfection, and even
purpose. But such a definition appears to be
a bit too general and blurry, doesn't it? What
then is meant by the term `symmetry' more
specifically? What features signal the
presence, or absence for that matter, of
symmetry in a given instance?


\speak{\txtauth} The term `symmetry' ($\sigma \nu \mu \mu \epsilon \tau \rho \nu \alpha $) is the Greek for `proportionality, similarity in arrangement of parts'.


\speak{\txtread} But this definition is again not concrete.


\speak{\txtauth} Right. A mathematically rigorous
concept of symmetry took shape only
recently, in the 19th century. The simplest
rendering (according to Weyl) of the modern
definition of symmetry is as follows: \emph{an object
is called symmetrical if it can be changed
somehow to obtain the same object.}

\speak{\txtread} If I understand you correctly, the
modern conception of symmetry assumes the
\emph{unchangeability} of an object subject to some
\emph{transformations}.

\speak{\txtauth} Exactly.

\speak{\txtread} Will you please clarify this by an

\speak{\txtauth} Take, for example, the symmetry of the letters \textbf{U}, \textbf{H} and \textbf{N}.

\speak{\txtread} But \textbf{N} does not appear to be
symmetrical at all.

\speak{\txtauth} Let's start with \textbf{U}. If one half of the letter were reflected in a plane mirror, as is
shown in  (\hyperref[alphabets]{Figure \ref{alphabets} a}), the reflection would coincide exactly with the other half. This is an example of so-called \emph{mirror symmetry}, or rather a symmetry in relation to mirror reflection. We have already encountered this form of symmetry: it then appeared as an equilibrium of the left and right sides (the left and the right in (\hyperref[buridan]{Figure~\ref{buridan}}) can be viewed as mirror reflections of each other).

\begin{figure*}[!ht]
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-00/alphabets.jpg}
\caption{Looking at symmetries of the letters of alphabet.}
\label{alphabets}
\end{figure*}



Letter \textbf{H} is symmetrical to an even higher degree than \textbf{U}. It can be reflected in two plane mirrors (\hyperref[alphabets]{Figure \ref{alphabets} b}). As for the letter \textbf{N}, it has no mirror symmetry, but it has the so-called \emph{rotational symmetry} instead. If \textbf{N} were turned through \ang{180} about the axis, that is normal to the letter's plane and passes through its centre (\hyperref[alphabets]{Figure \ref{alphabets} c}), the letter would coincide with itself. Put another way, the letter \textbf{N} is symmetrical in relation to a turn through \ang{180}. Note that rotational symmetry is also inherent in \textbf{H}, but not in \textbf{U}. 

\speak{\txtread} An Egyptian pyramid too possesses a rotational symmetry. It would coincide with itself if mentally turned through \ang{90} about the vertical axis passing through the pyramid's summit.

\speak{\txtauth} Right. In addition, a pyramid
possesses a mirror symmetry. It would
coincide with itself if mentally reflected in any
of the four imaginary planes shown in (\hyperref[planes-rotation]{Figure~\ref{planes-rotation}})

\begin{marginfigure}%[!ht]
\centering
\includegraphics[width=\textwidth]{figs/ch-00/planes-of-rotation.jpg}
\caption{The planes of symmetry for a pyramid.}
\label{planes-rotation}
\end{marginfigure}


\speak{\txtread} But how is one to understand the above-mentioned symmetry in the alternation of seasons? 

\speak{\txtauth} As the unchangeability of a certain set of phenomena (including weather, blooming of plants, the coming of snow, etc.) in relation to shift in time over 12 months

\speak{\txtread} The beauty of symmetrical objects and phenomena seems to come from their harmony and regularity. 

\speak{\txtauth} The question of the beauty related to symmetry is not that obvious.

\speak{\txtread} Why not? I can imagine that
looking at harmonious, balanced, recurrent
parts of a symmetrical object must give one
the feeling of peace, stability, and order. As
a result, the object will be perceived as beautiful. And even more so, if in its symmetry we
also see some purpose. On the contrary, any
accidental violation of symmetry (the
collapsed corner of a building, the broken
piece of a neon letter, an early snow) must be
received negatively - as a threat to our trust
in the stability and orderliness of the surrounding world.

\speak{\txtauth} That is all very well but it is known
that symmetry may also produce negative
emotions. Just look at some modern
residential areas consisting of identical
symmetrical houses (often fairly convenient
and rational), do they not bore you to death?
On the other hand, some deviations from
symmetry used widely in painting and sculpture create the mood of freedom and
nonchalance, and impart an inimitable
individuality to a work of art. Hardly anyone would be immune to the spell of a spring
meadow in full bloom with an absolutely asymmetrical array of colours. Do you really think that a neatly mown lawn or a trimmed tree look prettier than a clearing in a forest or an oak growing in a field?

\speak{\txtread}  Beauty is thus not always related to symmetry.

\speak{\txtauth} The truth is that when considering symmetry, you have to take into account not only symmetry as such, but also deviations from it. \emph{Symmetry} and \emph{asymmetry} must be approached simultaneously.

\speak{\txtread}  Perhaps as is the case in nature?

\speak{\txtauth} Of course. But one important point
here: one should not merely look at given
violations of symmetry in a specific flower or
organism of an animal. The issue of symmetry-asymmetry is far deeper. Symmetry
may be said to express something \emph{general},
characteristic of different objects and
phenomena, it is associated with the structure
and lies at the very foundation of things.

Asymmetry, on the other hand, is related to realizations of the structure of a given \emph{specific} object or phenomenon.

\speak{\txtread} Symmetry is general, asymmetry is specific?

\speak{\txtauth} If you like, you may think of it this
way. In a specific object we find elements of
symmetry, which link it to other similar
objects. But the individual `face' of a given
object invariably shows up through the
presence of some element of asymmetry.
Spruces have much in common: a vertical stem, characteristic branches arranged in a certain rotational symmetry around the stem, a definite alternation of branches along the stem, and lastly the structure of needles. And still you take your time selecting the right spruce at a Christmas tree bazar. Among the many trees available you look for individual traits that you like most.


\speak{\txtread} It turns out then that the mathematical idea of symmetry in each case is embodied in real, not-very-symmetrical objects and phenomena.


\speak{\txtauth} Let's try and visualize a world, arranged in a totally symmetrical manner. Such a world would have to be able to be reproduced in any rotation about any axis, in the reflection of any mirror, in any translation, and so on. This would be something absolutely homogeneous, uniformly spread out over the entire space.

Stated another way, in that sort of world you would observe nothing, neither objects nor phenomena. And so such a world is impossible.

The world exists owing to the marriage of symmetry and asymmetry, which can in a way be treated as the unity of the general and the specific.

\speak{\txtread} Frankly, I never thought of symmetry in such a broad context.

\speak{\txtauth}  Let's draw some conclusions. Symmetry occurs widely, both in nature and in human life. Therefore, even a layman generally has no difficulty in discerning symmetry in its relatively simple manifestations.

Our world, all the things and phenomena in it, must be approached as a manifestation of symmetry and asymmetry. In that case, symmetry is not just abundant, it is ubiquitous, in the broadest sense.

Symmetry is utterly diverse. Objects may remain unchanged under turns, reflections, translations, interchanges, and so forth. 

Symmetry has many causes. It may be related to orderliness and equilibrium, proportionality and harmony of parts (and sometimes monotony), purposefulness and usefulness.

\end{dialogue}
\pagecolor{white}
