% !TEX root = tarasov-symmetry.tex






\chapter{Regular Polyhedra}




Since time immemorial, \marginnote{Inhabitants of even the most distant galaxy cannot play dice having a shape of a regular convex polyhedron unknown to us. - M. Gardner} when contemplating the picture of the Universe, man made active use of the concept of \emph{symmetry}. Ancient Greeks preached a symmetrical universe simply because symmetry was beautiful to them. Proceeding from the concept of symmetry, they made some conjectures. So Pythagorus (the 5th century B.C.), who thought of the sphere as the most symmetrical and perfect shape, concluded that the Earth is spherical and moves over a sphere. He also assumed that the Earth revolves about some `central fire', together with the six planets known at the time, the Moon, the Sun, and the stars.

The ancients liked symmetry and besides spheres they also used regular polyhedra. So they established the striking fact that there are \emph{only five regular convex polyhedra}. First studied by the Pythagoreans, those five regular polyhedra were later described in more detail by Plato and so they came to be known in mathematics as \emph{Platonic solids}.



\section{The Five Platonic Solids}

A \emph{regular polyhedron} is a volume figure with congruent faces having the shape of regular polygons, and with congruent dihedral angles. It turns out that there can be only five such figures (although there exist an infinite number of regular polygons). All the types of regular polyhedra are provided in \hyperref[regular-poly]{Figure~\ref{regular-poly}}: the \emph{tetrahedron} (regular triangular pyramid), \emph{octahedron, icosahedron, hexahedron} (cube), \emph{dodecahedron}. The cube and octahedron are \emph{mutual}: if in one of these polyhedra the centres of faces having a common edge are connected, the other polyhedron is obtained. Also mutual are the dodecahedron and icosahedron.

It can readily be shown why only five regular polyhedra are possible. Let us take the simplest face - \emph{the equilateral triangle}. A polyhedral angle can be obtained by putting together three, four or five equilateral triangles, that is, in three ways. (If there are six triangles, the angles at the common vertex sum up to \ang{360}.) With \emph{squares} a polyhedral angle can only be formed in one way - with three squares. With \emph{pentagons} too, a polyhedral angle can be obtained in one way - with three pentagons. Clearly, no regular polyhedral angles can be formed out of $n$-hedra with $n \geqslant  6$. Consequently, only five types of regular polyhedra can exist: three polyhedra with triangular faces (tetrahedron, octahedron, and icosahedron), one with square faces (cube), and one with pentagon faces (dodecahedron).

\begin{figure*}
\centering
\includegraphics[width=0.9\textwidth]{figs/ch-04/regular-polyhedra.jpg} 
\caption{The five possible types of regular polyhedra.}
\label{regular-poly} 
\end{figure*}

\section{The Symmetry of the Regular Polyhedra}

The elements of \emph{symmetry of the tetrahedron} are illustrated in \hyperref[tetra-symm]{Figure~\ref{tetra-symm}}. It has four three-fold rotation axes and three two-fold axes. Each of the three-fold axes passes through a vertex and the centre of the opposite face (for example, axis $AE$ in the figure). Each two-fold axis passes through the middles of opposite edges (for example, axis $FG$). Through each two-fold axis pass two planes of symmetry (through an axis and one of the edges that intersect with a given axis); in the figure planes $AGC$ and $DFB$ pass through axis $FG$. The tetrahedron thus has six planes of symmetry. In addition, the tetrahedron has a mirror-rotational symmetry: each two-fold rotation axis also doubles as a four-fold mirror-rotation axis.

\begin{marginfigure}[-2cm]
 \centering
  \includegraphics[width=\textwidth]{figs/ch-04/tetrahedron-symm.jpg}
  \caption{The symmetry of tetrahedron.}
  \label{tetra-symm}
\end{marginfigure}

The \emph{symmetry of the cube} was discussed partially in Chapter 2. Recall that the cube has 13 rotation axes of symmetry: three four-fold axes, four three-fold axes, and six two-fold axes. Interestingly, the \emph{octahedron} has the same elements of \emph{symmetry} as the cube. The octahedron has three four-fold rotation axes (they pass through opposite vertices, like axis $AB$ in \hyperref[octa-symm]{Figure~\ref{octa-symm}}), four three-fold axes (they pass through the centres of opposite faces, like axis $CD$), and six two-fold axes (they pass through centres of opposite, mutually parallel edges, like axis $EF$). Both the cube and the octahedron have nine planes of symmetry (find them on your own). Lastly, each three-fold rotation axis in the cube and the octahedron is at the same time a six-fold mirror-rotation axis.


\begin{marginfigure}%[-2cm]
 \centering
  \includegraphics[width=0.9\textwidth]{figs/ch-04/octacahedron-symm.jpg}
  \caption{The symmetry of octahedron.}
  \label{octa-symm}
\end{marginfigure}


It has already been noted above that cube and octahedron are \emph{mutual} polyhedra. That is why they have the same elements of symmetry. The mutuality of the dodecahedron and the icosahedron also implies that the two have the same symmetry.

The \emph{symmetry of the dodecahedron} is illustrated in \hyperref[dodeca-symm]{Figure~\ref{dodeca-symm}}. Axis $AB$, which passes through the centres of the opposite faces, is one of the six five-fold rotation axes; axis $CD$, which passes through the opposite vertices, is one of the ten three-fold axes; axis $EF$, which passes through the centres of the opposite mutually parallel edges, is one of the 15 two-fold rotation axes. The same rotation axes are present in the \emph{icosahedron}; only in it five-fold axes pass through opposite vertices, not through the centres of opposite faces, whereas three-fold axes pass through the face centres.

\begin{marginfigure}
 \centering
  \includegraphics[width=0.9\textwidth]{figs/ch-04/dodecahedron-symm.jpg}
  \caption{The symmetry of dodecahedron.}
  \label{dodeca-symm}
\end{marginfigure}

\section{The Uses of the Platonic Solids to Explain Some Fundamental Problems}

The notion of symmetry has often been used as the framework of thought for hypotheses and theories of scholars of centuries past, who put much stock in the mathematical harmony of the creation of the world and regarded that harmony as a sign of divine will. They viewed the Platonic solids as a fact of fundamental importance, directly related to the structure of matter and the Universe.

So the Pythagoreans, and later Plato, believed that matter consists of four principal elements - \emph{fire, earth, air,} and \emph{water}. According to their thinking, the atoms of principal elements must have the shape of various Platonic solids: fire atoms must be tetrahedra; earth atoms, cubes; air atoms, octahedra; and water atoms, icosahedra.

The concept of symmetry coded in the five Platonic solids enthralled the famous German astronomer Johannes Kepler (1571-1630), who undertook to explain why there are just six planets in the Solar system (in Kepler's days, too, only six planets were known), and why the radii of their `spheres' (orbits) are in the ratio $8:15:20:30:115:195$ (according to Kepler's results). Kepler inscribed a cube into Saturn's sphere. Next into that cube he inscribed another sphere, that of Jupiter. Into Jupiter's sphere he inscribed a tetrahedron, and into the tetrahedron the Martian sphere, and into the latter a dodecahedron. Finally, he inscribed the Earth's sphere. Then followed in succession an icosahedron, inscribed into the Earth's sphere, Venus's sphere, an octahedron inscribed into the latter, and lastly Mercury's sphere. It is easily seen that Kepler's scheme employs all five Platonic solids. Part of the scheme is depicted in \hyperref[kepler-scheme]{Figure~\ref{kepler-scheme}}. Kepler calculated the radii of the planetary spheres in accordance with his scheme to find that the ratio of these radii were in good agreement with the observations. This striking coincidence made Kepler believe in his underlying assumption. He thought that he had succeeded in explaining the structure of the entire Solar system on the basis of a geometrical system using spheres and the five Platonic solids, thus directly relating the existence of six planets to the existence of the five Platonic solids. Kepler wrote: 
\begin{quote}
The great joy I experienced from that discovery cannot be put into words. I did not regret any of the time spent and felt no fatigue. I was not afraid of cumbersome calculations while seeking to find whether my hypothesis was in agreement with Copernicus's theory of orbits, or whether my joy was to vanish into smoke.
\end{quote}
\begin{figure}
\centering
\includegraphics[width=0.75\textwidth]{figs/ch-04/kepler.jpg}
\caption{Scheme of the Solar System based on the regular polyhedra devised by Kepler.}
\label{kepler-scheme}
\end{figure}

Kepler's enthusiasm turned out to be premature. The coincidence he hit upon was accidental and, as was shown by later observations, quite rough. And to top it off, there are actually nine planets, not six, in the Solar system.

\section{On the Role of Symmetry in the Cognition of Nature}

The two just-discussed examples of unsuccessful applications of the
Platonic solids to explain fundamental issues of nature suggest that symmetry alone is not sufficient. Yet the uses of symmetry to ponder the world around us are significant.

The Platonic solids simply furnish an example of how symmetry may drastically limit the variety of structures possible in nature. We will now elaborate this important idea. Suppose that in some distant galaxy, live highly developed creatures who, among other things, are fond of games. We may be totally ignorant of their tastes, structure of their bodies, and their mentality. We know, however, that their dice have any of the five shapes - tetrahedron, cube, octahedron, dodecahedron, and icosahedron. Any other shape of a dice is impossible in principle, since the requirement that any face must have equal probability of appearance dictates the use of a regular polyhedron, and there are only five regular polyhedra.

\emph{A measure of order introduced by symmetry shows up, above all, in limitation o f the variety o f possible structures and reduction o f the number o f possible versions.}

Later in the book we will discuss some limitations imposed by symmetry on the diversity of structures of molecules and crystals. The modern American popularizer of science Martin Gardner writes: 
\begin{quote}
Perhaps some day physicists will discover some mathematical constraints to be met by the number of elementary particles and basic laws of nature.
\end{quote}

                        